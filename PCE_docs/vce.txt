                       PC Engine Video Colour Encoder

                 by Paul Clifford (paul@plasma.demon.co.uk)


$0400 (R?/W) Set frequency dividing ratio
    Bit 15-1 - Unused
    Bit    0 - Dividing ratio of 4 (0) or 3 (1)
               [can be ignored unless you're writing for a real PC Engine]
$0402 (R?/W) LSB of palette address
$0403 (R?/W) MSB of palette address
$0404 (R /W) LSB of palette data
$0405 (R /W) MSB of palette data

Palette data
    Bit 15-9 - Unused
    Bit  8-6 - Green
    Bit  5-3 - Red
    Bit  2-0 - Blue

The Video Colour Encoder (VCE) supports 32 palette tables, each of which has
16 entries.  The first 16 tables are used by the background tiles and the
second 16 by the sprites.  Each palette entry is a 9 bit G-R-B value, giving
a choice of 2^9 = 512 different colours.  However, in both the background and
sprite palettes, colour 0 is treated specially:

- Background colour zero is always taken from the first palette, ie from
  address $000 in the table, regardless of which palette is being used.  It
  is also the only colour over which a background sprite (bit 7 of the sprite
  attributes) will be visible.

- Sprite colour 0 is always treated as transparent, again regardless of which
  palette is being used.

The address formed by $0402 and $0403 is automatically incremented by one
each time a byte is read from or written to $0405.

The layout of the VCE memory is summarised in the diagram below:

                                         876 543 210
     +--------------+        /--        +---+---+---+
$000 | BG Palette 0 |  ------|    +$000 | G | R | B | Colour 0
     +--------------+        |          +---+---+---+
$010 | BG Palette 1 |        |    +$001 | G | R | B | Colour 1
     +--------------+        |          +---+---+---+
           ....              |               ...
     +--------------+        |          +---+---+---+
$0F0 | BG Palette F |        |    +$00F | G | R | B | Colour F
     +--------------+        \--        +---+---+---+
$100 | SP Palette 0 |
     +--------------+
$110 | SP Palette 1 |
     +--------------+
           ....
     +--------------+
$1F0 | SP Palette F |
     +--------------+
