package com.laconic.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Set;

import com.laconic.gameboy.controller.Cart;
import com.laconic.gameboy.controller.CartType;
import com.laconic.gameboy.controller.MBC1;
import com.laconic.gameboy.controller.MBC3;

public class GameboyReader extends RomReader {

    public static Cart loadCart(String filename){
        byte[] romData = readROM(filename);
        getCartMetaData(romData);
        int[] ramData = setRAM(romData[0x149] & 0xFF);
        int numberOfRomBanks = setROM(romData[0x148] & 0xFF);
        int cartType = romData[0x147] & 0xFF;
        return loadCartType(romData,ramData[0],numberOfRomBanks,ramData[1],cartType,"");
    }

    private static void getCartMetaData(byte[] romData){
        System.out.println("~!~!~Cart metadata~!~!~");
        System.out.println("color gb flag: "+String.format("0x%02X",romData[0x143]));
        System.out.println("Game mfg code: "+String.format("0x%02X",romData[0x144])+":"+String.format("0x%02X",romData[0x145]));
        System.out.println("Cart type: "+String.format("0x%02X",romData[0x147]));
        System.out.println("ROM size: "+String.format("0x%02X",romData[0x148]));
        System.out.println("RAM size: "+String.format("0x%02X",romData[0x149]));
        System.out.println("~!~!~Cart metadata end~!~!~");
      }
    
    
    private static int[] setRAM(int ramSize){
        int[] resultSet = new int[2];
        switch(ramSize){
    
            case 0:
            resultSet[0] = 1;
            resultSet[1] = 0;
            break;
    
            case 1:
            resultSet[0] = 0x800;
            resultSet[1] = 1;
            break;
    
            case 2:
            resultSet[0] = 0x2000;
            resultSet[1] = 1;
            break;
    
            case 3:
            resultSet[0] = 0x8000;
            resultSet[1] = 4;
            break;
    
            case 4:
            resultSet[0] = 0x20000;
            resultSet[1] = 16;
            break;
    
            case 5:
            resultSet[0] = 0x10000;
            resultSet[1] = 8;
            break;

            default:
            resultSet[0] = -1;
            resultSet[1] = -1;
        }
        return resultSet;
      }
    
      private static int setROM(int romSize){
        switch(romSize){
            case 0:
            return 0;
    
            case 1:
            return 4;
    
            case 2:
            return 8;
    
            case 3:
            return 16;
    
            case 4:
            return 32;

            default:
            return -1;
        }
      }

      private static Cart loadCartType(byte[] rom, int ramSize, int romBanks, int ramBanks, int index, String title){
        if(index > 0 && index < 4){
          return new MBC1(rom, ramSize, romBanks, ramBanks, CartType.MBC1, ""); //MBC1;
        }else if(index > 7 && index < 9){
          System.out.println("unimplemented cart type MBC2");
          return null;
        }else if(index > 0xE && index < 0x19){
          return new MBC3(rom, ramSize, romBanks, ramBanks, CartType.MBC3, ""); //MBC3
        }else if(index > 0x18 && index < 0x1F){
          System.out.println("unimplemented cart type MBC5");
          return null;
        }else{
          System.out.println("bad cart read in Gameboyreader");
          return null;
        }
      }
}