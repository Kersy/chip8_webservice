package com.laconic.util;

import com.laconic.pce.hucard.Hucard;

public class PceReader extends RomReader{
    
    public static Hucard loadCart(String filename){
        return new Hucard(readROM(filename));
    }
}