package com.laconic.gameboy;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;
import com.laconic.gameboy.GBInterruptManager.InterruptType;

public class GBSerial implements StatefulComponent, Endpoint{

    public int id = 0;
    public byte SB;
    byte SC = 0x7E;
    GBInterruptManager ime;
    Socket socket;
    int cycler = 0;
    boolean transfering = false;
    public Endpoint endpoint = null;

    public GBSerial(GBInterruptManager ime) {
        this.ime = ime;
        // this.endpoint = endpoint;
    }

    public void cycle(int numberOfCycles){
        //if this is the slave, we may have a message, if it's the master
        //transfering will already be true, so this doesn't matter

        if(!transfering){
            // System.out.println("id: "+this.id);
            // return;
            // if(cycler > 7){
            //     cycler = 0;
            //     this.ime.requestInterrupt(InterruptType.SERIAL);
            // }
        }else{
            if(this.endpoint == null){
                return;
            }
            if(cycler++ < 8){
                // int outBit = ((this.SB & 0b10000000) == 0)? 0 : 1;
                // int inBit = this.endpoint.transfer((byte)outBit);
                // this.SB = (byte)((this.SB << 1) | inBit);
            }else{
                this.SB = this.endpoint.transfer(this.SB);
                System.out.println("transfer complete: "+String.format("0x%02X",this.SB));
                this.cycler = 0;
                this.transfering = false;
                this.SC = 0x7F;
                this.ime.requestInterrupt(InterruptType.SERIAL);
            }
            try{
                Thread.sleep(1);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void write(int address, byte data){
        if(address == 0xFF01){
            this.SB = data;
        }else if(address == 0xFF02){
            byte req = (byte)(data & 0b10000001);
            this.SC = (byte)(this.SC | req);
            if((this.SC & 0x80) != 0 && ((this.SC & 0x1) != 0)){
                this.transfering = true;
                this.cycler = 0;
            }
        }else{
            System.out.println("illegal serial write at address: "+address);
        }
    }

    public byte read(int address){
        if(address == 0xFF01){
            return this.SB;
        }else if(address == 0xFF02){
            return (byte)(this.SC | 0b01111110);
        }else{
            System.out.println("error when reading from serial at address: "+address);
            return -1;
        }
    }

    @Override
    public byte[] save() {
        ByteBuffer buffer = ByteBuffer.allocate(100_000);
    
        String header = "SRL";
        String SBR = "SBR";
        String SCR = "SCR";

        appendBuffer(header, new byte[4], buffer);
        appendByte(SBR, this.SB, buffer);
        appendByte(SCR, this.SC, buffer);
    
        return buffer.array();
    }

    @Override
    public void load(byte[] data) {
        byte[] bytelength = new byte[4];
        ByteBuffer buffer = ByteBuffer.wrap(data);

        String header = "SRL";
        String SBR = "SBR";
        String SCR = "SCR";

        loadData(header,buffer,bytelength);
        loadByte(SBR, buffer);
        loadByte(SCR,buffer);
    }

    @Override
    public byte transfer(byte bitIn) {
        // cycler++;
        byte outByte = this.SB;
        this.SB = bitIn;
        this.SC = (byte) (this.SC ^ 0b10000000);
        this.ime.requestInterrupt(InterruptType.SERIAL);
        return outByte;
        // int outBit = ((this.SB & 0b10000000) == 0)? 0 : 1;
        // this.SB = (byte)((this.SB << 1) | bitIn);
        // cycler++;
        // return (byte)outBit;
    }
}