package com.laconic.gameboy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.laconic.emulator.Display;
import com.laconic.emulator.Emulator;
import com.laconic.emulator.LinkCable;
import com.laconic.gameboy.gpu.PPU;

public class GUILite extends JFrame implements Runnable{

  private static final long serialVersionUID = 865320822984443203L;

  private JTextArea sourceArea;//, listingArea;
  private JButton button, butt, tile;
  private JMenuItem saveMI, resetMI, loadMI, quitMI;
  private final JFileChooser jfile;
  private Display buttonPanel;
  private Emulator cpu;
//  private static GBProcessor cpu;
  private GBRegisters registers;
  private GBMMU mmu;
//  private static GBGPU gpu;
  private PPU gpu;
  private LCDController lcdc;
  private GBTimer timer;

  private byte[] tileSet;
  private byte[] romData;
  private int ti = 0;

  private LinkCable linkCable;

  private byte[][] display = new byte[144][160];

  private boolean running = true;

  private GBJoypad joypad;

  private JPanel textPanel;
  
  public GUILite(Emulator emulator){

    joypad = ((GBProcessor)emulator).joypad;
    // joypad = new GBJoypad();
    cpu = (GBProcessor)emulator;
    // cpu = new GBProcessor(joypad);
    mmu = ((GBProcessor)cpu).mmu; 
    linkCable = new LinkCable();

    running = true;
    // buttonPanel.requestFocus();

    // cpu = emulator;
    // cpu.CPUReset();

    setTitle("Chip8 Emulator");
    sourceArea = new JTextArea();
    sourceArea.setEditable(false);
    //this is for when we want to print tile maps
//    sourceArea.setCo128);

    button = new JButton("step");
    butt = new JButton("load");
    tile = new JButton("save");

    // buttonPanel = new Display(cpu.getDisplay(),64,32,5);
     buttonPanel = new Display(new short[144][160],160,144,3);

    textPanel = new JPanel(new GridLayout(1, 2));
    textPanel.add(buttonPanel);
    textPanel.add(new JScrollPane(sourceArea));

    Container container = getContentPane();
    container.add(textPanel, BorderLayout.CENTER);
    container.add(button,BorderLayout.SOUTH);
    container.add(tile,BorderLayout.WEST);
    container.add(butt,BorderLayout.EAST);

    saveMI = new JMenuItem("Save");
    resetMI = new JMenuItem("Reset");
    loadMI = new JMenuItem("Load ROM");
    quitMI = new JMenuItem("Quit SkinnyBASIC parser");
    JMenu fileMenu = new JMenu("File");
    fileMenu.add(saveMI);
    fileMenu.add(resetMI);
    fileMenu.add(loadMI);
    fileMenu.addSeparator();

    fileMenu.addSeparator();
    fileMenu.add(quitMI);
    JMenuBar bar = new JMenuBar();
    bar.add(fileMenu);
    setJMenuBar(bar);
    jfile = new JFileChooser();
    butt.addActionListener(new LoadListingListener());
    button.addActionListener(new SaveListingListener());
    tile.addActionListener(new TileListener());
    saveMI.addActionListener(new SaveListener());
    resetMI.addActionListener(new ResetListener());
    loadMI.addActionListener(new LoadListener());

    buttonPanel.addKeyListener(new KeyboardListener());
    buttonPanel.requestFocus();
    this.pack();

  }

  @Override
  public void run(){}

  private class LoadListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e){
//      jfile.showOpenDialog(GUI.this);
//      File rom = jfile.getSelectedFile();
//      try{
//        GBCart cart = new GBCart(rom);
//        joypad = new GBJoypad();
//
//        mmu = new GBMMU(cart,joypad);
//        GBInterruptManager ime = new GBInterruptManager(mmu);
//        gpu = new PPU(mmu,ime);
//        mmu.setPPU(gpu);
//        timer = new GBTimer(mmu,ime);
//        cpu = new GBProcessor(mmu,timer,ime,gpu);
//      }catch(Exception ex){
//        ex.printStackTrace();
//      }
    }
  }
  private class SaveListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
      System.out.println("state saved");
    }
  }

  private class ResetListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e){
//      cpu.reset();
      System.out.println("cpu reset succesfully");
    }
  }

  private class TileListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      // running = false;
      cpu.saveState("matthew");
      // cpu.loadState("");
      // running = true;
      buttonPanel.requestFocus();
    }
  }

  private class SaveListingListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      // cpu.decode();
      buttonPanel.requestFocus();
      // refreshDisplay();
    }
  }

  private class LoadListingListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      //running = false; //stop running
      cpu.loadState("matthew");
      //running = true;
      buttonPanel.requestFocus();
    }
  }

  private class KeyboardListener implements KeyListener{

    public void keyPressed(KeyEvent k){

      switch(k.getKeyCode()){

        case KeyEvent.VK_A: //A
          joypad.setButton((byte)0b1110);
          break;

        case KeyEvent.VK_S: //B
          joypad.setButton((byte)0b1101);
          break;

        case KeyEvent.VK_ENTER: //start
          joypad.setButton((byte)0b0111);
          break;

        case KeyEvent.VK_SPACE: //select
          joypad.setButton((byte)0b1011);
          break;

        case KeyEvent.VK_LEFT: //left
          joypad.setDirection((byte)0b1101);
          break;

        case KeyEvent.VK_RIGHT: //right
          joypad.setDirection((byte)0b1110);
          break;

        case KeyEvent.VK_UP: //up
          joypad.setDirection((byte)0b1011);
          break;

        case KeyEvent.VK_DOWN: //down
          joypad.setDirection((byte)0b0111);
          break;

        default:
        System.out.println(k.getKeyCode());
        break;

      }
    }

    public void keyReleased(KeyEvent k){
      switch(k.getKeyCode()){

        case KeyEvent.VK_A: //A
          joypad.resetButton((byte)0b1110);
          break;

        case KeyEvent.VK_S: //B
          joypad.resetButton((byte)0b1101);
          break;

        case KeyEvent.VK_ENTER: //start
          joypad.resetButton((byte)0b0111);
          break;

        case KeyEvent.VK_SPACE: //select
          joypad.resetButton((byte)0b1011);
          break;

        case KeyEvent.VK_LEFT: //left
          joypad.resetDirection((byte)0b1101);
          break;

        case KeyEvent.VK_RIGHT: //right
          joypad.resetDirection((byte)0b1110);
          break;

        case KeyEvent.VK_UP: //up
          joypad.resetDirection((byte)0b1011);
          break;

        case KeyEvent.VK_DOWN: //down
          joypad.resetDirection((byte)0b0111);
          break;

      }
    }
    public void keyTyped(KeyEvent k){}
  }

  private class QuitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}

  public void init(){
    buttonPanel.requestFocus();
    loop();
  }

  public void connectLinkCable(Endpoint endpoint){
    this.linkCable.connectSlave(endpoint);
  }

  public LinkCable getLinkCable(){
    return this.linkCable;
  }

  public GBProcessor getCPU(){
    return (GBProcessor)this.cpu;
  }

  public void refreshDisplay(){
    display = cpu.getDisplay();
    buttonPanel.refreshDisplay(display);
  }

  private String formatByte(byte b){
    int i = b & 0xFF;
    return String.format("0x%02X",i);
  }

  public void loop(){
    long startTime = 0;
    int MAX_CYCLES = 69905;
    int cycles = 0;
    int counter = 0;
    long count = 0;
    long time = 0;

    //do a cycle every 1/60th of a second
    while(true){
      while(running){

        startTime = System.nanoTime();

        while(cycles < MAX_CYCLES){
          cpu.decode();
          cycles += cpu.getCurrentCycles();
        }

        cycles = 0;
        refreshDisplay();
        pause(startTime);
        count++;

        time += (System.nanoTime() - startTime);
        if(time >= 1_000_000_000){
          printFPS(count);
//          System.out.println("FPS: "+count);
          time=0;
          count=0;
        }
      }
    }
  }

  public void pause(long start){
    while((System.nanoTime() - start) < (1_000_000_000 / 60)){ //loop until we reach 1/60th of a second which is
      try{
        Thread.sleep(1);
      }catch(Exception e){
        e.printStackTrace();
      }
    }
  }

  public void printFPS(long fps){
    sourceArea.setText(fps+" FPS\n");
    sourceArea.append("Serial transfer byte: "+String.format("0x%02X",(mmu.read(0xFF01)))+"\n");
    sourceArea.append("Serial control: "+String.format("0x%02X",(mmu.read(0xFF02)))+"\n");
    sourceArea.append("Serial id: "+((GBProcessor)cpu).serial.id);
  }

  public void refreshStats(){
    // sourceArea.setText("");
    // sourceArea.append("CPU registers\n");
    // sourceArea.append("A: "+String.format("0x%02X",(registers.A & 0xFF))+"\n");
    // sourceArea.append("F: "+String.format("0x%02X",(registers.F & 0xFF))+"\n");
    // sourceArea.append("B: "+String.format("0x%02X",(registers.B & 0xFF))+"\n");
    // sourceArea.append("C: "+String.format("0x%02X",(registers.C & 0xFF))+"\n");
    // sourceArea.append("D: "+String.format("0x%02X",(registers.D & 0xFF))+"\n");
    // sourceArea.append("E: "+String.format("0x%02X",(registers.E & 0xFF))+"\n");
    // sourceArea.append("H: "+String.format("0x%02X",(registers.H & 0xFF))+"\n");
    // sourceArea.append("L: "+String.format("0x%02X",(registers.L & 0xFF))+"\n");

    // sourceArea.append("Reserved Registers\n");
    // sourceArea.append("HiLo "+String.format("0x%02X",(mmu.specialRegisters[0x00] & 0xFF))+"\n");
    // sourceArea.append("Serial: "+String.format("0x%02X",(mmu.specialRegisters[0x01] & 0xFF))+"\n");
    // sourceArea.append("SIO: "+String.format("0x%02X",(mmu.specialRegisters[0x02] & 0xFF))+"\n");
    // sourceArea.append("Divider: "+String.format("0x%02X",(mmu.specialRegisters[0x04] & 0xFF))+"\n");
    // sourceArea.append("TIMA: "+String.format("0x%02X",(mmu.read(0xFF05) & 0xFF))+"\n");
    // sourceArea.append("TMA: "+String.format("0x%02X",(mmu.read(0xFF06) & 0xFF))+"\n");
    // sourceArea.append("TAC: "+String.format("0x%02X",(mmu.read(0xFF07) & 0xFF))+"\n");
    // sourceArea.append("IF: "+String.format("0x%02X",(mmu.specialRegisters[0x0F] & 0xFF))+"\n");

    //  sourceArea.append("LCDC "+String.format("0x%02X",(cpuLCDC & 0xFF))+"\n");
    //  sourceArea.append("STAT: "+String.format("0x%02X",(gpu.STAT & 0xFF))+"\n");
    //  sourceArea.append("SCX: "+String.format("0x%02X",(gpu.SCX & 0xFF))+"\n");
    //  sourceArea.append("SCY: "+String.format("0x%02X",(gpu.SCY & 0xFF))+"\n");
    //  sourceArea.append("LY: "+String.format("0x%02X", (gpu.LY & 0xFF))+"\n");
    //  sourceArea.append("LYC: "+String.format("0x%02X",(gpu.LYC & 0xFF))+"\n");
    //  sourceArea.append("DMA: "+String.format("0x%02X",(gpu.DMA & 0xFF))+"\n");
    //  sourceArea.append("BGP: "+String.format("0x%02X",(gpu.BGP & 0xFF))+"\n");
    //  sourceArea.append("OBP0 "+String.format("0x%02X",(gpu.OBP0 & 0xFF))+"\n");
    //  sourceArea.append("OBP1: "+String.format("0x%02X",(gpu.OBP1 & 0xFF))+"\n");
    //  sourceArea.append("WY: "+String.format("0x%02X",(gpu.WY & 0xFF))+"\n");
    //  sourceArea.append("WX: "+String.format("0x%02X",(gpu.WX & 0xFF))+"\n");

//   sourceArea.append("LCDC "+String.format("0x%02X",(gpu.getLcdc().getLcdc() & 0xFF))+"\n");
//   sourceArea.append("STAT: "+String.format("0x%02X",(gpu.getLcdc().getStat() & 0xFF))+"\n");
//   sourceArea.append("SCY: "+String.format("0x%02X",(gpu.getLcdc().getSCX() & 0xFF))+"\n");
//   sourceArea.append("SCX: "+String.format("0x%02X",(gpu.getLcdc().getSCY() & 0xFF))+"\n");
//   sourceArea.append("LY: "+String.format("0x%02X", (gpu.getLcdc().getLY() & 0xFF))+"\n");
//   sourceArea.append("LYC: "+String.format("0x%02X",(gpu.getLcdc().getLYC() & 0xFF))+"\n");
//   sourceArea.append("DMA: "+String.format("0x%02X",(gpu.getLcdc().getDMA()& 0xFF))+"\n");
//   sourceArea.append("BGP: "+String.format("0x%02X",(gpu.getLcdc().getBGP() & 0xFF))+"\n");
//   sourceArea.append("OBP0 "+String.format("0x%02X",(gpu.getLcdc().getOBP0() & 0xFF))+"\n");
//   sourceArea.append("OBP1: "+String.format("0x%02X",(gpu.getLcdc().getOBP1()& 0xFF))+"\n");
//   sourceArea.append("WY: "+String.format("0x%02X",(gpu.getLcdc().getWY() & 0xFF))+"\n");
//   sourceArea.append("WX: "+String.format("0x%02X",(gpu.getLcdc().getWX() & 0xFF))+"\n");
    // sourceArea.append("IE: "+String.format("0x%02X",(mmu.specialRegisters[0xFF] & 0xFF))+"\n");

    sourceArea.append("~~~~OPcode info~~~~\n");
    sourceArea.append("Program Counter: "+String.format("0x%02X",(registers.PC & 0xFFFF))+"\n");
    sourceArea.append("Stack Pointer: "+String.format("0x%02X",(registers.SP & 0xFFFF))+"\n");
  }
}
