package com.laconic.gameboy;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.laconic.emulator.Display;
import com.laconic.emulator.Emulator;
import com.laconic.gameboy.gpu.PPU;
import com.laconic.util.*;

import com.laconic.pce.hucard.Hucard;
import com.laconic.pce.io.Gamepad;
import com.laconic.pce.PCEMMU;
import com.laconic.pce.cpu.HuC6280;
import com.laconic.pce.gpu.InterruptControl;
import com.laconic.pce.gpu.Timer;
import com.laconic.pce.gpu.VCE;
import com.laconic.pce.gpu.VDC;

public class GUI extends JFrame implements Runnable{  

  private static final long serialVersionUID = 865320822984443203L;

  private static JTextArea sourceArea;//, listingArea;
  private JButton button, butt, tile;
  private JMenuItem saveMI, resetMI, loadMI, quitMI, breakx, breaky, breaka, breakpc, brk;
  private final JFileChooser jfile;
  private static Display buttonPanel;
  private static Emulator cpu;
//  private static GBProcessor cpu;
  private static GBRegisters registers;
  private static GBMMU mmu;
//  private static GBGPU gpu;
  private static PPU gpu;
  private static LCDController lcdc;
  private static Timer timer;

  private static byte[] tileSet;
  private static byte[] romData;
  private static int ti = 0;

  private static VDC vdc;

  private static boolean stayLooped = true;
  private static boolean breakX = false;
  private static boolean breakY = false;
  private static boolean breakA = false;
  private static boolean breakPC = false;
  private static boolean _brk = false;


  private static int _X;
  private static int _Y;
  private static int _A;
  private static int _PC;

  // private static byte[][] display = new byte[144][160];
  // private static short[][] display = new short[256][512];

  public static enum views{
    VRAM, ZP, STACK;
  }

  private static boolean running = true;
  private static GBJoypad joypad;
  private static Gamepad gamepad;
  private static JPanel textPanel;


  private static GUI frm;
  
  public GUI(Emulator emulator){

    cpu = emulator;
    // gpu = ((GBProcessor)cpu).ppu;
    // cpu.CPUReset();

    setTitle("Probably an Emulator");
    sourceArea = new JTextArea();
    sourceArea.setEditable(false);
    //this is for when we want to print tile maps
//    sourceArea.setCo128);

    button = new JButton("step");
    butt = new JButton("toggle data");
    tile = new JButton("run");

    // buttonPanel = new Display(cpu.getDisplay(),64,32,5);
    // buttonPanel = new Display(new byte[144][160],160,144,3);
    buttonPanel = new Display(new short[512][1024],1024,512,2);

    textPanel = new JPanel(new GridLayout(1, 2));
    textPanel.add(buttonPanel);
    textPanel.add(new JScrollPane(sourceArea));

    Container container = getContentPane();
    container.add(textPanel, BorderLayout.CENTER);
    container.add(button,BorderLayout.SOUTH);
    container.add(tile,BorderLayout.WEST);
    container.add(butt,BorderLayout.EAST);

    saveMI = new JMenuItem("Save");
    resetMI = new JMenuItem("Reset");
    loadMI = new JMenuItem("Load ROM");
    quitMI = new JMenuItem("Quit SkinnyBASIC parser");

    breakx = new JMenuItem("break x");
    breaky = new JMenuItem("break y");
    breaka = new JMenuItem("break a");
    breakpc = new JMenuItem("break pc");
    brk = new JMenuItem("clear break");

    JMenu fileMenu = new JMenu("File");
    fileMenu.add(saveMI);
    fileMenu.add(resetMI);
    fileMenu.add(loadMI);
    fileMenu.addSeparator();
    fileMenu.addSeparator();
    fileMenu.add(quitMI);

    JMenu menu2 = new JMenu("Options");
    menu2.add(breakx);
    menu2.add(breaky);
    menu2.add(breaka);
    menu2.add(breakpc);
    menu2.add(brk);

    JMenuBar bar = new JMenuBar();
    bar.add(fileMenu);
    bar.add(menu2);
    setJMenuBar(bar);
    jfile = new JFileChooser();

    butt.addActionListener(new LoadListingListener());
    button.addActionListener(new SaveListingListener());

    tile.addActionListener(new SaveListener());

    saveMI.addActionListener(new SaveListener());
    resetMI.addActionListener(new ResetListener());
    loadMI.addActionListener(new LoadListener());

    breakx.addActionListener(new BreakpointListener(this,"x"));
    breaky.addActionListener(new BreakpointListener(this,"y"));
    breaka.addActionListener(new BreakpointListener(this,"a"));
    breakpc.addActionListener(new BreakpointListener(this, "pc"));
    brk.addActionListener(new BreakpointListener(this, "clear break"));

    buttonPanel.addKeyListener(new KeyboardListener());
    buttonPanel.requestFocus();
    this.pack();
  }

  @Override
  public void run(){}

  private class LoadListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e){

      jfile.showOpenDialog(GUI.this);
      File rom = jfile.getSelectedFile();
      //  String filename = "/home/matthew/Downloads/PCE_PadTest/PadTest.pce";
      System.out.println(rom.getAbsolutePath());
      destroyPCE();
      PCEinit(rom.getAbsolutePath());
      printStats();
      running = false;
    }
  }

  private class BreakpointListener implements ActionListener{

    GUI g = null;
    String var = null;

    BreakpointListener(GUI gui, String var){
      g = gui;
      this.var=var;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      String val;
      switch(var){
        case "x":
        val = JOptionPane.showInputDialog(g, "set x breakpoint"); //showMessageDialog(g, "set x breakpoint", "breakpoint", JOptionPane.PLAIN_MESSAGE);
        _X = Integer.parseInt(val);
        breakX = true;
        break;

        case "y":
        val = JOptionPane.showInputDialog(g, "set y breakpoint"); //showMessageDialog(g, "set x breakpoint", "breakpoint", JOptionPane.PLAIN_MESSAGE);
        _Y = Integer.parseInt(val);
        breakY = true;
        break;

        case "a":
        val = JOptionPane.showInputDialog(g, "set a breakpoint"); //showMessageDialog(g, "set x breakpoint", "breakpoint", JOptionPane.PLAIN_MESSAGE);
        _A = Integer.parseInt(val);
        breakA = true;
        break;

        case "pc":
        val = JOptionPane.showInputDialog(g, "set pc breakpoint"); //showMessageDialog(g, "set x breakpoint", "breakpoint", JOptionPane.PLAIN_MESSAGE);
        _PC = Integer.parseInt(val);
        breakPC = true;
        break;

        case "clear break":
        _brk = true;
        break;
      }
    }
  }

  private class SaveListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
      running = true;
      buttonPanel.requestFocus();
      System.out.println("run");
    }
  }

  private class ResetListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e){
//      cpu.reset();
      System.out.println("cpu reset succesfully");
    }
  }

  private class TileListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      // running = false;
      cpu.saveState("matthew");
      // cpu.loadState("");
      // running = true;
      buttonPanel.requestFocus();
    }
  }

  private class SaveListingListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      running = false;
      cpu.decode();
      vdc.cycles(((HuC6280)cpu).clock);
      timer.cycle(((HuC6280)cpu).clock);
      printStats();
      refreshDisplay(vdc);
      buttonPanel.requestFocus();
    }
  }

  private class LoadListingListener implements ActionListener
  {
    public void actionPerformed(ActionEvent e)
    {
      //running = false; //stop running
      cpu.loadState("matthew");
      //running = true;
      buttonPanel.requestFocus();
    }
  }

  private class KeyboardListener implements KeyListener{

    public void keyPressed(KeyEvent k){

      switch(k.getKeyCode()){

        case KeyEvent.VK_A: //A
          // joypad.setButton((byte)0b1110);
          gamepad.buttonDown(Gamepad.button.I);
          // System.out.println("a down");
          break;

        case KeyEvent.VK_S: //B
          gamepad.buttonDown(Gamepad.button.II);
          // joypad.setButton((byte)0b1101);
          break;

        case KeyEvent.VK_ENTER: //start
          gamepad.buttonDown(Gamepad.button.RUN);
          // joypad.setButton((byte)0b0111);
          break;

        case KeyEvent.VK_SPACE: //select
          // joypad.setButton((byte)0b1011);
          running = false;
          cpu.decode();
          vdc.cycles(((HuC6280)cpu).clock);
          timer.cycle(((HuC6280)cpu).clock);
          printStats();
          buttonPanel.requestFocus();
          break;

        case KeyEvent.VK_LEFT: //left
          gamepad.buttonDown(Gamepad.button.LEFT);
          break;

        case KeyEvent.VK_RIGHT: //right
          gamepad.buttonDown(Gamepad.button.RIGHT);
          break;

        case KeyEvent.VK_UP: //up
          gamepad.buttonDown(Gamepad.button.UP);
          // joypad.setDirection((byte)0b1011);
          break;

        case KeyEvent.VK_DOWN: //down
          gamepad.buttonDown(Gamepad.button.DOWN);
          // joypad.setDirection((byte)0b0111);
          break;

        default:
        System.out.println(k.getKeyCode());
        break;

      }
    }

    public void keyReleased(KeyEvent k){
      switch(k.getKeyCode()){

        case KeyEvent.VK_A: //A
          // joypad.resetButton((byte)0b1110);
          gamepad.buttonRelease(Gamepad.button.I);
          break;

        case KeyEvent.VK_S: //B
          gamepad.buttonRelease(Gamepad.button.II);
          // joypad.resetButton((byte)0b1101);
          break;

        case KeyEvent.VK_ENTER: //start
          gamepad.buttonRelease(Gamepad.button.RUN);
          // joypad.resetButton((byte)0b0111);
          break;

        case KeyEvent.VK_SPACE: //select
          // joypad.resetButton((byte)0b1011);
          break;

        case KeyEvent.VK_LEFT: //left
          gamepad.buttonRelease(Gamepad.button.LEFT);
          // joypad.resetDirection((byte)0b1101);
          break;

        case KeyEvent.VK_RIGHT: //right
          // joypad.resetDirection((byte)0b1110);
          gamepad.buttonRelease(Gamepad.button.RIGHT);
          break;

        case KeyEvent.VK_UP: //up
          gamepad.buttonRelease(Gamepad.button.UP);
          // joypad.resetDirection((byte)0b1011);
          break;

        case KeyEvent.VK_DOWN: //down
          gamepad.buttonRelease(Gamepad.button.DOWN);
          // joypad.resetDirection((byte)0b0111);
          break;

      }
    }
    public void keyTyped(KeyEvent k){}
  }

  private class QuitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			System.exit(0);
		}
	}

  public static void main(String args[])throws Exception{
    // init();
    // String filename = "C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\Splatterhouse.pce";
    // String filename = "C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\jjandjeff.pce";
    // String filename = "C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\Youkai_Douchuuki.pce";
    // String filename = "C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\Ninja_Spirit.pce";
    // String filename = "C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\Soldier_Blade.pce";
    // String filename = "C:\\Users\\matthew\\Downloads\\PCE_PadTest\\PadTest.pce"; 
    // String filename = "C:\\Users\\matthew\\Projects\\TurboForce\\main.pce";
    String filename = "C:\\Users\\matthew\\Downloads\\tutorial06\\06-05\\TurboForce.pce";

    // String filename = "/home/matthew/projects/emulation_server/ROMS/pce/jjandjeff.pce";
    // String filename = "/home/matthew/projects/emulation_server/ROMS/pce/Splatterhouse.pce";
    // String filename = "/home/matthew/projects/emulation_server/ROMS/pce/Soldier_Blade.pce";
    // String filename = "/home/matthew/projects/emulation_server/ROMS/pce/Youkai_Douchuuki.pce";
    // String filename = "/home/matthew/projects/emulation_server/ROMS/pce/Ninja_Spirit.pce";  

    // String filename = "/home/matthew/projects/jspce/pce_test2.pce";
    // String filename = "/home/matthew/projects/jspce/pce_test.pce";

    PCEinit(filename);
    init();
  }

  public static void init(){
    // GBInterruptManager ime = new GBInterruptManager();
    // joypad = new GBJoypad(ime);
    // cpu = new GBProcessor(joypad, ime);
    // mmu = ((GBProcessor)cpu).mmu; 
    // GUI frm = new GUI(cpu);

    frm = new GUI(cpu);

    frm.setSize(1200,500);
    frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frm.setVisible(true);
    buttonPanel.requestFocus();

    printStats();
    loop(vdc);
  }

  public static void PCEinit(String filename){

    gamepad = new Gamepad();
    Hucard hucard = new Hucard(PceReader.readROM(filename));
    VCE vce = new VCE();
    InterruptControl ic = new InterruptControl();
    PCEMMU mmu = new PCEMMU(hucard,new Timer(ic),ic,gamepad,vce);
    timer = mmu.timer;
    cpu = new HuC6280(mmu,ic);
    vdc = mmu.vdc;

    running = false;
    // stayLooped = true;

    // frm = new GUI(cpu);
    // GUI frm = new GUI(cpu);

    // frm.setSize(1200,500);
    // frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // frm.setVisible(true);
    // running = false;
    // buttonPanel.requestFocus();

    // printStats();
    // loop(vdc);
  }

  public static void destroyPCE(){
    // stayLooped = false;
    running = false;
    cpu = null;
    vdc = null;
  }

  private static void breakCheck(){
    if(breakX){
      if( (((HuC6280)cpu).registers.X & 0xFF) == _X){
        running = false;
        // breakX = false;
      }
    }
    if(breakY){
      if( (((HuC6280)cpu).registers.Y & 0xFF) == _Y){
        running = false;
        // breakY = false;
      }
    }
    if(breakA){
      if( (((HuC6280)cpu).registers.A & 0xFF) == _A){
        running = false;
        // breakA = false;
      }
    }
    if(breakPC){
      if( (((HuC6280)cpu).registers.PC & 0xFFFF) == _PC){
        running = false;
        // breakPC = false;
      }
    }
    if(_brk){
      breakPC = false;
      breakX = false;
      breakA = false;
      breakY = false;
      _brk = false;
    }
  }

  public static void refreshDisplay(){
    // display = cpu.getDisplay();
    // printStats();
    // buttonPanel.refreshDisplay(display);
  }

  public static void refreshDisplay(VDC vdc){
    // display = cpu.getDisplay();

    // short[] vramData = vdc.getTile(0);
    // for(int i=0; i<8; i++){
    //   System.arraycopy(vramData, i*8, display[i], 0, 8);
    // }
    // printStats();
    printStats();
    buttonPanel.refreshDisplay(vdc.getDisplay());
  }

  public static void drawLine(){
    gpu.drawLine();
    buttonPanel.refreshDisplay((gpu.getBackgroundBuffer()));
  }

  public static void loop(VDC vdc){
    System.out.println("entering loop");

    stayLooped = true;
    long startTime = 0;
    int cycles = 0;
    int counter = 0;
    long count = 0;
    long time = 0;
    int speed = ((HuC6280)cpu).speed;
    int clockCycles = 0;
    int MAX_CYCLES = 119_314;

    //do a cycle every 1/60th of a second
    while(stayLooped){
      while(running){

        startTime = System.nanoTime();

        // while(cycles < MAX_CYCLES && running){
        while(!vdc.frameReady()){//we'll see

          cpu.decode();
          // speed = ((HuC6280)cpu).speed;
          clockCycles = ((HuC6280)cpu).clock; //* speed;
          // vdc.vdcRun(4);
          vdc.cycles(clockCycles);
          timer.cycle(clockCycles);
          cycles += clockCycles;
          breakCheck();
        }

        cycles = 0;
        
        // refreshDisplay();
        // System.out.println(cycles);
        counter++;
        pause(startTime);
        refreshDisplay(vdc);
        // count++;
        // MAX_CYCLES = ((21_477_270 / vdc.VCEClock)/60);

        // time += (System.nanoTime() - startTime);
        // if(time >= (1_000_000_000)){
        //   System.out.println("FPS: "+count);
        //   time=0;
        //   count=0;
        // }
      }
      try{
        Thread.sleep(1);
      }catch(Exception e){
        e.printStackTrace();
      }
    }
    System.out.println("exiting loop");
  }

  public static void pause(long start){
    while((System.nanoTime() - start) < (1_000_000_000 / 60)){ //loop until we reach 1/60th of a second which is
      try{
        Thread.sleep(1);
      }catch(Exception e){
        e.printStackTrace();
      }
    }
  }

  public static void printStats(){
    sourceArea.setText("CPU registers\n");
    // sourceArea.append("A: "+String.format("0x%02X",(((GBProcessor)cpu).registers.A & 0xFF))+"\n");
    // sourceArea.append("F: "+String.format("0x%02X",(((GBProcessor)cpu).registers.F & 0xFF))+"\n");
    // sourceArea.append("B: "+String.format("0x%02X",(((GBProcessor)cpu).registers.B & 0xFF))+"\n");
    // sourceArea.append("C: "+String.format("0x%02X",(((GBProcessor)cpu).registers.C & 0xFF))+"\n");
    // sourceArea.append("D: "+String.format("0x%02X",(((GBProcessor)cpu).registers.D & 0xFF))+"\n");
    // sourceArea.append("E: "+String.format("0x%02X",(((GBProcessor)cpu).registers.E & 0xFF))+"\n");
    // sourceArea.append("H: "+String.format("0x%02X",(((GBProcessor)cpu).registers.H & 0xFF))+"\n");
    // sourceArea.append("L: "+String.format("0x%02X",(((GBProcessor)cpu).registers.L & 0xFF))+"\n");
    // sourceArea.append("PC: "+String.format("0x%02X",(((GBProcessor)cpu).registers.PC & 0xFFFF))+"\n");
    // sourceArea.append("Serial transfer byte: "+String.format("0x%02X",(mmu.read(0xFF01)))+"\n");
    // sourceArea.append("Serial control: "+String.format("0x%02X",(mmu.read(0xFF02)))+"\n");
    // sourceArea.append("BGP: "+String.format("0x%02X",(mmu.read(0xFF47) & 0xFF))+"\n");
    // sourceArea.append("OBP0: "+String.format("0x%02X",(mmu.read(0xFF48) & 0xFF))+"\n");
    // sourceArea.append("OBP1: "+String.format("0x%02X",(mmu.read(0xFF49) & 0xFF))+"\n");
    // sourceArea.append("STAT: "+String.format("0x%02X",(mmu.read(0xFF41) & 0xFF))+"\n");
    sourceArea.append(((HuC6280)cpu).registers.toString());
    sourceArea.append("current instruction: "+String.format(Integer.toString(((HuC6280)cpu).currentInstruction&0xFF),"0x%02X")+"\n");
    sourceArea.append("timestamp: "+((HuC6280)cpu).timestamp+"\n");
    // VDC //
    sourceArea.append("~~~VDC~~~\n");
    sourceArea.append("select: "+vdc.getSelect()+"\n");
    sourceArea.append("status: "+vdc.getStatus()+"\n");
    sourceArea.append("MARR: "+vdc.registers[vdc.MARR]+"\n");
    sourceArea.append("MAWR: "+vdc.registers[vdc.MAWR]+"\n");
    sourceArea.append("CR: "+vdc.registers[vdc.CR]+"\n");
    sourceArea.append("RCR: "+vdc.registers[vdc.RCR]+"\n");
    sourceArea.append("BXR: "+vdc.registers[vdc.BXR]+"\n");
    sourceArea.append("BYR: "+vdc.registers[vdc.BYR]+"\n");
    sourceArea.append("MWR: "+vdc.registers[vdc.MWR]+"\n");
    sourceArea.append("VDS: "+vdc.getVDS()+"\n");
    sourceArea.append("VSW: "+vdc.getVSW()+"\n");
    sourceArea.append("VDW: "+vdc.getVDW()+"\n");
    sourceArea.append("VCR: "+vdc.getVCR()+"\n");
    sourceArea.append("HDS: "+vdc.getHDS()+"\n");
    sourceArea.append("HSW: "+vdc.getHSW()+"\n");
    sourceArea.append("HDW: "+vdc.getHDW()+"\n");
    sourceArea.append("HDE: "+vdc.getHDE()+"\n");

    // sourceArea.append("~~~MPR~~~: "+"\n");
    // sourceArea.append("MPR0: "+((HuC6280)cpu).mmu.MRP[0]+"\n");
    // sourceArea.append("MPR1: "+((HuC6280)cpu).mmu.MRP[1]+"\n");
    // sourceArea.append("MPR2: "+((HuC6280)cpu).mmu.MRP[2]+"\n");
    // sourceArea.append("MPR3: "+((HuC6280)cpu).mmu.MRP[3]+"\n");
    // sourceArea.append("MPR4: "+((HuC6280)cpu).mmu.MRP[4]+"\n");
    // sourceArea.append("MPR5: "+((HuC6280)cpu).mmu.MRP[5]+"\n");
    // sourceArea.append("MPR6: "+((HuC6280)cpu).mmu.MRP[6]+"\n");
    // sourceArea.append("MPR7: "+((HuC6280)cpu).mmu.MRP[7]+"\n");

  }

  public static void printFPS(long fps){
    sourceArea.setText(fps+" FPS\n");
    sourceArea.append("CPU registers\n");
    sourceArea.append("A: "+String.format("0x%02X",(((GBProcessor)cpu).registers.A & 0xFF))+"\n");
    sourceArea.append("F: "+String.format("0x%02X",(((GBProcessor)cpu).registers.F & 0xFF))+"\n");
    sourceArea.append("B: "+String.format("0x%02X",(((GBProcessor)cpu).registers.B & 0xFF))+"\n");
    sourceArea.append("C: "+String.format("0x%02X",(((GBProcessor)cpu).registers.C & 0xFF))+"\n");
    sourceArea.append("D: "+String.format("0x%02X",(((GBProcessor)cpu).registers.D & 0xFF))+"\n");
    sourceArea.append("E: "+String.format("0x%02X",(((GBProcessor)cpu).registers.E & 0xFF))+"\n");
    sourceArea.append("H: "+String.format("0x%02X",(((GBProcessor)cpu).registers.H & 0xFF))+"\n");
    sourceArea.append("L: "+String.format("0x%02X",(((GBProcessor)cpu).registers.L & 0xFF))+"\n");
    sourceArea.append("Serial transfer byte: "+String.format("0x%02X",(mmu.read(0xFF01)))+"\n");
    sourceArea.append("Serial control: "+String.format("0x%02X",(mmu.read(0xFF02)))+"\n");
    sourceArea.append("BGP: "+String.format("0x%02X",(mmu.read(0xFF47) & 0xFF))+"\n");
    sourceArea.append("OBP0: "+String.format("0x%02X",(mmu.read(0xFF48) & 0xFF))+"\n");
    sourceArea.append("OBP1: "+String.format("0x%02X",(mmu.read(0xFF49) & 0xFF))+"\n");
    sourceArea.append("STAT: "+String.format("0x%02X",(mmu.read(0xFF41) & 0xFF))+"\n");
  }

  public static void refreshStats(){
    sourceArea.append("~~~~OPcode info~~~~\n");
    sourceArea.append("Program Counter: "+String.format("0x%02X",(registers.PC & 0xFFFF))+"\n");
    sourceArea.append("Stack Pointer: "+String.format("0x%02X",(registers.SP & 0xFFFF))+"\n");
  }
}
