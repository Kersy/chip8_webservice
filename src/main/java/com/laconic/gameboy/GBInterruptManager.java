package com.laconic.gameboy;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;

public class GBInterruptManager implements StatefulComponent {

  /*
  *
  * Interrupt Master Enabe (IME)
  *
  * FFFF -- Interrupt Enable Register
  * FF80 -- Internal RAM
  * FF4C -- Empty
  * FF00 -- I/O ports
  *
  */
  public final static int IE = 0xFFFF; //Interrupt flag
  // Bit 4: Transition from High to Low of Pin number P10-P13.              
  // Bit 3: Serial I/O transfer complete              
  // Bit 2: Timer Overflow              
  // Bit 1: LCDC (see STAT)              
  // Bit 0: V-Blank
  
  public final static int IF = 0xFF0F; //Interrupt enable
  // Bit 4: Transition from High to Low of Pin number P10-P13              
  // Bit 3: Serial I/O transfer complete              
  // Bit 2: Timer Overflow              
  // Bit 1: LCDC (see STAT)              
  // Bit 0: V-Blank

  private byte IFReg = 0;
  private byte IEReg = 0;
  // GBMMU mmu;
  // boolean masterInterrupt = false;

  public enum InterruptType{
    VBLANK, LCDC, TIMER, SERIAL, HILO;
  }

  // public GBInterruptManager(GBMMU mmu){
  //   this.mmu=mmu;
  // }

  public GBInterruptManager(){}

  public void requestInterrupt(InterruptType req){
    // byte r = this.mmu.read(IF);
    IFReg |= (byte)(1 << req.ordinal());
    // this.mmu.write(IF,r);
  }

  public void disableInterrupt(){
    // this.mmu.write(IF,0);
    this.IFReg = 0;
  }

  public int doInterrupt(int pc){
    //this will do the steps necessary for the interrupt and then return the
    //JP location for the CPU
    int newPC = pc;

    byte reqInterrupts = this.IFReg;
    byte enabledInterrupts = this.IEReg;
    // byte reqInterrupts = this.mmu.read(IF);
    // byte enabledInterrupts = this.mmu.read(IE);
    byte interruptBits = (byte) (reqInterrupts & enabledInterrupts);

    if((interruptBits & 0b00000001) != 0){
      newPC = 0x40;
      this.IFReg  = (byte)(this.IFReg & ~ 0b00000001);
      // this.mmu.write(IF,(this.mmu.read(IF) & ~0b00000001));
    }else if((interruptBits & 0b00000010) != 0){
      newPC = 0x48;
      // this.mmu.write(IF,(this.mmu.read(IF) & ~0b00000010));
      this.IFReg  = (byte)(this.IFReg & ~ 0b00000010);
    }else if((interruptBits & 0b00000100) != 0){
      newPC = 0x50;
      // this.mmu.write(IF,(this.mmu.read(IF) & ~0b00000100));
      this.IFReg  = (byte)(this.IFReg & ~ 0b00000100);
    }else if((interruptBits & 0b00001000) != 0){
      newPC= 0x58;
      // this.mmu.write(IF,(this.mmu.read(IF) & ~0b00001000));
      this.IFReg  = (byte)(this.IFReg & ~ 0b00001000);
    }else if((interruptBits & 0b00100000) != 0){
      newPC= 0x60;
      this.IFReg  = (byte)(this.IFReg & ~ 0b00010000);
      // this.mmu.write(IF,(this.mmu.read(IF) & ~0b00010000));
    }
      return newPC;
  }

  public void write(int address, byte data){
    if(address == 0xFFFF){
      this.IEReg = data;
    }else if(address == 0xFF0F){
      this.IFReg = data;
    }else{
      System.out.println("bad address write to IME: "+address);
    }
  }

  public byte read(int address){
    if(address == 0xFFFF){
      return this.IEReg;
    }else if(address == 0xFF0F){
      return this.IFReg;
    }else{
      System.out.println("bad address read from IME: "+address);
      return -1;
    }
  }

  @Override
  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(100_000);
    
    String header = "IME";
    String IFF = "IFF";
    String IEF = "IEF";

    appendBuffer(header, new byte[4], buffer);
    appendByte(IFF, this.IFReg, buffer);
    appendByte(IEF, this.IEReg, buffer);

    return buffer.array();
  }

  @Override
  public void load(byte[] data) {
    byte[] bytelength = new byte[4];
    ByteBuffer buffer = ByteBuffer.wrap(data);

    String header = "IME";
    String IFF = "IFF";
    String IEF = "IEF";
    
    loadData(header,buffer,bytelength);
    loadByte(IFF, buffer);
    loadByte(IEF,buffer);
  }
}
