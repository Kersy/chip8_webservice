package com.laconic.gameboy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import com.laconic.emulator.Emulator;
import com.laconic.emulator.EmulatorState;
import com.laconic.emulator.EmulatorType;
import com.laconic.emulator.StatefulComponent;
import com.laconic.gameboy.controller.Cart;
import com.laconic.gameboy.gpu.PPU;
import com.laconic.gameboy.sound.AudioSystemSoundOutput;
import com.laconic.gameboy.sound.Sound;
import com.laconic.util.GameboyReader;

public class GBProcessor extends EmulatorState implements Emulator, StatefulComponent {

  private final static int MEM_START = 0x100; // start of bios connection
  // private final static int MEM_START = 0x100; //start of cart location
  // private final static int MEM_START = 0x2817; //location of tile instruction
  // in tetris
  private final static int MAX_MEM = 0x8000;
  private final static int CLOCK_SPEED = 4_194_304;
  private boolean isBIOS = true;

  private String currentGameName = "";
  private int dividerUpdate = 0;
  public int currentInstruction = -1;
  private boolean imeflag = false;
  public GBRegisters registers;
  private int lastCycleCount = 0;
  private boolean running = true;

  // private GBGPU gpu;
  public AudioSystemSoundOutput output;
  public PPU ppu;
  public Sound apu;
  public GBTimer timer;
  public GBInterruptManager ime;
  public GBMMU mmu;
  Cart cart;
  public GBJoypad joypad;
  public GBSerial serial;

  /*
   * THIS IS ONLY TEMPORARY WHILE I FIGURE OUT HOW I WANT TO HANDLE THE CYCLES,
   * POSSIBLY SWITCH DECODE TO RET THE NUMBER OF CYCLES?
   */

  public GBProcessor(GBJoypad joypad, GBInterruptManager ime) {
    this.currentGameName = "pkmnbl";
    this.output = new AudioSystemSoundOutput();
    this.ime = ime;
    // cart = GameboyReader.loadCart("C:\\Users\\Matth\\Work\\emulation_server\\ROMS\\gb\\samshow.gb");
    // cart = GameboyReader.loadCart("/home/root/projects/chip8_webservice/ROMS/linksawk.gb");
    cart = GameboyReader.loadCart("/home/matthew/projects/chip8_webservice/ROMS/linksawk.gb");
    this.timer = new GBTimer(ime);
    this.registers = new GBRegisters();
    this.joypad = joypad;
    this.serial = new GBSerial(ime);
    this.mmu = new GBMMU(cart, joypad,timer,serial,ime);
    this.ppu = new PPU(mmu, ime);
    this.apu = new Sound(output,false);
    this.mmu.setPPU(ppu);
    this.mmu.setAPU(apu);
    reset();
  }

  public GBProcessor() {
    this.currentGameName = "pkmnbl";
    this.output = new AudioSystemSoundOutput();
    // cart = GameboyReader.loadCart("/home/root/projects/chip8_webservice/ROMS/linksawk.gb");
    this.ime = new GBInterruptManager();
     cart = GameboyReader.loadCart("/home/matthew/projects/chip8_webservice/ROMS/linksawk.gb");
    this.timer = new GBTimer(ime);
    this.registers = new GBRegisters();
    this.joypad = new GBJoypad(ime);
    this.serial = new GBSerial(ime);
    this.mmu = new GBMMU(cart, joypad,timer,serial,ime);
    this.ppu = new PPU(mmu, ime);
    this.apu = new Sound(output,false);
    this.mmu.setPPU(ppu);
    this.mmu.setAPU(apu);
    reset();
  }

  public void load(String romName){
    this.currentGameName = romName;
    this.output = new AudioSystemSoundOutput();
    cart = GameboyReader.loadCart("/home/root/projects/chip8_webservice/ROMS/linksawk.gb");
    // cart = GameboyReader.loadCart("/home/matthew/projects/chip8_webservice/ROMS/"+romName+".gb");
    this.timer = new GBTimer(ime);
    this.registers = new GBRegisters();
    this.joypad = new GBJoypad(ime);
    this.serial = new GBSerial(ime);
    this.mmu = new GBMMU(cart, joypad,timer,serial,ime);
    this.ppu = new PPU(mmu, ime);
    this.apu = new Sound(output,false);
    this.mmu.setPPU(ppu);
    this.mmu.setAPU(apu);
    reset();
    running = true;
  }

  public void reset() {
    this.registers.PC = MEM_START;
    this.registers.SP = (short) 0xFFFE;
    this.registers.writeBC(0x0013);
    this.registers.writeDE(0x00D8);
    this.registers.writeHL(0x014D); // TODO: MAY HAVE TO CHANGE
    this.mmu.write(0xFF05, 00); // TIMA
    this.mmu.write(0xFF06, 00); // TMA
    this.mmu.write(0xFF07, 00); // TAC
    this.mmu.write(0xFF10, 0x80); // NR10
    this.mmu.write(0xFF11, 0xBF); // NR11
    this.mmu.write(0xFF12, 0xF3); // NR12
    this.mmu.write(0xFF14, 0xBF); // NR14
    this.mmu.write(0xFF16, 0x3F); // NR21
    this.mmu.write(0xFF17, 0x00); // NR22
    this.mmu.write(0xFF19, 0xBF); // NR24
    this.mmu.write(0xFF1A, 0x7F);
    this.mmu.write(0xFF1B, 0xFF); // NR30
    this.mmu.write(0xFF1C, 0x9F); // NR31
    this.mmu.write(0xFF1E, 0xBF); // NR32
    this.mmu.write(0xFF20, 0xFF); // NR33
    this.mmu.write(0xFF21, 0x00);
    this.mmu.write(0xFF22, 0x00);
    this.mmu.write(0xFF23, 0xBF);
    this.mmu.write(0xFF24, 0x77); // NR50
    this.mmu.write(0xFF25, 0xF3); // NR51
    this.mmu.write(0xFF26, 0xF1); // NR52 //F1 for GB, F0 for SGB
    this.mmu.write(0xFF40, 0x91); // LCDC
    this.mmu.write(0xFF42, 0x00); // SCY
    this.mmu.write(0xFF43, 0x00); // SCX
    this.mmu.write(0xFF45, 0x00); // LYC
    this.mmu.write(0xFF47, 0xFC); // BGP
    this.mmu.write(0xFF48, 0xFF); // OBP0
    this.mmu.write(0xFF49, 0xFF); // OBP1
    this.mmu.write(0xFF4A, 0x00); // WY
    this.mmu.write(0xFF4B, 0x00); // WX
    this.mmu.write(0xFFFF, 0x00); // IE

    // this.mmu.write(0xFF02,0x7E);
    this.mmu.write(0xFF00, 0xFF);// 1 = unset, 0 = set in joypad
  }

  public byte[] getMemory() {
    return null;
  }

  public byte[][] getDisplay() {
    return this.ppu.getBackgroundBuffer();
  }

  public String getGameName(){
    return this.currentGameName;
  }

  public void CPUReset() {
  }

  public void clearDisplay() {
  }

  public void releaseKey(int key) {
    switch (key) {
    case 21: // left
      joypad.resetDirection((byte) 0b1101);
      break;

    case 20: // right
      joypad.resetDirection((byte) 0b1110);
      break;

    case 22: // up
      joypad.resetDirection((byte) 0b1011);
      break;

    case 23: // down
      joypad.resetDirection((byte) 0b0111);
      break;

    case 9: // A
      joypad.resetButton((byte) 0b1110);
      break;

    case 10: // B
      joypad.resetButton((byte) 0b1101);
      break;

    case 24: // start
      joypad.resetButton((byte) 0b0111);
      break;

    case 25: // select
      joypad.resetButton((byte) 0b1011);
      break;
    }
  }

  public void setKey(int key) {
    switch (key) {
    case 21: // left
      joypad.setDirection((byte) 0b1101);
      break;

    case 20: // right
      joypad.setDirection((byte) 0b1110);
      break;

    case 22: // up
      joypad.setDirection((byte) 0b1011);
      break;

    case 23: // down
      joypad.setDirection((byte) 0b0111);
      break;

    case 9: // A
      joypad.setButton((byte) 0b1110);
      break;

    case 10: // B
      joypad.setButton((byte) 0b1101);
      break;

    case 24: // start
      joypad.setButton((byte) 0b0111);
      break;

    case 25: // select
      joypad.setButton((byte) 0b1011);
      break;
    }
  }

  public int getCurrentCycles() {
    return timer.currentCycles;
  }

  public void loadToMemory(byte[] data, int romLength) {
    // int len = data.length;
    // System.arraycopy(data, 0, this.internalRAM, MEM_START, romLength);
  }

  public GBRegisters getRegisters() {
    return this.registers;
  }

  public int getRegister(int regNumber) {
    return 0;
  } // this will be an int for the moment

  public EmulatorType getEmulatorType() {
    return EmulatorType.GB;
  }

  public int getMaxWidth() {
    return 0;
  }

  public int getMaxHeight() {
    return 0;
  }

  @Override
  public byte[] getAudioBuffer(){
    return this.apu.getBuffer();
  }

  public void loadState(String username) {
    this.running = false;
    byte[] data = new byte[100_000];
    byte[] smalldata = new byte[1_000];
    try {

      FileInputStream fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/MMU"));
      fis.read(data);
      fis.close();
      mmu.load(data);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/PPU"));
      fis.read(data);
      fis.close();
      ppu.load(data);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/TMR"));
      fis.read(smalldata);
      fis.close();
      timer.load(smalldata);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/REG"));
      fis.read(smalldata);
      fis.close();
      registers.load(smalldata);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/SRL"));
      fis.read(smalldata);
      fis.close();
      serial.load(smalldata);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/IME"));
      fis.read(smalldata);
      fis.close();
      ime.load(smalldata);

      fis = new FileInputStream(path.concat(username+"/"+this.currentGameName+"/CPU"));
      fis.read(smalldata);
      fis.close();
      load(smalldata);

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.running = true;
  }

  @Override
  public String saveState(String username) {
    this.running = false;
    try {
      File file = new File(path.concat(username+"/"+this.currentGameName+"/"));
      if(!file.exists()){
        System.out.println("trying to make "+file.toPath().toString());
        file.mkdirs();
      }
      FileOutputStream fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/MMU"));
      fos.write(mmu.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/PPU"));
      fos.write(ppu.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/TMR"));
      fos.write(timer.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/REG"));
      fos.write(registers.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/SRL"));
      fos.write(serial.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/IME"));
      fos.write(ime.save());
      fos.close();

      fos = new FileOutputStream(path.concat(username+"/"+this.currentGameName+"/CPU"));
      fos.write(save());
      fos.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    this.running = true;
    return "z"; 
  }

  public EmulatorType getEmuType(){
    return this.type;
  }

  void cycles(int numberOfCycles){
    this.lastCycleCount = numberOfCycles;
    this.timer.cycle(numberOfCycles);
    this.ppu.cycle(numberOfCycles);
    for(int i=0; i < numberOfCycles; i++){
      this.apu.tick();
    }
    this.serial.cycle(numberOfCycles);
  }

  public int getCycleCount(){
    return this.lastCycleCount;
  }

  private byte fetch(){
    // if((this.mmu.read(0xFF02) & 0xFF) != 0){
    //   byte[] c = new byte[1];
    //   c[0] = this.mmu.read(0xFF01);
    //   System.out.print(new String(c));
    //   this.mmu.write(0xFF02,0);
    // }
    
    byte fetched = this.mmu.read((this.registers.PC));
    this.currentInstruction = fetched & 0xFF;

    this.registers.PC+=1;
    return fetched;
  }

  // private boolean testInterrupt(){
  //   boolean wasIntrpt = false;
  //   if(this.imeflag){
  //     int rst = this.ime.doInterrupt(this.registers.PC);
  //     if(rst != this.registers.PC){
  //       PUSH();
  //       this.imeflag = false;
  //       wasIntrpt = true;
  //     }else{
  //       wasIntrpt = false;
  //     }
  //     this.registers.PC = (short)rst;
  //   }
  //   return wasIntrpt;
  // }

  public void decode(){
    if(!running){
      return;
    }
    //INTERRUPT SEQ
    if(this.imeflag){
      int rst = this.ime.doInterrupt(this.registers.PC);
      if(rst != this.registers.PC){
        PUSH();
        this.imeflag = false;
      }
      this.registers.PC = (short)rst;
    }
    // testInterrupt();

    byte op = fetch();
    byte cycles = 0;
    int n = 0;
    switch(op & 0xFF){

      case 0x0F:
      cycles = RRCA();
      cycles(cycles);
      break;

      case 0xFB:
      cycles = EI();
      cycles(cycles);
      break;

      case 0x00:
      cycles = NOP();
      cycles(cycles);
      break;

      case 0x06:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x0E:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x16:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x1E:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x26:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x2E:
      cycles = LD_NN(op, fetch());
      cycles(cycles);
      break;

      case 0x7F:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x78:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x79:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x7A:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x7B:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x7C:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x7D:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x7E:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x40:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x41:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x42:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x43:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x44:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x45:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x46:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x48:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x49:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x4A:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x4B:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x4C:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x4D:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x4E:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x50:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x51:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x52:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x53:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x54:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x55:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x56:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x58:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x59:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x5A:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x5B:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x5C:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x5D:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x5E:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x60:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x61:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x62:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x63:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x64:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x65:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x66:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x68:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x69:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x6A:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x6B:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x6C:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x6D:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x6E:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x70:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x71:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x72:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x73:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x74:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x75:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x36:
      cycles = LD_RR(op);
      cycles(cycles);
      break;

      case 0x0A:
      cycles = LD_AN(op);
      cycles(cycles);
      break;

      case 0x1A:
      cycles = LD_AN(op);
      cycles(cycles);
      break;

      case 0xFA:
      // byte mm = fetch();
      // byte ll = fetch();
      cycles = LD_AN(op);
      cycles(cycles);
      break;

      case 0x47:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x4F:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x57:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x5F:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x67:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x6F:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x02:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x12:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0x77:
      cycles = LD_NA(op);
      cycles(cycles);
      break;

      case 0xF2:
      cycles = LD_AC();
      cycles(cycles);
      break;

      case 0xE2:
      cycles = LD_CA();
      cycles(cycles);
      break;

      case 0x3A:
      cycles = LDD_AHL();
      cycles(cycles);
      break;

      case 0x32:
      cycles = LDD_HLA();
      cycles(cycles);
      break;

      case 0x34:
      cycles = INC_N((byte)0x34);
      cycles(cycles);
      break;

      case 0x2A:
      cycles = LDI_AHL();
      cycles(cycles);
      break;

      case 0x22:
      cycles = LDI_HLA();
      cycles(cycles);
      break;

      case 0xE0:
      cycles = LDH_NA(fetch());
      cycles(cycles);
      break;

      case 0xF0:
      cycles = LDH_AN(fetch());
      cycles(cycles);
      break;

      case 0x01:
      cycles = LD_NNN(op);
      cycles(cycles);
      break;

      case 0x11:
      cycles = LD_NNN(op);
      cycles(cycles);
      break;

      case 0x21:
      cycles = LD_NNN(op);
      cycles(cycles);
      break;

      case 0x31:
      cycles = LD_NNN(op); //TODO: STOP HERE
      cycles(cycles);
      break;

      case 0xF9:
      this.registers.SP = this.registers.getHL();
      cycles(cycles);
      break;

      case 0xF8:
      cycles = LDHL_SPN(fetch());
      cycles(cycles);
      break;

      case 0x08:
      cycles = LD_NNSP();
      // LD_NNSP((short)GBUtils.joinBytes(fetch(),fetch()));
      cycles(cycles);
      break;

      case 0xF5:
      cycles = PUSH_NN(0xF5);
      cycles(cycles);
      break;

      case 0xC5:
      cycles = PUSH_NN(0xC5);
      cycles(cycles);
      break;

      case 0xD5:
      cycles = PUSH_NN(0xD5);
      cycles(cycles);
      break;

      case 0xE5:
      cycles = PUSH_NN(0xE5);
      cycles(cycles);
      break;

      case 0xF1:
      cycles = POP_NN(0xF1);
      cycles(cycles);
      break;

      case 0x87:
      cycles = ADD_AN(this.registers.A);
      cycles(cycles);
      break;

      case 0x80:
      cycles = ADD_AN(this.registers.B);
      cycles(cycles);
      break;

      case 0x81:
      cycles = ADD_AN(this.registers.C);
      cycles(cycles);
      break;

      case 0x82:
      cycles = ADD_AN(this.registers.D);
      cycles(cycles);
      break;

      case 0x83:
      cycles = ADD_AN(this.registers.E);
      cycles(cycles);
      break;

      case 0x84:
      cycles = ADD_AN(this.registers.H);
      cycles(cycles);
      break;

      case 0x85:
      cycles = ADD_AN(this.registers.L);
      cycles(cycles);
      break;

      case 0x86:
      cycles = ADD_AN(this.mmu.read(this.registers.getHL()));
      cycles(cycles);
      break;

      case 0x8F:
      cycles = ADC_AN(this.registers.A);
      cycles(cycles);
      break;

      case 0x88:
      cycles = ADC_AN(this.registers.B);
      cycles(cycles);
      break;

      case 0x89:
      cycles = ADC_AN(this.registers.C);
      cycles(cycles);
      break;

      case 0x8A:
      cycles = ADC_AN(this.registers.D);
      cycles(cycles);
      break;

      case 0x8B:
      cycles = ADC_AN(this.registers.E);
      cycles(cycles);
      break;

      case 0x8C:
      cycles = ADC_AN(this.registers.H);
      cycles(cycles);
      break;

      case 0x8D:
      cycles = ADC_AN(this.registers.L);
      cycles(cycles);
      break;

      case 0x8E:
      byte b = this.mmu.read(this.registers.getHL());
      cycles = ADC_AN(b);
      cycles(cycles);
      break;

      case 0xCE:
      cycles = ADC_AN(fetch());
      cycles(cycles);
      break;

      case 0x97:
      cycles = SUB_N(this.registers.A);
      cycles(cycles);
      break;

      case 0x90:
      cycles = SUB_N(this.registers.B);
      cycles(cycles);
      break;

      case 0x91:
      cycles = SUB_N(this.registers.C);
      cycles(cycles);
      break;

      case 0x92:
      cycles = SUB_N(this.registers.D);
      cycles(cycles);
      break;

      case 0x93:
      cycles = SUB_N(this.registers.E);
      cycles(cycles);
      break;

      case 0x94:
      cycles = SUB_N(this.registers.H);
      cycles(cycles);
      break;

      case 0x95:
      cycles = SUB_N(this.registers.L);
      cycles(cycles);
      break;

      case 0x96:
      byte v = this.mmu.read(this.registers.getHL());
      cycles = SUB_N(v);
      cycles(cycles);
      break;

      case 0x9F:
      cycles = SBC_AN(this.registers.A);
      cycles(cycles);
      break;

      case 0x98:
      cycles = SBC_AN(this.registers.B);
      cycles(cycles);
      break;

      case 0x99:
      cycles = SBC_AN(this.registers.C);
      cycles(cycles);
      break;

      case 0x9A:
      cycles = SBC_AN(this.registers.D);
      cycles(cycles);
      break;

      case 0x9B:
      cycles = SBC_AN(this.registers.E);
      cycles(cycles);
      break;

      case 0x9C:
      cycles = SBC_AN(this.registers.H);
      cycles(cycles);
      break;

      case 0x9D:
      cycles = SBC_AN(this.registers.L);
      cycles(cycles);
      break;

      case 0x9E:
      cycles = SBC_AN(this.mmu.read(this.registers.getHL()));
      cycles(cycles);
      break;

      case 0xDE:
      cycles = SBC_AN(fetch());
      cycles(cycles);
      break;

      case 0xA7:
      cycles = AND_N(this.registers.A);
      cycles(cycles);
      break;

      case 0xA0:
      cycles = AND_N(this.registers.B);
      cycles(cycles);
      break;

      case 0xA1:
      cycles = AND_N(this.registers.C);
      cycles(cycles);
      break;

      case 0xA2:
      cycles = AND_N(this.registers.D);
      cycles(cycles);
      break;

      case 0xA3:
      cycles = AND_N(this.registers.E);
      cycles(cycles);
      break;

      case 0xA4:
      cycles = AND_N(this.registers.H);
      cycles(cycles);
      break;

      case 0xA5:
      cycles = AND_N(this.registers.L);
      cycles(cycles);
      break;

      case 0xA6:
      cycles = AND_N(this.mmu.read(this.registers.getHL()));
      cycles(cycles);
      break;

      case 0xB7:
      cycles = OR_N(this.registers.A);
      cycles(cycles);
      break;

      case 0xB0:
      cycles = OR_N(this.registers.B);
      cycles(cycles);
      break;

      case 0xB1:
      cycles = OR_N(this.registers.C);
      cycles(cycles);
      break;

      case 0xB2:
      cycles = OR_N(this.registers.D);
      cycles(cycles);
      break;

      case 0xB3:
      cycles = OR_N(this.registers.E);
      cycles(cycles);
      break;

      case 0xB4:
      cycles = OR_N(this.registers.H);
      cycles(cycles);
      break;

      case 0xB5:
      cycles = OR_N(this.registers.L);
      cycles(cycles);
      break;

      case 0xB6:
      cycles = OR_N(this.mmu.read(this.registers.getHL()));
      cycles(8);
      break;

      case 0xF6:
      cycles = OR_N(fetch());
      cycles(cycles);
      break;

      case 0xAF:
      cycles = XOR_N(this.registers.A);
      cycles(cycles);
      break;

      case 0xA8:
      cycles = XOR_N(this.registers.B);
      cycles(cycles);
      break;

      case 0xA9:
      cycles = XOR_N(this.registers.C);
      cycles(cycles);
      break;

      case 0xAA:
      cycles = XOR_N(this.registers.D);
      cycles(cycles);
      break;

      case 0xAB:
      cycles = XOR_N(this.registers.E);
      cycles(cycles);
      break;

      case 0xAC:
      cycles = XOR_N(this.registers.H);
      cycles(cycles);
      break;

      case 0xAD:
      cycles = XOR_N(this.registers.L);
      cycles(cycles);
      break;

      case 0xAE:
      byte data = this.mmu.read(this.registers.getHL());
      cycles = XOR_N(data);
      cycles(cycles);
      break;

      case 0xEE:
      cycles = XOR_N(fetch());
      cycles(cycles);
      break;

      case 0xBF:
      cycles = CP_N(this.registers.A);
      cycles(cycles);
      break;

      case 0xB8:
      cycles = CP_N(this.registers.B);
      cycles(cycles);
      break;

      case 0xB9:
      cycles = CP_N(this.registers.C);
      cycles(cycles);
      break;

      case 0xBA:
      cycles = CP_N(this.registers.D);
      cycles(cycles);
      break;

      case 0xBB:
      cycles = CP_N(this.registers.E);
      cycles(cycles);
      break;

      case 0xBC:
      cycles = CP_N(this.registers.H);
      cycles(cycles);
      break;

      case 0xBD:
      cycles = CP_N(this.registers.L);
      cycles(cycles);
      break;

      case 0xBE:
      cycles = CP_N(this.mmu.read(this.registers.getHL()));
      cycles(cycles);
      break;

      case 0xFE:
      cycles = CP_N(fetch());
      cycles(cycles);
      break;

      case 0x3C:
      cycles = INC_N(0x3C);
      cycles(cycles);
      break;

      case 0x04:
      cycles = INC_N(0x04);
      cycles(cycles);
      break;

      case 0x0C:
      cycles = INC_N(0x0C);
      cycles(cycles);
      break;

      case 0x14:
      cycles = INC_N(0x14);
      cycles(cycles);
      break;

      case 0x1C:
      cycles = INC_N(0x1C);
      cycles(cycles);
      break;

      case 0x24:
      cycles = INC_N(0x24);
      cycles(cycles);
      break;

      case 0x2C:
      cycles = INC_N(0x2C);
      cycles(cycles);
      break;

      case 0x3D:
      cycles = DEC_N(0x3D);
      cycles(cycles);
      break;

      case 0x05:
      cycles = DEC_N(0x05);
      cycles(cycles);
      break;

      case 0x0D:
      cycles = DEC_N(0x0D);
      cycles(cycles);
      break;

      case 0x15:
      cycles = DEC_N(0x15);
      cycles(cycles);
      break;

      case 0x1D:
      cycles = DEC_N(0x1D);
      cycles(cycles);
      break;

      case 0x25:
      cycles = DEC_N(0x25);
      cycles(cycles);
      break;

      case 0x2D:
      cycles = DEC_N(0x2D);
      cycles(cycles);
      break;
      
      case 0x35:
      cycles = DEC_N(0x35);
      cycles(cycles);
      break;

      case 0x09:
      cycles = ADD_HLN(this.registers.getBC());
      cycles(cycles);
      break;

      case 0x19:
      cycles = ADD_HLN(this.registers.getDE());
      cycles(cycles);
      break;

      case 0x29:
      cycles = ADD_HLN(this.registers.getHL());
      cycles(cycles);
      break;

      case 0x39:
      cycles = ADD_HLN(this.registers.SP);
      cycles(cycles);
      break;

      case 0xE8:
      cycles = ADD_SPN(fetch());
      cycles(cycles);
      break;

      case 0x03:
      cycles = INC_NN((byte)0x03);
      cycles(cycles);
      break;

      case 0x13:
      cycles = INC_NN((byte)0x13);
      cycles(cycles);
      break;

      case 0x23:
      cycles = INC_NN((byte)0x23);
      cycles(cycles);
      break;

      case 0x33:
      this.registers.SP += 1;
      cycles(cycles);
      break;

      case 0x0B:
      this.registers.writeBC(this.registers.getBC() - 1);
      cycles(cycles);
      break;

      case 0x1B:
      this.registers.writeDE(this.registers.getDE() - 1);
      cycles(cycles);
      break;

      case 0x2B:
      this.registers.decHL();
      cycles(cycles);
      break;

      case 0x3B:
      this.registers.decSP();
      cycles(cycles);
      break;

      case 0x3E:
      cycles = LD_AN(op);
      cycles(cycles);
      break;

      case 0x27:
      cycles = DAA();
      cycles(cycles);
      break;

      case 0xC6:
      cycles = ADD_AN(fetch());
      cycles(cycles);
      break;

      case 0xC1:
      cycles = POP_NN(0xC1);
      cycles(cycles);
      break;

      case 0xD1:
      cycles = POP_NN(0xD1);
      cycles(cycles);
      break;

      case 0xE1:
      cycles = POP_NN(0xE1);
      cycles(cycles);
      break;

      case 0x3F:
      cycles = CCF();
      cycles(cycles);
      break;

      case 0x37:
      cycles = SCF();
      cycles(cycles);
      break;

      case 0xC3:
      byte ms = fetch();
      byte ls = fetch();
      cycles = JPNN(GBUtils.joinBytes(ls,ms));
      cycles(cycles);
      break;

      case 0xC2:
      cycles = JPCCNN(0xC2);
      cycles(cycles);
      break;

      case 0xCA:
      cycles = JPCCNN(0xCA);
      cycles(cycles);
      break;

      case 0xD2:
      cycles = JPCCNN(0xD2);
      cycles(cycles);
      break;

      case 0xDA:
      cycles = JPCCNN(0xDA);
      cycles(cycles);
      break;

      case 0xE9:
      this.registers.PC = this.registers.getHL();
      cycles(4);
      break;

      case 0x18:
      byte fetched = fetch();
      this.registers.PC += fetched;
      cycles(12);
      break;

      case 0x20:
      cycles = JRCCN(op);
      cycles(cycles);
      break;

      case 0x28:
      cycles = JRCCN(op);
      cycles(cycles);
      break;

      case 0x30:
      cycles = JRCCN(op);
      cycles(cycles);
      break;

      case 0x38:
      cycles = JRCCN(op);
      cycles(cycles);
      break;

      case 0xCD:
      byte bOne = fetch();
      byte bTwo = fetch();
      cycles = CALLNN(GBUtils.joinBytes(bTwo,bOne));
      cycles(cycles);
      break;

      case 0xC4:
      cycles = CALLCCNN((byte)0xC4);
      cycles(cycles);
      break;

      case 0xCC:
      cycles = CALLCCNN((byte)0xCC);
      cycles(cycles);
      break;

      case 0xD4:
      cycles = CALLCCNN((byte)0xD4);
      cycles(cycles);
      break;

      case 0xDC:
      cycles = CALLCCNN((byte)0xDC);
      cycles(cycles);
      break;

      case 0xC7:
      cycles = RSTN(0x00);
      cycles(cycles);
      break;

      case 0xCF:
      cycles = RSTN(0x08);
      cycles(cycles);
      break;

      case 0xD7:
      cycles = RSTN(0x10);
      cycles(cycles);
      break;

      case 0xDF:
      cycles = RSTN(0x18);
      cycles(cycles);
      break;

      case 0xE7:
      cycles = RSTN(0x20);
      cycles(cycles);
      break;

      case 0xEF:
      cycles = RSTN(0x28);
      cycles(cycles);
      break;

      case 0xF7:
      cycles = RSTN(0x30);
      cycles(cycles);
      break;

      case 0xFF:
      cycles = RSTN(0x38);
      cycles(cycles);
      break;

      case 0xC9:
      cycles = RET();
      cycles(cycles);
      break;

      case 0xC0:
      cycles = RETCC(0xC0);
      cycles(cycles);
      break;

      case 0xC8:
      cycles = RETCC(0xC8);
      cycles(cycles);
      break;

      case 0xD0:
      cycles = RETCC(0xD0);
      cycles(cycles);
      break;

      case 0xD8:
      cycles = RETCC(0xD8);
      cycles(cycles);
      break;

      case 0xD9:
      cycles = RETI();
      cycles(cycles);
      break;

      case 0xEA:
      cycles = LD_RR((byte) 0xEA);
      cycles(cycles);
      break;

      case 0xF3:
      cycles = DI();
      cycles(cycles);
      break;

      case 0xD6:
      cycles = SUB_N(fetch());
      cycles(cycles);
      break;

      case 0x10:
      cycles = STOP();
      cycles(cycles);
      break;

      case 0xE6:
      cycles = AND_N(fetch());
      cycles(cycles);
      break;

      case 0x1F:
      cycles = RRA();
      cycles(cycles);
      break;

      case 0x07:
      cycles = RLCA();
      cycles(cycles);
      break;

      case 0x2F:
      cycles = CPL(); //complete fn's from here on
      cycles(cycles);
      break;

      case 0x17:
      cycles = RLA();
      cycles(cycles);
      break;

      case 0x76:
      cycles = HALT();
      cycles(cycles);
      break;

      case 0xCB:
      byte z = fetch();
      switch(z & 0xFF){        

        case 0x40:
        cycles = BIT_BR(this.registers.B,0);
        cycles(cycles);
        break;

        case 0x41:
        cycles = BIT_BR(this.registers.C,0);
        cycles(cycles);
        break;

        case 0x42:
        cycles = BIT_BR(this.registers.D,0);
        cycles(cycles);
        break;

        case 0x43:
        cycles = BIT_BR(this.registers.E,0);
        cycles(cycles);
        break;

        case 0x44:
        cycles = BIT_BR(this.registers.H,0);
        cycles(cycles);
        break;

        case 0x45:
        cycles = BIT_BR(this.registers.L,0);
        cycles(cycles);
        break;

        case 0x46:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),0);
        cycles(cycles);
        break;

        case 0x47:
        cycles = BIT_BR(this.registers.A,0);
        cycles(cycles);
        break;

        case 0x48:
        cycles = BIT_BR(this.registers.B,1);
        cycles(cycles);
        break;

        case 0x49:
        cycles = BIT_BR(this.registers.C,1);
        cycles(cycles);
        break;

        case 0x4A:
        cycles = BIT_BR(this.registers.D,1);
        cycles(cycles);
        break;

        case 0x4B:
        cycles = BIT_BR(this.registers.E,1);
        cycles(cycles);
        break;

        case 0x4C:
        cycles = BIT_BR(this.registers.H,1);
        cycles(cycles);
        break;

        case 0x4D:
        cycles = BIT_BR(this.registers.L,1);
        cycles(cycles);
        break;

        case 0x4E:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),1);
        cycles(cycles);
        break;

        case 0x4F:
        cycles = BIT_BR(this.registers.A,1);
        cycles(cycles);
        break;

        case 0x50:
        cycles = BIT_BR(this.registers.B,2);
        cycles(cycles);
        break;

        case 0x51:
        cycles = BIT_BR(this.registers.C,2);
        cycles(cycles);
        break;

        case 0x52:
        cycles = BIT_BR(this.registers.D,2);
        cycles(cycles);
        break;

        case 0x53:
        cycles = BIT_BR(this.registers.E,2);
        cycles(cycles);
        break;

        case 0x54:
        cycles = BIT_BR(this.registers.H,2);
        cycles(cycles);
        break;

        case 0x55:
        cycles = BIT_BR(this.registers.L,2);
        cycles(cycles);
        break;

        case 0x56:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),2);
        cycles(cycles);
        break;

        case 0x57:
        cycles = BIT_BR(this.registers.A,2);
        cycles(cycles);
        break;

        case 0x58:
        cycles = BIT_BR(this.registers.B,3);
        cycles(cycles);
        break;

        case 0x59:
        cycles = BIT_BR(this.registers.C,3);
        cycles(cycles);
        break;

        case 0x5A:
        cycles = BIT_BR(this.registers.D,3);
        cycles(cycles);
        break;

        case 0x5B:
        cycles = BIT_BR(this.registers.E,3);
        cycles(cycles);
        break;

        case 0x5C:
        cycles = BIT_BR(this.registers.H,3);
        cycles(cycles);
        break;

        case 0x5D:
        cycles = BIT_BR(this.registers.L,3);
        cycles(cycles);
        break;

        case 0x5E:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),3);
        cycles(cycles);
        break;

        case 0x5F:
        cycles = BIT_BR(this.registers.A,3);
        cycles(cycles);
        break;

        case 0x60:
        cycles = BIT_BR(this.registers.B,4);
        cycles(cycles);
        break;

        case 0x61:
        cycles = BIT_BR(this.registers.C,4);
        cycles(cycles);
        break;

        case 0x62:
        cycles = BIT_BR(this.registers.D,4);
        cycles(cycles);
        break;

        case 0x63:
        cycles = BIT_BR(this.registers.E,4);
        cycles(cycles);
        break;

        case 0x64:
        cycles = BIT_BR(this.registers.H,4);
        cycles(cycles);
        break;

        case 0x65:
        cycles = BIT_BR(this.registers.L,4);
        cycles(cycles);
        break;

        case 0x66:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),4);
        cycles(cycles);
        break;

        case 0x67:
        cycles = BIT_BR(this.registers.A,4);
        cycles(cycles);
        break;

        case 0x68:
        cycles = BIT_BR(this.registers.B,5);
        cycles(cycles);
        break;

        case 0x69:
        cycles = BIT_BR(this.registers.C,5);
        cycles(cycles);
        break;

        case 0x6A:
        cycles = BIT_BR(this.registers.D,5);
        cycles(cycles);
        break;

        case 0x6B:
        cycles = BIT_BR(this.registers.E,5);
        cycles(cycles);
        break;

        case 0x6C:
        cycles = BIT_BR(this.registers.H,5);
        cycles(cycles);
        break;

        case 0x6D:
        cycles = BIT_BR(this.registers.L,5);
        cycles(cycles);
        break;

        case 0x6E:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),5);
        cycles(cycles);
        break;

        case 0x6F:
        cycles = BIT_BR(this.registers.A,5);
        cycles(cycles);
        break;

        case 0x70:
        cycles = BIT_BR(this.registers.B,6);
        cycles(cycles);
        break;

        case 0x71:
        cycles = BIT_BR(this.registers.C,6);
        cycles(cycles);
        break;

        case 0x72:
        cycles = BIT_BR(this.registers.D,6);
        cycles(cycles);
        break;

        case 0x73:
        cycles = BIT_BR(this.registers.E,6);
        cycles(cycles);
        break;

        case 0x74:
        cycles = BIT_BR(this.registers.H,6);
        cycles(cycles);
        break;

        case 0x75:
        cycles = BIT_BR(this.registers.L,6);
        cycles(cycles);
        break;

        case 0x76:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),6);
        cycles(cycles);
        break;

        case 0x77:
        cycles = BIT_BR(this.registers.A,6);
        cycles(cycles);
        break;

        case 0x78:
        cycles = BIT_BR(this.registers.B,7);
        cycles(cycles);
        break;

        case 0x79:
        cycles = BIT_BR(this.registers.C,7);
        cycles(cycles);
        break;

        case 0x7A:
        cycles = BIT_BR(this.registers.D,7);
        cycles(cycles);
        break;

        case 0x7B:
        cycles = BIT_BR(this.registers.E,7);
        cycles(cycles);
        break;

        case 0x7C:
        cycles = BIT_BR(this.registers.H,7);
        cycles(cycles);
        break;

        case 0x7D:
        cycles = BIT_BR(this.registers.L,7);
        cycles(cycles);
        break;

        case 0x7E:
        cycles = BIT_BR(this.mmu.read(this.registers.getHL()),7);
        cycles(cycles);
        break;

        case 0x7F:
        cycles = BIT_BR(this.registers.A,7);
        cycles(cycles);
        break;

        case 0x17: 
        cycles = RLN((byte) 0x17); 
        cycles(cycles);
        break;

        case 0x10: 
        cycles = RLN((byte) 0x10); 
        cycles(cycles);
        break;

        case 0x11: 
        cycles = RLN((byte) 0x11); 
        cycles(cycles);
        break;

        case 0x12: 
        cycles = RLN((byte) 0x12); 
        cycles(cycles);
        break;

        case 0x13: 
        cycles = RLN((byte) 0x13); 
        cycles(cycles);
        break;

        case 0x14: 
        cycles = RLN((byte) 0x14); 
        cycles(cycles);
        break;

        case 0x15: 
        RLN((byte) 0x15); 
        cycles(cycles);
        break;

        case 0x16: 
        cycles = RLN((byte) 0x16);
        cycles(cycles);
        break;

        case 0x07: 
        cycles = RLCN((byte)0x07); 
        cycles(cycles);
        break;

        case 0x00: 
        cycles = RLCN((byte)0x00); 
        cycles(cycles);
        break;

        case 0x01: 
        cycles = RLCN((byte)0x01); 
        cycles(cycles);
        break;

        case 0x02:
        cycles = RLCN((byte)0x02); 
        cycles(cycles);
        break;

        case 0x03: 
        cycles = RLCN((byte)0x03); 
        cycles(cycles);
        break;

        case 0x04:
        cycles = RLCN((byte)0x04); 
        cycles(cycles);
        break;

        case 0x05: 
        cycles = RLCN((byte)0x05); 
        cycles(cycles);
        break;

        case 0x06:
        cycles = RLCN((byte)0x06); 
        cycles(cycles);
        break;

        case 0x0F: 
        cycles = RRCN((byte)0x0F);
        cycles(cycles);
        break;

        case 0x08: 
        cycles = RRCN((byte)0x08);
        cycles(cycles);
        break;

        case 0x09:
        cycles = RRCN((byte)0x09);
        cycles(cycles);
        break;

        case 0x0A: 
        cycles = RRCN((byte)0x0A);
        cycles(cycles);
        break;

        case 0x0B:
        cycles = RRCN((byte)0x0B);
        cycles(cycles);
        break;

        case 0x0C: 
        cycles = RRCN((byte)0x0C);
        cycles(cycles);
        break;

        case 0x0D: 
        cycles = RRCN((byte)0x0D);
        cycles(cycles);
        break;

        case 0x0E:
        cycles = RRCN((byte)0x0E);
        cycles(cycles);
        break;

        case 0x1F: 
        cycles = RRN((byte)0x1F);
        cycles(cycles);
        break;

        case 0x18: 
        cycles = RRN((byte)0x18);
        cycles(cycles);
        break;

        case 0x19:
        cycles = RRN((byte)0x19);
        cycles(cycles);
        break;

        case 0x1A:
        cycles = RRN((byte)0x1A);
        cycles(cycles);
        break;

        case 0x1B:
        cycles = RRN((byte)0x1B);
        cycles(cycles);
        break;

        case 0x1C:
        cycles = RRN((byte)0x1C);
        cycles(cycles);
        break;

        case 0x1E:
        cycles = RRN((byte)0x1E);
        cycles(cycles);
        break;

        case 0x1D:
        cycles = RRN((byte)0x1D);
        cycles(cycles);
        break;

        case 0x20:
        cycles = SLAN((byte)0x20);
        cycles(cycles);
        break;

        case 0x21:
        cycles = SLAN((byte)0x21);
        cycles(cycles);
        break;

        case 0x22:
        cycles = SLAN((byte)0x22);
        cycles(cycles);
        break;

        case 0x23:
        cycles = SLAN((byte)0x23);
        cycles(cycles);
        break;

        case 0x24:
        cycles = SLAN((byte)0x24);
        cycles(cycles);
        break;

        case 0x25:
        cycles = SLAN((byte)0x25);
        cycles(cycles);
        break;

        case 0x26:
        cycles = SLAN((byte)0x26);
        cycles(cycles);
        break;

        case 0x27:
        cycles = SLAN((byte)0x27);
        cycles(cycles);
        break;

        case 0x28:
        cycles = SRAN((byte)0x28);
        cycles(cycles);
        break;

        case 0x29:
        cycles = SRAN((byte)0x29);
        cycles(cycles);
        break;

        case 0x2A:
        cycles = SRAN((byte)0x2A);
        cycles(cycles);
        break;

        case 0x2B:
        cycles = SRAN((byte)0x2B);
        cycles(cycles);
        break;

        case 0x2C:
        cycles = SRAN((byte)0x2C);
        cycles(cycles);
        break;

        case 0x2D:
        cycles = SRAN((byte)0x2D);
        cycles(cycles);
        break;

        case 0x2E:
        cycles = SRAN((byte)0x2E);
        cycles(cycles);
        break;

        case 0x2F:
        cycles = SRAN((byte)0x2F);
        cycles(cycles);
        break;

        case 0x3F: 
        cycles = SRLN((byte)0x3F); 
        cycles(cycles);
        break;

        case 0x38: 
        cycles = SRLN((byte)0x38);
        cycles(cycles);
        break;

        case 0x39:
        cycles = SRLN((byte)0x39);
        cycles(cycles);
        break;

        case 0x3A: 
        cycles = SRLN((byte)0x3A);
        cycles(cycles);
        break;

        case 0x3B: 
        cycles = SRLN((byte)0x3B);
        cycles(cycles);
        break;

        case 0x3C: 
        cycles = SRLN((byte)0x3C);
        cycles(cycles);
        break;

        case 0x3D: 
        cycles = SRLN((byte)0x3D);
        cycles(cycles);
        break;

        case 0x3E:
        cycles = SRLN((byte)0x3E);
        cycles(cycles);
        break;

        case 0x30:
        cycles = SWAP_N((byte)0x30);
        cycles(cycles);
        break;

        case 0x31:
        cycles = SWAP_N((byte)0x31);
        cycles(cycles);
        break;

        case 0x32:
        cycles = SWAP_N((byte)0x32);
        cycles(cycles);
        break;

        case 0x33:
        cycles = SWAP_N((byte)0x33);
        cycles(cycles);
        break;

        case 0x34:
        cycles = SWAP_N((byte)0x34);
        cycles(cycles);
        break;

        case 0x35:
        cycles = SWAP_N((byte)0x35);
        cycles(cycles);
        break;

        case 0x36:
        cycles = SWAP_N((byte)0x36);
        cycles(cycles);
        break;

        case 0x37:
        cycles = SWAP_N((byte)0x37);
        cycles(cycles);
        break;

        case 0x80:
        this.registers.B = RES_BR(this.registers.B, 0);
        cycles(8);
        break;

        case 0x81:
        this.registers.C = RES_BR(this.registers.C, 0);
        cycles(8);
        break;

        case 0x82:
        this.registers.D = RES_BR(this.registers.D, 0);
        cycles(8);
        break;

        case 0x83:
        this.registers.E = RES_BR(this.registers.E, 0);
        cycles(8);
        break;

        case 0x84:
        this.registers.H = RES_BR(this.registers.H, 0);
        cycles(8);
        break;

        case 0x85:
        this.registers.L = RES_BR(this.registers.L, 0);
        cycles(8);
        break;

        case 0x86:
        // this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),0)&0xFF);
        this.mmu.write(this.registers.getHL(),RES_BR(this.mmu.read(this.registers.getHL()),0));
        cycles(16);
        break;

        case 0x87:
        this.registers.A = RES_BR(this.registers.A, 0);
        cycles(8);
        break;

        case 0x88:
        this.registers.B = RES_BR(this.registers.B, 1);
        cycles(8);
        break;

        case 0x89:
        this.registers.C = RES_BR(this.registers.C, 1);
        cycles(8);
        break;

        case 0x8A:
        this.registers.D = RES_BR(this.registers.D, 1);
        cycles(8);
        break;

        case 0x8B:
        this.registers.E = RES_BR(this.registers.E, 1);
        cycles(8);
        break;

        case 0x8C:
        this.registers.H = RES_BR(this.registers.H, 1);
        cycles(8);
        break;

        case 0x8D:
        this.registers.L = RES_BR(this.registers.L, 1);
        cycles(8);
        break;

        case 0x8E:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),1)&0xFF);
        cycles(16);
        break;

        case 0x8F:
        this.registers.A = RES_BR(this.registers.A, 1);
        cycles(8);
        break;

        case 0x90:
        this.registers.B = RES_BR(this.registers.B, 2);
        cycles(8);
        break;

        case 0x91:
        this.registers.C = RES_BR(this.registers.C, 2);
        cycles(8);
        break;

        case 0x92:
        this.registers.D = RES_BR(this.registers.D, 2);
        cycles(8);
        break;

        case 0x93:
        this.registers.E = RES_BR(this.registers.E, 2);
        cycles(8);
        break;

        case 0x94:
        this.registers.H = RES_BR(this.registers.H, 2);
        cycles(8);
        break;

        case 0x95:
        this.registers.L = RES_BR(this.registers.L, 2);
        cycles(8);
        break;

        case 0x96:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),2)&0xFF);
        cycles(16);
        break;

        case 0x97:
        this.registers.A = RES_BR(this.registers.A, 2);
        cycles(8);
        break;

        case 0x98:
        this.registers.B = RES_BR(this.registers.B, 3);
        cycles(8);
        break;

        case 0x99:
        this.registers.C = RES_BR(this.registers.C, 3);
        cycles(8);
        break;

        case 0x9A:
        this.registers.D = RES_BR(this.registers.D, 3);
        cycles(8);
        break;

        case 0x9B:
        this.registers.E = RES_BR(this.registers.E, 3);
        cycles(8);
        break;

        case 0x9C:
        this.registers.H = RES_BR(this.registers.H, 3);
        cycles(8);
        break;

        case 0x9D:
        this.registers.L = RES_BR(this.registers.L, 3);
        cycles(8);
        break;

        case 0x9E:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),3)&0xFF);
        cycles(16);
        break;

        case 0x9F:
        this.registers.A = RES_BR(this.registers.A, 3);
        cycles(8);
        break;

        case 0xA0:
        this.registers.B = RES_BR(this.registers.B, 4);
        cycles(8);
        break;

        case 0xA1:
        this.registers.C = RES_BR(this.registers.C, 4);
        cycles(8);
        break;

        case 0xA2:
        this.registers.D = RES_BR(this.registers.D, 4);
        cycles(8);
        break;

        case 0xA3:
        this.registers.E = RES_BR(this.registers.E, 4);
        cycles(8);
        break;

        case 0xA4:
        this.registers.H = RES_BR(this.registers.H, 4);
        cycles(8);
        break;

        case 0xA5:
        this.registers.L = RES_BR(this.registers.L, 4);
        cycles(8);
        break;

        case 0xA6:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),4)&0xFF);
        cycles(16);
        break;

        case 0xA7:
        this.registers.A = RES_BR(this.registers.A, 4);
        cycles(8);
        break;

        case 0xA8:
        this.registers.B = RES_BR(this.registers.B, 5);
        cycles(8);
        break;

        case 0xA9:
        this.registers.C = RES_BR(this.registers.C, 5);
        cycles(8);
        break;

        case 0xAA:
        this.registers.D = RES_BR(this.registers.D, 5);
        cycles(8);
        break;

        case 0xAB:
        this.registers.E = RES_BR(this.registers.E, 5);
        cycles(8);
        break;

        case 0xAC:
        this.registers.H = RES_BR(this.registers.H, 5);
        cycles(8);
        break;

        case 0xAD:
        this.registers.L = RES_BR(this.registers.L, 5);
        cycles(8);
        break;

        case 0xAE:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),5)&0xFF);
        cycles(16);
        break;

        case 0xAF:
        this.registers.A = RES_BR(this.registers.A, 5);
        cycles(8);
        break;

        case 0xB0:
        this.registers.B = RES_BR(this.registers.B, 6);
        cycles(8);
        break;

        case 0xB1:
        this.registers.C = RES_BR(this.registers.C, 6);
        cycles(8);
        break;

        case 0xB2:
        this.registers.D = RES_BR(this.registers.D, 6);
        cycles(8);
        break;

        case 0xB3:
        this.registers.E = RES_BR(this.registers.E, 6);
        cycles(8);
        break;

        case 0xB4:
        this.registers.H = RES_BR(this.registers.H, 6);
        cycles(8);
        break;

        case 0xB5:
        this.registers.L = RES_BR(this.registers.L, 6);
        cycles(8);
        break;

        case 0xB6:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),6)&0xFF);
        cycles(16);
        break;

        case 0xB7:
        this.registers.A = RES_BR(this.registers.A, 6);
        cycles(8);
        break;

        case 0xB8:
        this.registers.B = RES_BR(this.registers.B, 7);
        cycles(8);
        break;

        case 0xB9:
        this.registers.C = RES_BR(this.registers.C, 7);
        cycles(8);
        break;

        case 0xBA:
        this.registers.D = RES_BR(this.registers.D, 7);
        cycles(8);
        break;

        case 0xBB:
        this.registers.E = RES_BR(this.registers.E, 7);
        cycles(8);
        break;

        case 0xBC:
        this.registers.H = RES_BR(this.registers.H, 7);
        cycles(8);
        break;

        case 0xBD:
        this.registers.L = RES_BR(this.registers.L, 7);
        cycles(8);
        break;

        case 0xBE:
        this.mmu.write(this.registers.getHL(), RES_BR(this.mmu.read(this.registers.getHL()),7)&0xFF);
        cycles(16);
        break;

        case 0xBF:
        this.registers.A = RES_BR(this.registers.A, 7);
        cycles(8);
        break;
        
        case 0xC0:
        this.registers.B = SET(this.registers.B, 0);
        cycles(8);
        break;

        case 0xC1:
        this.registers.C = SET(this.registers.C, 0);
        cycles(8);
        break;

        case 0xC2:
        this.registers.D = SET(this.registers.D, 0);
        cycles(8);
        break;

        case 0xC3:
        this.registers.E = SET(this.registers.E, 0);
        cycles(8);
        break;

        case 0xC4:
        this.registers.H = SET(this.registers.H, 0);
        cycles(8);
        break;

        case 0xC5:
        this.registers.L = SET(this.registers.L, 0);
        cycles(8);
        break;

        case 0xC6:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),0)&0xFF);
        cycles(16);
        break;

        case 0xC7:
        this.registers.A = SET(this.registers.A, 0);
        cycles(8);
        break;

        case 0xC8:
        this.registers.B = SET(this.registers.B, 1);
        cycles(8);
        break;

        case 0xC9:
        this.registers.C = SET(this.registers.C, 1);
        cycles(8);
        break;

        case 0xCA:
        this.registers.D = SET(this.registers.D, 1);
        cycles(8);
        break;

        case 0xCB:
        this.registers.E = SET(this.registers.E, 1);
        cycles(8);
        break;

        case 0xCC:
        this.registers.H = SET(this.registers.H, 1);
        cycles(8);
        break;

        case 0xCD:
        this.registers.L = SET(this.registers.L, 1);
        cycles(8);
        break;

        case 0xCE:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),1)&0xFF);
        cycles(16);
        break;

        case 0xCF:
        this.registers.A = SET(this.registers.A, 1);
        cycles(8);
        break;

        case 0xD0:
        this.registers.B = SET(this.registers.B, 2);
        cycles(8);
        break;

        case 0xD1:
        this.registers.C = SET(this.registers.C, 2);
        cycles(8);
        break;

        case 0xD2:
        this.registers.D = SET(this.registers.D, 2);
        cycles(8);
        break;

        case 0xD3:
        this.registers.E = SET(this.registers.E, 2);
        cycles(8);
        break;

        case 0xD4:
        this.registers.H = SET(this.registers.H, 2);
        cycles(8);
        break;

        case 0xD5:
        this.registers.L = SET(this.registers.L, 2);
        cycles(8);
        break;

        case 0xD6:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),2)&0xFF);
        cycles(16);
        break;

        case 0xD7:
        this.registers.A = SET(this.registers.A, 2);
        cycles(8);
        break;

        case 0xD8:
        this.registers.B = SET(this.registers.B, 3);
        cycles(8);
        break;

        case 0xD9:
        this.registers.C = SET(this.registers.C, 3);
        cycles(8);
        break;

        case 0xDA:
        this.registers.D = SET(this.registers.D, 3);
        cycles(8);
        break;

        case 0xDB:
        this.registers.E = SET(this.registers.E, 3);
        cycles(8);
        break;

        case 0xDC:
        this.registers.H = SET(this.registers.H, 3);
        cycles(8);
        break;

        case 0xDD:
        this.registers.L = SET(this.registers.L, 3);
        cycles(8);
        break;

        case 0xDE:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),3)&0xFF);
        cycles(16);
        break;

        case 0xDF:
        this.registers.A = SET(this.registers.A, 3);
        cycles(8);
        break;

        case 0xE0:
        this.registers.B = SET(this.registers.B, 4);
        cycles(8);
        break;

        case 0xE1:
        this.registers.C = SET(this.registers.C, 4);
        cycles(8);
        break;

        case 0xE2:
        this.registers.D = SET(this.registers.D, 4);
        cycles(8);
        break;

        case 0xE3:
        this.registers.E = SET(this.registers.E, 4);
        cycles(8);
        break;

        case 0xE4:
        this.registers.H = SET(this.registers.H, 4);
        cycles(8);
        break;

        case 0xE5:
        this.registers.L = SET(this.registers.L, 4);
        cycles(8);
        break;

        case 0xE6:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),4)&0xFF);
        cycles(16);
        break;

        case 0xE7:
        this.registers.A = SET(this.registers.A, 4);
        cycles(8);
        break;

        case 0xE8:
        this.registers.B = SET(this.registers.B, 5);
        cycles(8);
        break;

        case 0xE9:
        this.registers.C = SET(this.registers.C, 5);
        cycles(8);
        break;

        case 0xEA:
        this.registers.D = SET(this.registers.D, 5);
        cycles(8);
        break;

        case 0xEB:
        this.registers.E = SET(this.registers.E, 5);
        cycles(8);
        break;

        case 0xEC:
        this.registers.H = SET(this.registers.H, 5);
        cycles(8);
        break;

        case 0xED:
        this.registers.L = SET(this.registers.L, 5);
        cycles(8);
        break;

        case 0xEE:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),5)&0xFF);
        cycles(16);
        break;

        case 0xEF:
        this.registers.A = SET(this.registers.A, 5);
        cycles(8);
        break;

        case 0xF0:
        this.registers.B = SET(this.registers.B, 6);
        cycles(8);
        break;

        case 0xF1:
        this.registers.C = SET(this.registers.C, 6);
        cycles(8);
        break;

        case 0xF2:
        this.registers.D = SET(this.registers.D, 6);
        cycles(8);
        break;

        case 0xF3:
        this.registers.E = SET(this.registers.E, 6);
        cycles(8);
        break;

        case 0xF4:
        this.registers.H = SET(this.registers.H, 6);
        cycles(8);
        break;

        case 0xF5:
        this.registers.L = SET(this.registers.L, 6);
        cycles(8);
        break;

        case 0xF6:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),6)&0xFF);
        cycles(16);
        break;

        case 0xF7:
        this.registers.A = SET(this.registers.A, 6);
        cycles(8);
        break;

        case 0xF8:
        this.registers.B = SET(this.registers.B, 7);
        cycles(8);
        break;

        case 0xF9:
        this.registers.C = SET(this.registers.C, 7);
        cycles(8);
        break;

        case 0xFA:
        this.registers.D = SET(this.registers.D, 7);
        cycles(8);
        break;

        case 0xFB:
        this.registers.E = SET(this.registers.E, 7);
        cycles(8);
        break;

        case 0xFC:
        this.registers.H = SET(this.registers.H, 7);
        cycles(8);
        break;

        case 0xFD:
        this.registers.L = SET(this.registers.L, 7);
        cycles(8);
        break;

        case 0xFE:
        this.mmu.write(this.registers.getHL(), SET(this.mmu.read(this.registers.getHL()),7)&0xFF);
        cycles(16);
        break;

        case 0xFF:
        this.registers.A = SET(this.registers.A, 7);
        cycles(8);
        break;

        default: 
        System.out.println("error after CB OP: "+z); 
        break;
      }
      break;

      default:
      System.out.println("no proper opcode found: "+(op & 0xFF)+" at location: "+(this.registers.PC - 1));
      break;
    }
  }

  /*
  *
  * 16 bit loads
  *
  */

  private byte LD_NN(byte op, byte n){
    switch(op & 0xFF){

      case 0x06: this.registers.B = n; break;

      case 0x0E: this.registers.C = n; break;

      case 0x16:
          this.registers.D = n; break;

      case 0x1E:
          this.registers.E = n; break;

      case 0x26: this.registers.H = n; break;

      case 0x2E: this.registers.L = n; break;

      default: System.out.println("error loading 8 bit command "+(op & 0xFF));
    }
    return 8;
  }

  private byte LD_RR(byte reg){
    // System.out.println("load rr?");
    switch(reg & 0xFF){
      case 0x7F: this.registers.A = this.registers.A; break;

      case 0x78: 
      this.registers.A = this.registers.B; 
      break;

      case 0x79: this.registers.A = this.registers.C; break;

      case 0x7A: this.registers.A = this.registers.D; break;

      case 0x7B: this.registers.A = this.registers.E; break;

      case 0x7C:
      this.registers.A = this.registers.H; break;

      case 0x7D: this.registers.A = this.registers.L; break;

      case 0x7E: this.registers.A = this.mmu.read(this.registers.getHL()); break;

      case 0x40: 
      this.registers.B = this.registers.B; 
      break;

      case 0x41: this.registers.B = this.registers.C; break;

      case 0x42: this.registers.B = this.registers.D; break;

      case 0x43: this.registers.B = this.registers.E; break;

      case 0x44: this.registers.B = this.registers.H; break;

      case 0x45: this.registers.B = this.registers.L; break;

      case 0x46:
      this.registers.B = this.mmu.read(this.registers.getHL());
      break;

      case 0x48: this.registers.C = this.registers.B; break;

      case 0x49: this.registers.C = this.registers.C; break;

      case 0x4A: this.registers.C = this.registers.D; break;

      case 0x4B: this.registers.C = this.registers.E; break;

      case 0x4C: this.registers.C = this.registers.H; break;

      case 0x4D: this.registers.C = this.registers.L; break;

      case 0x4E:
      this.registers.C = this.mmu.read(this.registers.getHL());
      break;

      case 0x50: this.registers.D = this.registers.B; break;

      case 0x51: this.registers.D = this.registers.C; break;

      case 0x52: this.registers.D = this.registers.D; break;

      case 0x53: this.registers.D = this.registers.E; break;

      case 0x54: this.registers.D = this.registers.H; break;

      case 0x55: this.registers.D = this.registers.L; break;

      case 0x56:
      this.registers.D = this.mmu.read(this.registers.getHL());
      break;

      case 0x58: this.registers.E = this.registers.B; break;

      case 0x59: this.registers.E = this.registers.C; break;

      case 0x5A: this.registers.E = this.registers.D; break;

      case 0x5B: this.registers.E = this.registers.E; break;

      case 0x5C: this.registers.E = this.registers.H; break;

      case 0x5D: this.registers.E = this.registers.L; break;

      case 0x5E:
      this.registers.E = this.mmu.read(this.registers.getHL());
      break;

      case 0x60: this.registers.H = this.registers.B; break;

      case 0x61: this.registers.H = this.registers.C; break;

      case 0x62: this.registers.H = this.registers.D; break;

      case 0x63: this.registers.H = this.registers.E; break;

      case 0x64: this.registers.H = this.registers.H; break;

      case 0x65: this.registers.H = this.registers.L; break;

      case 0x66:
      this.registers.H = this.mmu.read(this.registers.getHL());
      break;

      case 0x68: this.registers.L = this.registers.B; break;

      case 0x69: this.registers.L = this.registers.C; break;

      case 0x6A: this.registers.L = this.registers.D; break;

      case 0x6B: this.registers.L = this.registers.E; break;

      case 0x6C: this.registers.L = this.registers.H; break;

      case 0x6D: this.registers.L = this.registers.L; break;

      case 0x6E:
      this.registers.L = this.mmu.read(this.registers.getHL());
      break;

      case 0x70:
      this.mmu.write(this.registers.getHL(),this.registers.B & 0xFF);
      break;

      case 0x71:
      this.mmu.write(this.registers.getHL(),this.registers.C & 0xFF);
      break;

      case 0x72:
      this.mmu.write(this.registers.getHL(),this.registers.D & 0xFF);
      break;

      case 0x73:
      this.mmu.write(this.registers.getHL(),this.registers.E & 0xFF);
      break;

      case 0x74:
      this.mmu.write(this.registers.getHL(),this.registers.H & 0xFF);
      break;

      case 0x75:
      this.mmu.write(this.registers.getHL(),this.registers.L & 0xFF);
      break;

      case 0x36:
      this.mmu.write(this.registers.getHL(),(fetch() & 0xFF)); 
      break;

      case 0xEA:
      byte bOne = fetch();
      byte bTwo = fetch();
      int address = GBUtils.joinBytes(bTwo,bOne);
      this.mmu.write(address,(this.registers.A & 0xFF));
      break;

      default: System.out.println("error loading 8 bit command LDRR: "+(reg&0xFF));
    }
    return 4;
  }

  private byte LD_AN(byte reg){
    switch(reg & 0xFF){

      case 0x0A:
      this.registers.A = this.mmu.read(this.registers.getBC()); 
      break;

      case 0x1A:
      this.registers.A = this.mmu.read(this.registers.getDE()); 
      break;

      case 0x7E:
      this.registers.A = this.mmu.read(this.registers.getHL());
      break;

      case 0xFA:
      byte bOne = fetch(); //7E
      byte bTwo = fetch(); //DF
      this.registers.A =  this.mmu.read(GBUtils.joinBytes(bTwo,bOne));
      break;

      case 0x3E:
      this.registers.A = fetch(); 
      break;

      default: System.out.println("error loading 8 bit command AN: "+(reg&0xFF));
      break;

    }
    return 8;
  }

  private byte LD_NA(byte reg){
    switch(reg & 0xFF){
      case 0x7F: 
      this.registers.A = this.registers.A;
      break;

      case 0x47: 
      this.registers.B = this.registers.A; 
      break;

      case 0x4F: 
      this.registers.C = this.registers.A; 
      break;

      case 0x57: 
      this.registers.D = this.registers.A; 
      break;

      case 0x5F: 
      this.registers.E = this.registers.A; 
      break;

      case 0x67: 
      this.registers.H = this.registers.A; 
      break;

      case 0x6F: 
      this.registers.L = this.registers.A;
      break;

      case 0x02:
      this.mmu.write(this.registers.getBC() & 0xFFFF,this.registers.A&0xFF);
      break;

      case 0x12:
      this.mmu.write(this.registers.getDE() & 0xFFFF,this.registers.A&0xFF);
      break;

      case 0x77:
      this.mmu.write(this.registers.getHL() & 0xFFFF,this.registers.A&0xFF);
      break;

      default: System.out.println("error loading 8 bit command LN: "+(reg&0xFF));
      break;

    }
    return 4;
  }

  private byte LD_AC(){
    int n = (0xFF00) + (this.registers.C & 0xFF);
    byte ret = this.mmu.read(n);
    this.registers.A = ret;
    return 8;
  }

  private byte LD_CA(){
    int n = (0xFF00) + (this.registers.C & 0xFF);
    this.mmu.write(n,this.registers.A&0xFF);
    return 8;
  }

  private byte LDD_AHL(){
    this.registers.A = this.mmu.read(this.registers.getHL());
    this.registers.decHL();
    return 8;
  }

  private byte LDD_HLA(){
    this.mmu.write(this.registers.getHL(),this.registers.A & 0xFF);
    this.registers.decHL();
    return 8;
  }

  private byte LDI_AHL(){
    // System.out.println(this.registers.getHL());
    this.registers.A = this.mmu.read(this.registers.getHL() & 0xFFFF);
    this.registers.incHL();
    return 8;
  }

  private byte LDI_HLA(){
    this.mmu.write(this.registers.getHL(),this.registers.A & 0xFF);
    this.registers.incHL();
    return 8;
  }

  private byte LDH_NA(byte n) {
    int addr = (n & 0xFF) + 0xFF00;
    if (addr == 0xFF50) { //unlock boot
      this.mmu.write((0xFF00 + (n & 0xFF)), (this.registers.A & 0xFF));
      this.registers.PC = 0x100;
    }else if(addr == 0xFF46){ //DMA transfer
      int sourceAddr = (this.registers.A & 0xFF) * 0x100;
      for(int i=0; i<0x9F; i++){
        this.mmu.write(0xFE00+i,this.mmu.read(sourceAddr+i));
      }
    }else{
      this.mmu.write((0xFF00 +(n & 0xFF)),(this.registers.A & 0xFF));
    }
    return 12;
  }

  private byte LDH_AN(byte n){
    byte ld = mmu.read((0xFF00 + (n & 0xFF)));
    this.registers.A = ld;
    return 12;
  }

  /*
  *
  * 16 bit loads
  *
  */

  private byte LD_NNN(byte op){
    byte bOne = fetch();
    byte bTwo = fetch();
    switch(op & 0xFF){

      case 0x01:
      this.registers.writeBC(bTwo,bOne);
      break;

      case 0x11:
      this.registers.writeDE(bTwo,bOne);
      break;

      case 0x21:
      this.registers.writeHL(bTwo,bOne);
      break;

      case 0x31:
      this.registers.SP = (short)GBUtils.joinBytes(bTwo,bOne);
      break;

      default: System.out.println("Error LD_NNN");break;
    }
    return 12;
  }

  private byte LD_SHHL(){
    // this.registers.SP = this.registers.HL;
    this.registers.SP = this.registers.getHL();
    return -1;
  }

  // private void LD_HLSPN(){
  //
  // }

  private byte LDHL_SPN(byte n){

    short result = (short)((this.registers.SP & 0xFFFF) + n);

    this.registers.resetZeroFlag();
    this.registers.resetSubtractFlag();

    if(((this.registers.SP & 0xF) + (n & 0xF)) > 0xF){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }
    
    if(((this.registers.SP & 0xFF) + (n & 0xFF)) > 0xFF){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.writeHL(result);
    return 12;
  }

  private byte LD_NNSP(){
    byte bOne = fetch();
    byte bTwo = fetch();
    int n = GBUtils.joinBytes(bTwo,bOne);
    byte bytes[] = GBUtils.splitBytes(this.registers.SP);
    //DFEF
    this.mmu.write(n,bytes[1]);
    this.mmu.write(n+1,bytes[0]);
    return 20;
  }

  public byte PUSH(){
    byte[] bytes = GBUtils.splitBytes(this.registers.PC);
    this.mmu.write((--this.registers.SP & 0xFFFF),(bytes[0]) & 0xFF);
    this.mmu.write((--this.registers.SP & 0xFFFF),(bytes[1]) & 0xFF);
    return 16;
  }

  public byte PUSH_NN(int op){
    switch(op){

      case 0xF5: //CHANGED
      this.mmu.write(--this.registers.SP, this.registers.A & 0xFF);
      this.mmu.write(--this.registers.SP, this.registers.F & 0xFF);
      break;

      case 0xC5://CHANGED
      this.mmu.write(--this.registers.SP, this.registers.B & 0xFF);
      this.mmu.write(--this.registers.SP, this.registers.C & 0xFF);
      break;

      case 0xD5://CHANGED
      this.mmu.write(--this.registers.SP, this.registers.D & 0xFF);
      this.mmu.write(--this.registers.SP, this.registers.E & 0xFF);
      break;

      case 0xE5://CHANGED
      this.mmu.write(--this.registers.SP, this.registers.H & 0xFF);
      this.mmu.write(--this.registers.SP, this.registers.L & 0xFF);
      break;

      default: System.out.println("Error PUSH_NN");
      break;
    }
    return 16;
  }

  private byte POP_NN(int op){
    // System.out.println("popping: "+(op & 0xFF));
    byte bOne = this.mmu.read(this.registers.SP++);
    byte bTwo = this.mmu.read(this.registers.SP++);
    switch(op){

      case 0xF1://CHANGED    
      // this.registers.writeAF(bOne,(byte)(bTwo & 0xF0));
      this.registers.writeAF(bTwo,(byte)(bOne & 0xF0));
      break;

      case 0xC1://CHANGED
      this.registers.writeBC(bTwo,bOne);
      break;

      case 0xD1://CHANGED
      this.registers.writeDE(bTwo,bOne);
      break;

      case 0xE1://CHANGED
      this.registers.writeHL(bTwo,bOne);
      break;

      default: System.out.println("Error POP_N");
      break;
    }
    return 12;
  }

  /*
  *
  * 8-Bit ALU
  *
  */

  private byte ADD_AN(byte n){
    int result = ((this.registers.A & 0xFF) + (n & 0xFF));
    int hc = (((this.registers.A & 0xF) + (n & 0xF)) & 0x10);

    if(((byte)result) == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.resetSubtractFlag();

    if(hc == 16){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }

    if(result > 0xFF){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.A = (byte)result;
    return 4;
  }

  private byte ADC_AN(byte n){
    byte bit = (byte)((this.registers.isCarryFlagSet())? 1 : 0);
    byte result = (byte)(this.registers.A + n + bit);
    int hc = (((this.registers.A & 0xF) + (n & 0xF) + (bit & 0xF)) & 0x10);

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    if(hc == 0x10){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }
    if(((n & 0xFF) + (bit & 0xFF) + (this.registers.A & 0xFF)) > 0xFF){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.A = (byte) result;
    return 4;
  }

  private byte SUB_N(byte n){
    byte result = (byte)(this.registers.A - n);

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.setSubtractFlag();

    if((n & 0x0F) > (0x0F & this.registers.A)){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }

    if((this.registers.A & 0xFF) < (n & 0xFF)){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    //will need to do the half carry too
    this.registers.A = result;
    return 4;
  }

  private byte SBC_AN(byte n){
    byte c = (byte)((this.registers.isCarryFlagSet())? (byte)1 : (byte)0);
    byte result = (byte)(this.registers.A - n - c);

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.setSubtractFlag();

    if(((n & 0x0F) + c) > (0x0F & this.registers.A)){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }

    if((this.registers.A & 0xFF) < ((n & 0xFF) + c)){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.A = result;
    return 8;
  }

  private byte AND_N(byte n){

    // byte result = (byte)((this.registers.A & 0xFF) & (n & 0xFF));
    byte result = (byte)(this.registers.A & n);

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.setHalfCarryFlag();
    this.registers.resetCarryFlag();

    this.registers.A = result;
    return 4;
  }

  private byte OR_N(byte n){

    byte results = (byte) (this.registers.A | n);

    if(results == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();
    this.registers.resetCarryFlag();

    this.registers.A = results;
    return 4;
  }

  private byte XOR_N(byte n){
    byte result = (byte) (this.registers.A ^ n); //don't think I need AND + Cast
    // System.out.println(String.format("0x%02X",n));
    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();
    this.registers.resetCarryFlag();
    this.registers.A = result;

    return 4;
  }

  private byte CP_N(byte op){
    int results = 0;
    byte comp = op;
    results = (this.registers.A - comp);

    if(this.registers.A == comp){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.setSubtractFlag();

    if(((this.registers.A & 0x0F) - (comp & 0x0F)) < 0){//CORRECT HALF CARRY
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }

    if((this.registers.A & 0xFF) < (comp & 0xFF)){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }
    return 4;
  }

  private byte INC_N(int reg){
    byte result  = 0;
    switch(reg){
      case 0x3C: 
      result = (byte)((this.registers.A + 1));
      this.registers.A++; 
      break;

      case 0x04: 
      result = (byte)((this.registers.B + 1));
      this.registers.B++; 
      break;

      case 0x0C: 
      result = (byte)((this.registers.C + 1));
      this.registers.C++; 
      break;

      case 0x14: 
      result = (byte)((this.registers.D + 1));
      this.registers.D++; 
      break;

      case 0x1C: 
      result = (byte)((this.registers.E + 1));
      this.registers.E++;
      break;

      case 0x24: 
      result = (byte)((this.registers.H + 1));
      this.registers.H++; 
      break;

      case 0x2C: 
      result = (byte)((this.registers.L + 1));
      this.registers.L++; 
      break;

      case 0x34: 
      result = (byte)(this.mmu.read((this.registers.getHL() & 0xFFFF)) + 1);
      this.mmu.write((this.registers.getHL()&0xFFFF),result);
      break;
    }

    if((result & 0xFF) == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    this.registers.resetSubtractFlag();

    if(((result - 1) & 0xF) == 0xF){
      this.registers.setHalfCarryFlag();
     }else{
       this.registers.resetHalfCarryFlag();
     }

     return 4;
  }

  public byte DEC_N(int reg){//FIX
    //THIS SHOULD TAKE IN THE REGISTER IS WANTS TO DEC AND RETURN IF POSSIBLE
    byte result  = 0;
    switch(reg){

      case 0x3D: 
      result = (byte)(this.registers.A - 1);
      this.registers.A--; 
      break;

      case 0x05: 
      result = (byte)(this.registers.B - 1);
      this.registers.B--; 
      break;

      case 0x0D:
      result = (byte)(this.registers.C - 1);
      this.registers.C--; 
      break;

      case 0x15:
      result = (byte)(this.registers.D - 1);
      this.registers.D--; 
      break;

      case 0x1D: 
      result = (byte)(this.registers.E - 1);
      this.registers.E--; 
      break;

      case 0x25:
      result = (byte)(this.registers.H - 1);
      this.registers.H--; 
      break;

      case 0x2D:
      result = (byte)(this.registers.L - 1);
      this.registers.L--; 
      break;

      case 0x35: 
      result = (byte)(this.mmu.read(this.registers.getHL()) - 1);
      this.mmu.write(this.registers.getHL(), result);
      break;
    }

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    this.registers.setSubtractFlag();

    if(((result+1) & 0xF) == 0){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }
    return 8;
  }

  /*
  *
  * 16-bit ALU
  *
  */

  private byte ADD_HLN(short reg){
    short result = (short)(this.registers.getHL() + reg);
    long res = (this.registers.getHL() & 0xFFFF) + (reg & 0xFFFF);

    this.registers.resetSubtractFlag();

    if(((this.registers.getHL() ^ reg ^ result) & 0x1000) != 0){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }

    if(res > 0xFFFF){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.writeHL(result);
    return 8;
  }

  private byte ADD_SPN(byte n){
    short result = (short)((this.registers.SP & 0xFFFF) + n);

    this.registers.resetZeroFlag();
    this.registers.resetSubtractFlag();

    if(((this.registers.SP & 0xF) + (n & 0xF)) > 0xF){
      this.registers.setHalfCarryFlag();
    }else{
      this.registers.resetHalfCarryFlag();
    }
    
    if(((this.registers.SP & 0xFF) + (n & 0xFF)) > 0xFF){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }
    
    this.registers.SP = result;
    return 16;
  }

  private byte INC_NN(byte op){

    // short results = 0;
    switch(op & 0xFF){

      case 0x03:
      short bc = (short)(this.registers.getBC() + 1);
      this.registers.writeBC(bc);
      break;

      case 0x13:
      short de = (short)(this.registers.getDE() + 1);
      this.registers.writeDE(de);
      break;

      case 0x23:
      this.registers.incHL();
      break;

      case 0x33:
      // System.out.println(this.registers.SP);
      ++this.registers.SP;
      break;

    }
    return 8;
  }

  private byte DEC_NN(short op){
    byte[] bytes = new byte[2];
    switch(op){

      case 0x0B:
      bytes = GBUtils.splitBytes((short)GBUtils.joinBytes(this.registers.B,this.registers.C)-1);
      this.registers.B = bytes[1];
      this.registers.C = bytes[0];
      break;

      case 0x1B:
      bytes = GBUtils.splitBytes((short)GBUtils.joinBytes(this.registers.D,this.registers.E)-1);
      this.registers.D = bytes[1];
      this.registers.E = bytes[0];
      break;

      case 0x2B:
      bytes = GBUtils.splitBytes((short)GBUtils.joinBytes(this.registers.H,this.registers.L)-1);
      this.registers.H = bytes[1];
      this.registers.L = bytes[0];
      break;

      case 0x3B:
      --this.registers.SP;
      break;

    }
    return -1;
  }

  /*
  *
  * MISCELLANEOUS
  *
  */

  private byte SWAP_N(byte op){
    switch(op & 0xFF){
      case 0x37: 
      this.registers.A = nibbleSwap(this.registers.A);
      if(this.registers.A==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x30: 
      this.registers.B = nibbleSwap(this.registers.B);
      if(this.registers.B==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x31: 
      this.registers.C = nibbleSwap(this.registers.C);
      if(this.registers.C==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x32: 
      this.registers.D = nibbleSwap(this.registers.D);
      if(this.registers.D==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x33: 
      this.registers.E = nibbleSwap(this.registers.E);
      if(this.registers.E==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x34: 
      this.registers.H = nibbleSwap(this.registers.H);
      if(this.registers.H==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x35: 
      this.registers.L = nibbleSwap(this.registers.L);
      if(this.registers.L==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;

      case 0x36:
      byte data = nibbleSwap(this.mmu.read(this.registers.getHL()));
      // short nibOne = (short) (data >> 8);
      // short nibTwo = (short) ((data & 0x0F) << 8);
      // this.registers.writeHL(data);
      this.mmu.write(this.registers.getHL(),data);
      if(data==0){
        this.registers.setZeroFlag();
      }else{
        this.registers.resetZeroFlag();
      }
      break;
    }
    this.registers.resetHalfCarryFlag();
    this.registers.resetCarryFlag();
    this.registers.resetSubtractFlag();

    return 8;
  }

  public static byte nibbleSwap(byte data){
    int nibOne = (data & 0xFF) >> 4;
    int nibTwo = (data & 0xF) << 4;
    return (byte)(nibOne + nibTwo);
  }

  private byte DAA(){
// note: assumes a is a uint8_t and wraps from 0xff to 0
    if (!this.registers.isSubFlagSet()) {  // after an addition, adjust if (half-)carry occurred or if result is out of bounds
      if (this.registers.isCarryFlagSet() || (this.registers.A & 0xFF) > 0x99) { this.registers.A  += 0x60; this.registers.setCarryFlag(); }
      if (this.registers.isHalfCarryFlagSet() || (this.registers.A  & 0x0f) > 0x09) { this.registers.A  += 0x6; }
    } else {  // after a subtraction, only adjust if (half-)carry occurred
      if (this.registers.isCarryFlagSet()) { this.registers.A  -= 0x60; }
      if (this.registers.isHalfCarryFlagSet()) { this.registers.A  -= 0x6; }
    }
    // these flags are always updated
    if(this.registers.A == 0){ this.registers.setZeroFlag(); }else{ this.registers.resetZeroFlag();} // the usual z flag
    this.registers.resetHalfCarryFlag();; // h flag is always cleared
    return 4;
  }

  private byte CPL(){
    this.registers.A ^= 0b11111111;
    this.registers.setSubtractFlag();
    this.registers.setHalfCarryFlag();
    return 4;
  }

  private byte CCF(){
    //set / unset complimentary flag
    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();
    if(this.registers.isCarryFlagSet()){
      this.registers.resetCarryFlag();
    }else{
      this.registers.setCarryFlag();
    }
    return 4;
  }

  private byte SCF(){
    //set carry flag
    this.registers.F &= 128; //Set highest bit if it's already set, reset everything else
    this.registers.F |= 16; //now set the carry
    return 4;
  }

  private byte NOP(){ return 4;}

  private byte HALT(){ //power down cpu...?
    boolean skipped = false;
    while(!this.imeflag){
      skipped = true;
      cycles(4);
    }
    if(skipped){
      this.registers.PC++;
    }
    return 4;
  }

  private byte STOP(){ //halt CPU and LCD display until a button is pressed
    //stop cycles until
    return 4;
  }

  private byte DI(){ //disable interrupts AFTER this instruction has finished
    this.imeflag = false;
    return 4;
  }

  private  byte EI(){ //enable interrupts after instruction is executed
    this.imeflag = true;
    return 4;
  }

  /*
  *
  * roatate and shifts
  *
  */

  private byte RLA(){
    byte highBit = ((this.registers.A & 0x80) != 0)? (byte)1 : (byte)0;
    byte oldCarry = ((this.registers.isCarryFlagSet()))? (byte)1 : (byte)0;
    this.registers.A = (byte)(this.registers.A << 1);
    this.registers.A |= oldCarry;
    if(highBit > 0){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetZeroFlag();
    this.registers.resetHalfCarryFlag();
    this.registers.resetSubtractFlag();
    return 4;
  }

  private byte RLCA(){ //rotate left through carry flag
    boolean carry = (((this.registers.A & 0xFF) & 0b10000000) == 0x80);
    this.registers.A = (byte)((this.registers.A & 0xFF) << 1);
    if(carry){
      this.registers.setCarryFlag();
      this.registers.A = (byte)(this.registers.A | 1);
    }else{
      this.registers.resetCarryFlag();
    }
    this.registers.resetHalfCarryFlag();
    this.registers.resetZeroFlag();
    this.registers.resetSubtractFlag();
    return 4;
  }

  private byte RRCA(){
    boolean newCarry  = (((this.registers.A & 0xFF) & 0b00000001) == 1);
    this.registers.A = (byte)((this.registers.A & 0xFF) >> 1);
    if(newCarry){
      this.registers.setCarryFlag();
      this.registers.A = (byte)(this.registers.A | 0x80);
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetHalfCarryFlag();
    this.registers.resetZeroFlag();
    this.registers.resetSubtractFlag();
    return 4;
  }

  public byte RRA(){
    byte newCarry = ((this.registers.A & 0x01) != 0)? (byte)1 : (byte)0;
    byte oldCarry = ((this.registers.isCarryFlagSet()))? (byte)0x80 : (byte)0;
    byte shifted = (byte)((this.registers.A & 0xFF) >> 1);
    this.registers.A = shifted;
    this.registers.A |= oldCarry;
    if(newCarry > 0){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetHalfCarryFlag();
    this.registers.resetSubtractFlag();
    this.registers.resetZeroFlag();

    return 4;
  }

  private byte RLCN(byte op){
    int carryBit = 0;
    switch(op & 0xFF){

      case 0x07:
      carryBit = ((this.registers.A & 0x80) == 0x80)? 1 : 0;
      this.registers.A = (byte)(this.registers.A << 1);
      this.registers.A = (byte)(this.registers.A | carryBit);
      if(this.registers.A == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x00:
      carryBit = ((this.registers.B & 0x80) == 0x80)? 1 : 0;
      this.registers.B = (byte)(this.registers.B << 1);
      this.registers.B = (byte)(this.registers.B | carryBit);
      if(this.registers.B == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x01:
      carryBit = ((this.registers.C & 0x80) == 0x80)? 1 : 0;
      this.registers.C = (byte)(this.registers.C << 1);
      this.registers.C = (byte)(this.registers.C | carryBit);
      if(this.registers.C == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x02:
      carryBit = ((this.registers.D & 0x80) == 0x80)? 1 : 0;
      this.registers.D = (byte)(this.registers.D << 1);
      this.registers.D = (byte)(this.registers.D | carryBit);
      if(this.registers.D == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x03:
      carryBit = ((this.registers.E & 0x80) == 0x80)? 1 : 0;
      this.registers.E = (byte)(this.registers.E << 1);
      this.registers.E = (byte)(this.registers.E | carryBit);
      if(this.registers.E == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x04:
      carryBit = ((this.registers.H & 0x80) == 0x80)? 1 : 0;
      this.registers.H = (byte)(this.registers.H << 1);
      this.registers.H = (byte)(this.registers.H | carryBit);
      if(this.registers.H == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x05:
      carryBit = ((this.registers.L & 0x80) == 0x80)? 1 : 0;
      this.registers.L = (byte)(this.registers.L << 1);
      this.registers.L = (byte)(this.registers.L | carryBit);
      if(this.registers.L == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;

      case 0x06:
      byte data = this.mmu.read(this.registers.getHL());
      carryBit = (((data) & 0x80) == 0x80)? 1 : 0;
      data = (byte)(data << 1);
      data = (byte)(data | carryBit);
      this.mmu.write(this.registers.getHL(),data);
      if(this.mmu.read(this.registers.getHL()) == 0){ this.registers.setZeroFlag();}else{ this.registers.resetZeroFlag();}
      break;
    }
    if(carryBit == 1){ 
      this.registers.setCarryFlag(); 
    }else { 
      this.registers.resetCarryFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();

    return 8;
  }

  private byte RLN(byte op){
    byte result = 0;
    byte oldCarry = 0;
    boolean newCarry = false;
    switch(op & 0xFF){

      case 0x17:
      newCarry = (((this.registers.A & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.A = (byte)(this.registers.A << 1);
      this.registers.A |= oldCarry;
      result = this.registers.A;
      break;

      case 0x10:
      newCarry = (((this.registers.B & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.B = (byte)(this.registers.B << 1);
      this.registers.B |= oldCarry;
      result = this.registers.B;
      break;

      case 0x11:
      newCarry = (((this.registers.C & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.C = (byte)(this.registers.C << 1);
      this.registers.C |= oldCarry;
      result = this.registers.C;
      break;

      case 0x12:
      newCarry = (((this.registers.D & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.D = (byte)(this.registers.D << 1);
      this.registers.D |= oldCarry;
      result = this.registers.D;
      break;

      case 0x13:
      newCarry = (((this.registers.E & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.E = (byte)(this.registers.E << 1);
      this.registers.E |= oldCarry;
      result = this.registers.E;
      break;

      case 0x14:
      newCarry = (((this.registers.H & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.H = (byte)(this.registers.H << 1);
      this.registers.H |= oldCarry;
      result = this.registers.H;
      break;

      case 0x15:
      newCarry = (((this.registers.L & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      this.registers.L = (byte)(this.registers.L << 1);
      this.registers.L |= oldCarry;
      result = this.registers.L;
      break;

      case 0x16:
      byte data = this.mmu.read(this.registers.getHL());
      newCarry = (((data & 0xFF) & 0x80) == 0x80);
      oldCarry = (this.registers.isCarryFlagSet())? (byte)1 : (byte)0;
      data = (byte)(data << 1);
      data |= oldCarry;
      result = data;
      this.mmu.write(this.registers.getHL(),data);
      break;
    }

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    if(newCarry){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetHalfCarryFlag();
    this.registers.resetSubtractFlag();

    return 8;
  }

  private byte RRCN(byte op){
    byte result;
    byte newCarry = -1;
    switch(op & 0xFF){

      case 0x08:
      newCarry = ((this.registers.B & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.B = (byte)((this.registers.B & 0xFF) >> 1);
      this.registers.B = (byte)(this.registers.B | newCarry);
      result = this.registers.B;
      break;

      case 0x09:
      newCarry = ((this.registers.C & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.C = (byte)((this.registers.C & 0xFF) >> 1);
      this.registers.C = (byte)(this.registers.C | newCarry);
      result = this.registers.C;
      break;

      case 0x0A:
      newCarry = ((this.registers.D & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.D = (byte)((this.registers.D & 0xFF) >> 1);
      this.registers.D = (byte)(this.registers.D | newCarry);
      result = this.registers.D;
      break;

      case 0x0B:
      newCarry = ((this.registers.E & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.E = (byte)((this.registers.E & 0xFF) >> 1);
      this.registers.E = (byte)(this.registers.E | newCarry);
      result = this.registers.E;
      break;

      case 0x0C:
      newCarry = ((this.registers.H & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.H = (byte)((this.registers.H & 0xFF) >> 1);
      this.registers.H = (byte)(this.registers.H | newCarry);
      result = this.registers.H;
      break;

      case 0x0D:
      newCarry = ((this.registers.L & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.L = (byte)((this.registers.L & 0xFF) >> 1);
      this.registers.L = (byte)(this.registers.L | newCarry);
      result = this.registers.L;
      break;

      case 0x0E:
      byte data = this.mmu.read(this.registers.getHL());
      newCarry = ((data & 0b00000001) == 1)? (byte)0x80 : 0;
      data = (byte) ((data & 0xFF) >> 1);
      data = (byte) (data | newCarry);
      this.mmu.write(this.registers.getHL(),data);
      result = data;
      break;

      case 0x0F:
      newCarry = ((this.registers.A & 0b00000001) == 1)? (byte)0x80 : 0;
      this.registers.A = (byte)((this.registers.A & 0xFF) >> 1);
      this.registers.A = (byte)(this.registers.A | newCarry);
      result = this.registers.A;
      break;

      default: 
      System.out.println("no RRC code found: "+op);
      result = -1;
      newCarry = -1;
    }

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    if((newCarry & 0xFF) != 0){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();

    return 8;
  }

  private byte RRN(byte op){
    byte oldCarry;
    boolean newCarry;
    byte result;

    switch(op & 0xFF){
      case 0x18:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.B & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.B & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.B = result;
      break;

      case 0x19:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.C & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.C & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.C = result;
      break;

      case 0x1A:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.D & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.D & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.D = result;
      break;

      case 0x1B:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.E & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.E & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.E = result;
      break;

      case 0x1C:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.H & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.H & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.H = result;
      break;

      case 0x1D:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.L & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.L & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.L = result;
      break;

      case 0x1E:
      byte data = this.mmu.read(this.registers.getHL());
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((data & 0xFF) & 0b00000001) != 0);
      result = (byte)(((data & 0xFF) >> 1) | oldCarry);
      this.mmu.write(this.registers.getHL(),result & 0xFF);
      break;

      case 0x1F:
      oldCarry = (byte)(this.registers.isCarryFlagSet()? 128: 0);
      newCarry = (((this.registers.A & 0xFF) & 0b00000001) != 0);
      result = (byte)((this.registers.A & 0xFF) >> 1);
      result = (byte)(result | oldCarry);
      this.registers.A = result;
      break;

      default:
      System.out.println("bad RR: "+op);
      result = -1;
      oldCarry = -1;
      newCarry = false;
      break;
    }

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();

    if(newCarry){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }
    
    return 8;
  }

  private byte SLAN(byte op){
    byte result;
    boolean carry;

    switch(op & 0xFF){
      case 0x20:
      carry = ((this.registers.B & 0b10000000) != 0);
      result = (byte)(this.registers.B << 1);
      this.registers.B = result;
      break;

      case 0x21:
      carry = ((this.registers.C & 0b10000000) != 0);
      result = (byte)(this.registers.C << 1);
      this.registers.C = result;
      break;

      case 0x22:
      carry = ((this.registers.D & 0b10000000) != 0);
      result = (byte)(this.registers.D << 1);
      this.registers.D = result;
      break;

      case 0x23:
      carry = ((this.registers.E & 0b10000000) != 0);
      result = (byte)(this.registers.E << 1);
      this.registers.E = result;
      break;

      case 0x24:
      carry = ((this.registers.H & 0b10000000) != 0);
      result = (byte)(this.registers.H << 1);
      this.registers.H = result;
      break;

      case 0x25:
      carry = ((this.registers.L & 0b10000000) != 0);
      result = (byte)(this.registers.L << 1);
      this.registers.L = result;
      break;

      case 0x26:
      carry = ((this.mmu.read(this.registers.getHL()) & 0b10000000) != 0);
      result = (byte)((this.mmu.read(this.registers.getHL())) << 1);
      this.mmu.write(this.registers.getHL(),result);
      break;

      case 0x27:
      carry = ((this.registers.A & 0b10000000) != 0);
      result = (byte)(this.registers.A << 1);
      this.registers.A = result;
      break;

      default:
      System.out.println("SLA error: "+op);
      carry = false;
      result = -1;
      break;
    }

    if(carry){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetHalfCarryFlag();
    this.registers.resetSubtractFlag();

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    return 8;
  }

  private byte SRAN(byte op){
    byte result;
    byte MSB;
    boolean carry;

    switch(op & 0xFF){
      case 0x28:
      carry = ((this.registers.B & 0b00000001) != 0);
      MSB = (byte)(this.registers.B & 0b10000000);
      result = (byte)(this.registers.B >> 1);
      result |= MSB;
      this.registers.B = result;
      break;

      case 0x29:
      carry = ((this.registers.C & 0b00000001) != 0);
      MSB = (byte)(this.registers.C & 0b10000000);
      result = (byte)(this.registers.C >> 1);
      result |= MSB;
      this.registers.C = result;
      break;

      case 0x2A:
      carry = ((this.registers.D & 0b00000001) != 0);
      MSB = (byte)(this.registers.D & 0b10000000);
      result = (byte)(this.registers.D >> 1);
      result |= MSB;
      this.registers.D = result;
      break;

      case 0x2B:
      carry = ((this.registers.E & 0b00000001) != 0);
      MSB = (byte)(this.registers.E & 0b10000000);
      result = (byte)(this.registers.E >> 1);
      result |= MSB;
      this.registers.E = result;
      break;

      case 0x2C:
      carry = ((this.registers.H & 0b00000001) != 0);
      MSB = (byte)(this.registers.H & 0b10000000);
      result = (byte)(this.registers.H >> 1);
      result |= MSB;
      this.registers.H = result;
      break;

      case 0x2D:
      carry = ((this.registers.L & 0b00000001) != 0);
      MSB = (byte)(this.registers.L & 0b10000000);
      result = (byte)(this.registers.L >> 1);
      result |= MSB;
      this.registers.L = result;
      break;

      case 0x2E:
      carry = ((this.mmu.read(this.registers.getHL()) & 0b00000001) != 0);
      MSB = (byte)((this.mmu.read(this.registers.getHL())) & 0b10000000);
      result = (byte)(this.mmu.read(this.registers.getHL()) >> 1);
      result |= MSB;
      this.mmu.write(this.registers.getHL(),result); //TODO: THIS FIX BROKE THE PPU
      break;

      case 0x2F:
      carry = ((this.registers.A & 0b00000001) != 0);
      MSB = (byte)(this.registers.A & 0b10000000);
      result = (byte)(this.registers.A >> 1);
      result |= MSB;
      this.registers.A = result;
      break;

      default:
      System.out.println("SRA error: "+op);
      carry = false;
      result = -1;
      break;
    }

    if(carry){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }

    this.registers.resetHalfCarryFlag();
    this.registers.resetSubtractFlag();

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }
    return 8;
  }

  private byte SRLN(byte op){
    byte result;
    boolean carry;
    switch(op & 0xFF){
      case 0x3F:
      carry = (this.registers.A & 0b00000001) == 1;
      result = (byte)((this.registers.A & 0xFF) >> 1);
      this.registers.A = result;
      break;

      case 0x38:
      carry = (this.registers.B & 0b00000001) == 1;
      result = (byte)((this.registers.B & 0xFF) >> 1);
      this.registers.B = result;
      break;

      case 0x39:
      carry = (this.registers.C & 0b00000001) == 1;
      result = (byte)((this.registers.C & 0xFF) >> 1);
      this.registers.C = result;
      break;

      case 0x3A:
      carry = (this.registers.D & 0b00000001) == 1;
      result = (byte)((this.registers.D & 0xFF) >> 1);
      this.registers.D = result;
      break;

      case 0x3B:
      carry = (this.registers.E & 0b00000001) == 1;
      result = (byte)((this.registers.E & 0xFF) >> 1);
      this.registers.E = result;
      break;

      case 0x3C:
      carry = (this.registers.H & 0b00000001) == 1;
      result = (byte)((this.registers.H & 0xFF) >> 1);
      this.registers.H = result;
      break;

      case 0x3D:
      carry = (this.registers.L & 0b00000001) == 1;
      result = (byte)((this.registers.L & 0xFF) >> 1);
      this.registers.L = result;
      break;

      case 0x3E:
      byte data = this.mmu.read(this.registers.getHL());
      carry = (data & 0b00000001) == 1;
      result = (byte)((data & 0xFF)  >> 1);
      this.mmu.write(this.registers.getHL(),result);
      break;

      default: 
      System.out.println("no SRL code found: "+op);
      result = -1;
      carry = false;
    }

    if(result == 0){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    if(carry){
      this.registers.setCarryFlag();
    }else{
      this.registers.resetCarryFlag();
    }
    this.registers.resetSubtractFlag();
    this.registers.resetHalfCarryFlag();

    return 8;
  }

  /*
  *
  * 16 bit loads
  *
  */

  // private byte BIT_BR(byte op){
  //   boolean result = false;
  //   switch(op & 0xFF){
  //     case 0x7C: //System.out.println("testing bit 7~~~~~");
  //     result = ((this.registers.H &  0x80) == 0x80);
  //     break;
  //   }

  //   if(result){
  //     this.registers.setZeroFlag();
  //   }else{
  //     this.registers.resetZeroFlag();
  //   }

  //   this.registers.resetSubtractFlag();
  //   this.registers.setHalfCarryFlag();
  // }

  private byte BIT_BR(byte reg, int b){
    boolean result = (((reg & 0xFF) & (0b00000001 << b)) == 0);

    if(result){
      this.registers.setZeroFlag();
    }else{
      this.registers.resetZeroFlag();
    }

    this.registers.resetSubtractFlag();
    this.registers.setHalfCarryFlag();
    return 8;
  }

  private byte SET(byte register, int bit){
    return (byte)(register | (1 << bit));
  }

  private byte RES_BR(byte register, int bit){
    byte comp = (byte)((1 << bit) ^ 0xFF);
    return (byte)(register & comp);
  }

  /*
  *
  * jumps
  *
  */

  private byte JPNN(int n){
    this.registers.PC = (short)(n & 0xFFFF); //not sure this & and cast is necessary
    return 16;
  }

  private byte JPCCNN(int n){
    boolean jump = false;
    byte bOne = fetch();
    byte bTwo = fetch();
    switch(n & 0xFF){
      case 0xC2:
      jump = (!this.registers.isZeroFlagSet()); //C1B9
      break;

      case 0xCA:
      jump = (this.registers.isZeroFlagSet());
      break;

      case 0xD2:
      jump = (!this.registers.isCarryFlagSet());
      break;

      case 0xDA:
      jump = (this.registers.isCarryFlagSet());
      break;
    }

    if(jump){
      this.registers.PC = (short) GBUtils.joinBytes(bTwo, bOne);
      return 16;
    }else{
      return 12;
    }
  }

  private void JPHL(){
    //TODO: come back to this?
    this.registers.PC = (short) (this.mmu.read(this.registers.getHL()) & 0xFF);
  }

  private void JRN(){

  }

  private byte JRCCN(byte op){
    boolean jp = false;
    byte n = fetch();
    switch(op & 0xFF){

      case 0x20:
      jp = (!this.registers.isZeroFlagSet())? true : false;
      break;

      case 0x28:
      jp = (this.registers.isZeroFlagSet())? true : false;
      break;

      case 0x30:
      jp = (!this.registers.isCarryFlagSet())? true : false;
      break;

      case 0x38:
      jp = (this.registers.isCarryFlagSet())? true : false;
      break;
    }
    if(jp){
      this.registers.PC += n;
      return 12;
    }else{
      return 8;
    }
  }

  /*
  *
  * calls
  *
  */

  private byte CALLNN(int n){
    //really not sure this is right. The doc says
    //push address of next instruction onto stack and then
    //jump to address nn
    //is would n be the next instruction but also a part of nn
    //or would the next instruction be n and then nn, being the
    //next two bytes to use???

    PUSH();
    JPNN(n);
    return 16;
  }

  private byte CALLCCNN(byte op){
    boolean jump = false;
    byte bOne = fetch();
    byte bTwo = fetch();
    // short join = (short) GBUtils.joinBytes(bTwo,bOne);
    switch(op & 0xFF){

      case 0xC4:
      jump = (!this.registers.isZeroFlagSet());      
      break;

      case 0xCC:
      jump = (this.registers.isZeroFlagSet());
      break;

      case 0xD4:
      jump = (!this.registers.isCarryFlagSet());
      break;

      case 0xDC:
      jump = (this.registers.isCarryFlagSet());
      break;
    }

    if(jump){
      short join = (short) GBUtils.joinBytes(bTwo,bOne);
      CALLNN(join);
      return 24;
    }else{
      return 12;
    }
  }
  /*
  *
  * RST and RET
  *
  */

  private byte RSTN(int n){
    PUSH();
    JPNN(n);
    return 16;
  }

  private byte RET(){
    byte one = this.mmu.read(this.registers.SP++);
    byte two = this.mmu.read(this.registers.SP++);
    this.registers.PC = (short) GBUtils.joinBytes(two,one);
    return 16;
  }

  private byte RETCC(int opcode){
    boolean jump = false;
    
    switch(opcode){
      case 0xC0:
      jump = (!this.registers.isZeroFlagSet());
      break;

      case 0xC8:
      jump = (this.registers.isZeroFlagSet());
      break;

      case 0xD0:
      jump = (!this.registers.isCarryFlagSet());
      break;

      case 0xD8:
      jump = (this.registers.isCarryFlagSet());
      break;

      default:
      System.out.println("error in RETCC: "+opcode);
      break;
    }

    if(jump){
      return (byte)(RET() + 4);
    }else{
      return 8;
    }
  }

  private byte RETI(){
    RET(); //SET PC
    EI(); //ENABLE INTERRUPTS
    return 16;
  }

  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(1_000);

    String header = "CPU";
    String divUpdate = "DVU"; //int
    String curint = "CUR"; //int
    String ifg = "IFG"; //bool
    String lastCy = "LCC";

    appendBuffer(header, new byte[4], buffer);
    appendInt(divUpdate, this.dividerUpdate, buffer);
    appendInt(curint, this.currentInstruction, buffer);
    appendInt(ifg, (imeflag)? 1: 0, buffer);
    appendInt(lastCy, this.lastCycleCount, buffer);

    return buffer.array();
  }

  public void load(byte[] data) {
    ByteBuffer buffer = ByteBuffer.wrap(data);

    String header = "CPU";
    String divUpdate = "DVU"; //int
    String curint = "CUR"; //int
    String ifg = "IFG"; //bool
    String lastCy = "LCC";

    loadData(header, buffer, new byte[4]);
    this.dividerUpdate = loadInt(divUpdate, buffer);
    this.currentInstruction = loadInt(curint, buffer);
    this.imeflag = loadInt(ifg, buffer) == 1;
    this.lastCycleCount = loadInt(lastCy, buffer);
  }
}
