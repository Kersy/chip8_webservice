package com.laconic.gameboy.gpu;

public class HBlank implements Mode{
    private int ticks = 256;

    @Override
    public void begin(){
        this.ticks = 256;
    }

    @Override
    public PPU_MODE getMode(){
        return PPU_MODE.HBLANK;
    }

    @Override
    public boolean tick(int n) {
        return (this.ticks -= n) > 0;
    }
}
