package com.laconic.gameboy.gpu;

public class PTransfer implements Mode{
    private int ticks = 160;

    @Override
    public void begin(){
        this.ticks = 160;
    }

    @Override
    public PPU_MODE getMode(){
        return PPU_MODE.PTRANSFER;
    }

    @Override
    public boolean tick(int n) {
        return (this.ticks -= n) > 0;
    }
}
