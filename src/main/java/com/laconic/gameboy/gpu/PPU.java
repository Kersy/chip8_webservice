package com.laconic.gameboy.gpu;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;
import com.laconic.gameboy.GBInterruptManager;
import com.laconic.gameboy.GBMMU;
import com.laconic.gameboy.Sprite;
import com.laconic.gameboy.Tile;
import com.laconic.gameboy.GBInterruptManager.InterruptType;

public class PPU implements StatefulComponent {

    public byte LCDC = 0;
    public byte STAT = 0;
    public byte SCX = 0;
    public byte SCY = 0;
    public byte LY = 0;
    public byte LYC = 0;
    public byte DMA = 0;
    public byte BGP = 0;
    public byte OBP0 = 0;
    public byte OBP1 = 0;
    public byte WY = 0;
    public byte WX = 0;

    private byte[] OAM = new byte[160]; //0xFE00 - 0xFE9F
    private byte[] tileTableOne = new byte[4096];
    private byte[] tileTableTwo = new byte[4096];
    private byte[] tileMapOne = new byte[1024]; //0x9800 - 0x9BFF
    private byte[] tileMapTwo = new byte[1024]; //0x9C00 - 0x9FFF

    private byte[][] spriteBuffer = new byte[144][160];
    private byte[][] display = new byte[144][160];

    private byte[] visibleSprites = new byte[10];

    private Mode hblank;
    private Mode vblank;
    private Mode ptrans;
    private Mode oamSearch;
    private Mode mode;

    private GBInterruptManager ime;
//    public PPU_MODE phase;

    public int scanline = 0;
    private int totalClocks = 0;

    public PPU(GBMMU mmu, GBInterruptManager ime){
        this.ime = ime;
//        this.mode = PPU_MODE.OAM; //I guess?

        hblank = new HBlank();
        vblank = new VBLank();
        ptrans = new PTransfer();
        oamSearch = new OAMSearch();

        STAT = (byte)((STAT & ~3) | 0x02);
        this.mode = oamSearch;
    }

    public void cycle(int cycles){
        if(!displayOn()){
            this.mode = vblank;
            this.mode.begin();
            this.scanline = 0;
            this.LY = 0;
            // LYCompare();
            return;
        }

        if(this.mode.tick(cycles)){
            if(mode.getMode() == PPU_MODE.VBLANK && this.scanline >= 153){
                this.LY = 0;
                this.scanline = 0;
                this.mode = oamSearch;
                this.mode.begin();
                STAT = (byte)((STAT & ~3) | 0x02);
                LYCompare();
                requestLYCIRQ();
            }
        }else{
            switch(this.mode.getMode()){

                case OAM:
                    oamSearch();
                    this.mode = ptrans;
                    this.mode.begin();
                    STAT = (byte)((STAT & ~3) | 0x03);
                    break;

                case PTRANSFER:
                    drawLine();
                    drawSprites();
                    this.mode = hblank;
                    this.mode.begin();
                    STAT = (byte)((STAT & ~3) | 0x00);
                    requestLCDC(3);
                    break;

                case HBLANK:
                    if(++this.scanline == 144){
                        ++this.LY;
                        this.mode = vblank;
                        this.mode.begin();
                        STAT = (byte)((STAT & ~3) | 0x01);
                        this.ime.requestInterrupt(GBInterruptManager.InterruptType.VBLANK);
                        requestLCDC(4);
                    }else{
                        this.mode = oamSearch;
                        this.mode.begin();
                        ++this.LY;
                        STAT = (byte)((STAT & ~3) | 0x02);
                        requestLCDC(5);
                    }
                    // requestLCDC(5);
                    LYCompare();
                    requestLYCIRQ();
                    break;

                case VBLANK:
                    if(++this.scanline == 1){
                        mode = oamSearch;
                        this.mode.begin();
                        ++this.LY;
                        STAT = (byte)((STAT & ~3) | 0x02);
                        requestLCDC(5);
                    }else{
                        this.mode.begin();
                        ++this.LY;
                        STAT = (byte)((STAT & ~3) | 0x01);
                    }
                    LYCompare();
                    requestLYCIRQ();
                    break;
            }
        }
    }

    private void LYCompare(){
        if(this.LY == this.LYC){
            this.STAT &= ~4;
            this.STAT |= 4;
        }else{
            this.STAT &= ~4;
        }
    }

    private void requestLYCIRQ(){
        if((this.STAT & 0x4) != 0 && getStatIRQMode(6)){
            this.ime.requestInterrupt(InterruptType.LCDC);
        }
    }

    private void requestLCDC(int bit){
        if(getStatIRQMode(bit)){
            this.ime.requestInterrupt(InterruptType.LCDC);
        }
    }

    private boolean getStatIRQMode(int bit){
        return ((this.STAT & (1 << bit)) != 0);
    }

    public byte[][] getSpriteBuffer(){
        return this.spriteBuffer;
    }

    public byte[][] getBackgroundBuffer() {
        return this.display;
    }

    public void drawLine(){
        if(displayOn()){
            int wy = (this.WY & 0xFF);
            int wx = (this.WX & 0xFF) - 7;
            int sy = this.SCY & 0xFF;
            int sx = this.SCX & 0xFF;

            int tileTableNumber = (bgWinTileTableSelect()? 0 : 1);
            byte[] tileMap = bgTileMapSelect()? tileMapTwo : tileMapOne;

            int lineNumber = ((this.scanline + sy) % 8);
            int tileRow = (((this.scanline + sy) / 8) % 32);

            if(windowOn() && scanline >= wy) {
                lineNumber = (this.scanline - wy) % 8;
                tileRow = ((this.scanline - wy) / 8) % 32;
                byte[] winTileMap = (this.winTileMapSelect() ? tileMapTwo : tileMapOne);

                int windowTileStart = ((tileRow * 32) + (wx / 8)) % 32;
                for (int i = windowTileStart; i < 20; i++) {
                    int tileNum = (tileRow * 32) + ((i + (wx / 8)) % 32);
                    Tile tile = getTile(winTileMap[tileNum], tileTableNumber, BGP);
                    byte[] colors = tile.convertRawData()[lineNumber];
                    drawRow(colors, i);
                }
               if(windowTileStart > 0){
                   for(int i=0; i<windowTileStart; i++){
                       int tileNum = (tileRow * 32)+((i+(wx / 8)) % 32);
                       Tile tile = getTile(winTileMap[tileNum],tileTableNumber,BGP);
                       byte[] colors = tile.convertRawData()[lineNumber];
                       drawRow(colors,i);
                   }
               }
            }else{
                for(int i=0; i<20; i++){//need 20 tiles to fill 160pixels
                    int tileNum = (tileRow * 32)+((i+(sx / 8)) % 32);
                    try{
                        Tile tile = getTile(tileMap[tileNum],tileTableNumber,BGP);
                        byte[] colors = tile.convertRawData()[lineNumber];
                        drawRow(colors,i);
                    }catch(Exception e){
                        System.out.println("tileNumber: "+tileNum+", scanline: "+this.scanline);
                    }
                }
            }
        }
    }

    private void drawRow(byte[] row, int i){
        for(int j=0; j<8; j++) {
            display[this.scanline][(8 * i + j)] = row[j];
        }
    }

    public Tile getTile(int tileNumber, int table,byte b){
        byte[] tile = new byte[16];
        byte[] tileMap = new byte[1024];
        switch(table){
          case 0: tileMap = this.tileTableOne;
          break;

          case 1: tileMap = this.tileTableTwo;
          break;
        }
        if(tileNumber < 0){
          tileMap = this.tileTableOne;
          for(int i=0; i<16; i++){
            tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
          }
        }else{
            if(table > 0){
                for(int i=0; i<16; i++){
                    tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)+2047];
                }
            }else{
                for(int i=0; i<16; i++){
                    tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
                  }
            }
        }
        return new Tile(tile,b);
    }

    private void drawSprites(){
        if(spriteDisplayOn()){
            drawSpriteLine();
        }
    }

    public void loadSpritesToDisplay(int index){
        byte bgp = BGP;
        byte paletteZero = (byte)(bgp & 3);

        int y = (OAM[index * 4] & 0xFF) - 16; //y pos of sprite
        int x = (OAM[index * 4 + 1] & 0xFF) - 8; //x pos of sprite
        int tile = (OAM[index * 4 + 2] & 0xFF); //tile to use for sprite
        int attr = OAM[index * 4 + 3]; //attributes of sprite
        boolean flipX = ((attr & 0b00100000) != 0); //ignoring for now
        boolean flipY = ((attr & 0b01000000) != 0); //ignoring for now
        boolean behindBG = ((attr & 0b10000000) != 0); //I guess not paint it if it's behind?
        byte objp = (((attr & 0b00010000) != 0)? OBP1: OBP0); //ignoring for now E4 then C4

        int lineNumber = scanline - y;
        if(y > 0 && x > 0){
            Sprite s;
            s = (spriteSize())? getExtendedSprite(tile,0,objp) : getSprite(tile,0,objp);
    
            byte[][] sprite = s.convertRawData();
            sprite = (flipX)? s.flipX(sprite) : sprite;
            sprite = (flipY)? s.flipY(sprite) : sprite;
    
            byte[] colors = sprite[lineNumber];
    
            for(int i=0; i<8; i++){
                if(x+i > 0 && x+i < 160 && colors[i] != 4){
                    this.display[this.scanline][x+i] = colors[i];
                }
            }
        }
    }

    public Sprite getSprite(int tileNumber, int table,byte b){
        byte[] tile = new byte[16];

        for(int i=0; i<16; i++){
            tile[i] = tileTableOne[i+((tileNumber & 0xFF) * 16)];
        }
        return new Sprite(tile,b);
    }

    public void write(int address, byte data){
        if((address >= 0xFE00) && (address < 0xFEA0)){
            this.OAM[address - 0xFE00] = data;
        }else if((address >= 0x8000) && (address <= 0x8FFF)){
            this.tileTableOne[address - 0x8000] = data;
        }else if(address <= 0x97FF){
            this.tileTableTwo[(address - 0x9000) + 2047] = data;
        }else if ((address >= 0x9800) && (address < 0x9C00)){
            this.tileMapOne[address - 0x9800] = data;
        }else if ((address >= 0x9C00) && (address < 0xA000)){
            this.tileMapTwo[address - 0x9C00] = data;
        }else{
            System.out.println("GPU - attempting to write outside of VRAM: "+address);
        }
    }

    public byte read(int address) {
        if((address >= 0xFE00) && (address < 0xFEA0)){
            return (this.OAM[address - 0xFE00]);
        }else if((address >= 0x8000) && (address <= 0x8FFF)){
            return this.tileTableOne[address - 0x8000];
        }else if(address <= 0x97FF){
            return this.tileTableTwo[(address - 0x9000) + 2047];
        }else if ((address < 0x9C00)){
            return this.tileMapOne[address - 0x9800];
        }else if ((address < 0xA000)){
            return this.tileMapTwo[address - 0x9C00];
        }else{
            System.out.println("GPU - attempting to read outside of VRAM: "+address);
            return (byte)0xFF;
        }
    }

    final public Sprite getExtendedSprite(int tileNumber, int table, byte p){
        byte[] tile = new byte[32];
        for(int i=0; i<32; i++){
            tile[i] = tileTableOne[i+((tileNumber & 0xFF) * 16)];
        }
        return new Sprite(tile,p);
    }

    private boolean checkBit(int bit){ return (((LCDC & 0xFF) & (1 << bit)) != 0); }

    public boolean displayOn(){ return checkBit(7); }

    public boolean winTileMapSelect(){ return checkBit(6); }

    public boolean windowOn(){ return checkBit(5); }

    public boolean bgWinTileTableSelect(){ return checkBit(4); }

    public boolean bgTileMapSelect(){ return checkBit(3); }

    public boolean spriteSize(){ return checkBit(2); }

    public boolean spriteDisplayOn(){ return checkBit(1); }

    public boolean bgAndWindowDisplay(){ return checkBit(0); }

    private void oamSearch(){
        int spriteCounter = 0;
        this.visibleSprites = new byte[10];
        int lLy = (this.LY & 0xFF);
        int sY;
        int doubleSize = (spriteSize())? 2 : 1;

        for(int i=0; i<40; i++){
            sY = (this.OAM[i * 4] & 0xFF) - 16;
            if((lLy - sY) < (8 * doubleSize) && (lLy - sY) >= 0 && spriteCounter < 10){
                this.visibleSprites[spriteCounter++] = (byte)(i+1);//because we use zeroth index
            }
        }
    }

    private void drawSpriteLine(){
        for(int i=0; i<visibleSprites.length; i++){
            if(visibleSprites[i] != 0){
                loadSpritesToDisplay(visibleSprites[i]-1);
            }
        }
    }

    @Override
    public byte[] save() {
        ByteBuffer buffer = ByteBuffer.allocate(100_000);
        //DON'T CARE ABOUT DMA, I DON'T THINK
        String PPU = "PPU";
        String LCD = "LCD";
        String STA = "STA";
        String SCX ="SCX";
        String SCY= "SCY";
        String SLY = "SLY";
        String LYC = "LYC";
        String BGP = "BGP";
        String OJ1 = "OJ1";
        String OJ2 = "OJ2";
        String SWY ="SWY";
        String SWX = "SWX";
        String OM = "OAM";
        String TT1 = "TTO";
        String TT2 = "TTT";
        String TM1 = "TMO";
        String TM2 = "TMT";

        String MODE = "MOD";
        String SCN = "SCN";
        String CLK = "CLK";
        
        appendBuffer(PPU, new byte[4], buffer);
        appendByte(LCD, this.LCDC, buffer);
        appendByte(STA, this.STAT, buffer);
        appendByte(SCX, this.SCX, buffer);
        appendByte(SCY, this.SCY, buffer);
        appendByte(SLY, this.LY, buffer);
        appendByte(LYC, this.LYC, buffer);
        appendByte(BGP, this.BGP, buffer);
        appendByte(OJ1, this.OBP0, buffer);
        appendByte(OJ2, this.OBP1, buffer);
        appendByte(SWY, this.WY, buffer);
        appendByte(SWX, this.WX, buffer);
        appendBuffer(OM, this.OAM, buffer);
        appendBuffer(TT1, this.tileTableOne, buffer);
        appendBuffer(TT2, this.tileTableTwo, buffer);
        appendBuffer(TM1, this.tileMapOne, buffer);
        appendBuffer(TM2, this.tileMapTwo, buffer);
//        appendInt(MODE, this.mode.ordinal(), buffer);
        appendInt(SCN, this.scanline, buffer);
        appendInt(CLK, this.scanline, buffer);
    
        //don't think I actually need to save displaybuffer/spritebuffer
        return buffer.array();
    }

    @Override
    public void load(byte[] data) {
        // ByteBuffer buffer = ByteBuffer.allocate(100_000);
        ByteBuffer buffer = ByteBuffer.wrap(data);

        String PPU = "PPU";
        String LCD = "LCD";
        String STA = "STA";
        String SCX ="SCX";
        String SCY= "SCY";
        String SLY = "SLY";
        String LYC = "LYC";
        String BGP = "BGP";
        String OJ1 = "OJ1";
        String OJ2 = "OJ2";
        String SWY ="SWY";
        String SWX = "SWX";
        String OM = "OAM";
        String TT1 = "TTO";
        String TT2 = "TTT";
        String TM1 = "TMO";
        String TM2 = "TMT";

        String MODE = "MOD";
        String SCN = "SCN";
        String CLK = "CLK";

        loadData(PPU, buffer, new byte[4]);
        this.LCDC = loadByte(LCD, buffer);
        this.STAT = loadByte(STA, buffer);
        this.SCX = loadByte(SCX, buffer);
        this.SCY = loadByte(SCY, buffer);
        this.LY = loadByte(SLY, buffer);
        this.LYC = loadByte(LYC, buffer);
        this.BGP = loadByte(BGP, buffer);
        this.OBP0 = loadByte(OJ1, buffer);
        this.OBP1 = loadByte(OJ2, buffer);
        this.WY = loadByte(SWY, buffer);
        this.WX = loadByte(SWX, buffer);
        loadData(OM, buffer, this.OAM);
        loadData(TT1, buffer, this.tileTableOne);
        loadData(TT2, buffer, this.tileTableTwo);
        loadData(TM1, buffer, this.tileMapOne);
        loadData(TM2, buffer, this.tileMapTwo);
//        this.mode = PPU_MODE.values()[loadInt(MODE, buffer)];
        this.scanline = loadInt(SCN, buffer);
        this.scanline = loadInt(CLK, buffer);
    }

}
