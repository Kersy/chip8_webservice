package com.laconic.gameboy.gpu;

public interface Mode{

    PPU_MODE getMode();
    boolean tick(int n);
    void begin();
}