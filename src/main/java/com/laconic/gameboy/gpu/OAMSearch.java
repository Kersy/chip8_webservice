package com.laconic.gameboy.gpu;

public class OAMSearch implements Mode{
    private int ticks = 40;

    public OAMSearch(){
    }

    @Override
    public void begin(){
        this.ticks = 40;
    }

    @Override
    public PPU_MODE getMode(){
        return PPU_MODE.OAM;
    }

    @Override
    public boolean tick(int n) {
        return (this.ticks -= n) > 0;
    }
}
