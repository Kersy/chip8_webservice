package com.laconic.gameboy.gpu;

public class VBLank implements Mode {
    private int ticks = 456;

    @Override
    public void begin(){
        this.ticks = 456;
    }

    @Override
    public PPU_MODE getMode(){
        return PPU_MODE.VBLANK;
    }

    @Override
    public boolean tick(int n) {
        return (this.ticks -= n) > 0;
    }
}
