package com.laconic.gameboy.gpu;

public enum PPU_MODE {
    OAM, HBLANK, VBLANK, PTRANSFER;
}
