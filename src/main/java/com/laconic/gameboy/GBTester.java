package com.laconic.gameboy;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.File;
import java.lang.annotation.Target;
import java.util.*;

import javax.sound.sampled.*;
import javax.sound.sampled.Control.Type;
import javax.swing.JFrame;

import java.nio.file.Files;
// import static org.junit.Assert.assertEquals;

public class GBTester {

  static final int BUFFER_SIZE = 1_024;

  public static void main(String args[]) {

    try {
      // File file = new File("/home/matthew/projects/chip8_webservice/src/main/resources/rec.wav");
      // FileInputStream fis = new FileInputStream(file);
      // int len = fis.available();
      byte[] buffer = new byte[BUFFER_SIZE];

      buffer[0] = 0;
      buffer[1] = 0;
      buffer[2] = 0;
      buffer[3] = 0;
      buffer[4] = 0;
      buffer[5] = 0;
      buffer[6] = 0;
      buffer[7] = 0;

      buffer[8] = 0;
      buffer[9] = 0;
      buffer[10] = 0;
      buffer[11] = 0;
      buffer[12] = 0;
      buffer[13] = 0;
      buffer[14] = 0;
      buffer[15] = 0;

      buffer[16] = -1;
      buffer[17] = -1;
      buffer[18] = -1;
      buffer[19] = -1;
      buffer[20] = -1;
      buffer[21] = -1;
      buffer[22] = -1;
      buffer[23] = -1;

      buffer[24] = -1;
      buffer[25] = -1;
      buffer[26] = -1;
      buffer[27] = -1;
      buffer[28] = -1;
      buffer[29] = -1;
      buffer[30] = -1;
      buffer[31] = -1;

      // byte[] fileData = new byte[len];
      // fis.read(fileData);
      // fis.close();

      
      AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_UNSIGNED, 22_050, 8, 2, 2, BUFFER_SIZE, false);
      // System.out.println(AudioSystem.getAudioFileFormat(file).getFormat());

      SourceDataLine dataLine = (SourceDataLine) AudioSystem.getSourceDataLine(format);
      dataLine.open();


      ByteArrayInputStream stream = new ByteArrayInputStream(buffer);
      byte[] audioData = new byte[BUFFER_SIZE / 4];
      // System.out.println("file data size = "+fileData.length);
      
      dataLine.start();
      // while(stream.available() > 0){
      //   stream.read(buffer);
      //   dataLine.write(audioData,0,audioData.length);
      // }
      for(int i=0; i<8; i++){
        stream.read(audioData);
        dataLine.write(audioData, 0, audioData.length);
      }
      dataLine.stop();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static class SoundOut implements LineListener {

    public SoundOut(){}

    @Override
    public void update(LineEvent event) {
      if(event.getType() == LineEvent.Type.START){
        //derp
      }else{
        //derp
      }
    }
    
  }
}