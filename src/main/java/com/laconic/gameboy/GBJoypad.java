package com.laconic.gameboy;

import com.laconic.gameboy.GBInterruptManager.InterruptType;

public class GBJoypad {

    byte directional = 0xF;
    byte buttons = 0xF;
    byte state = 0b11;

    GBInterruptManager ime;

    public GBJoypad(){}

    public GBJoypad(GBInterruptManager ime){
        this.ime = ime;
    }

    public byte getInput(){
        if(state == 0b10){
            return getButtons();
        }else if(state == 0b01){
            return getDirection();
        }else{
            return 0xF;
        }
    }

    public void setState(byte s){
        if((s & 0xFF) == 48){
            state = 0b11;
        }else if((s & 32) != 0){//want buttons
            state = 0b01;
        }else if((s & 16) != 0){//want directional
            state = 0b10;
        }
        this.ime.requestInterrupt(InterruptType.HILO);
    }

    private byte getButtons(){
        return buttons;
    }

    private byte getDirection(){
        return directional;
    }

    public void setDirection(byte d){
        directional &= d;
    }

    public void setButton(byte b){
        buttons &= b;
    }

    public void resetDirection(byte d){
        directional |= ~d;
    }

    public void resetButton(byte b){
        buttons |= ~b;
    }
}