package com.laconic.gameboy;

import com.laconic.util.Palette;

import java.util.Map;

public class Sprite extends Tile{

    Palette p;
    byte[] data;

    public Sprite(byte[] d, byte b){
        super(d, b);
        this.data = d;
        this.p = new Palette(b);
    }

    public byte[][] convertRawData(){
        byte[][] tmp = new byte[data.length/2][8];
        for(int i=0; i<data.length; i+=2){
            tmp[(i/2)] = bytesToLine(data[i],data[i+1]);
        }
        return tmp;
    }

    @Override
    public byte[] bytesToLine(byte l2, byte l1){
//        int size = (data.length > 16)? 16 : 8;
        byte[] colors = new byte[8];
        int colorSet = -1;
        Map m = p.getNumMap();

        for(int i=0; i<8; i++){
            int bit1 = (((1 << i) & l1) == 0)? 0 : 1;
            int bit2 = (((1 << i) & l2) == 0)? 0 : 1;

            if((bit1 == 1) && (bit2 == 1)){
                colorSet = (byte) m.get((byte)3);
            }else if((bit1 == 1) && (bit2 == 0)){
                colorSet = (byte) m.get((byte)2);
            }else if((bit1 == 0) && (bit2 == 1)){
                colorSet = (byte) m.get((byte)1);
            }else if((bit1 == 0) && (bit2 == 0)){
                colorSet = (byte)4;
            }else{
                colorSet = -1;
            }
            colors[7-i] = (byte)colorSet;
        }
        return colors;
    }
}
