package com.laconic.gameboy;

public class LCDController{
  public byte[] registers = new byte[0x0C];

  public LCDController(){
    // this.registers[0] = (byte) 0b10010011;
  }

  public boolean lcdOn(){
    return checkBit(7);
  }

  public boolean winTileSelect(){
    return checkBit(6);
  }

  public boolean windowOn(){
    return checkBit(5);
  }

  public boolean bgAndWinTileDisplay(){
    return checkBit(4);
  }

  public boolean bgTileDisplay(){
    return checkBit(3);
  }

  public boolean spriteSize(){
    return checkBit(2);
  }

  public boolean spriteDisplayOn(){
    return checkBit(1);
  }

  public boolean bgAndWindowDisplay(){
    return checkBit(0);
  }

  public void setLcdc(byte data){ this.registers[0] = data; }

  public void setStat(byte data){ this.registers[1] = data; }

  public void setSCX(byte data){ this.registers[3] = data; }

  public void setSCY(byte data){ this.registers[2] = data; }

  public void setLY(byte data){ this.registers[4] = data; }

  public void setLYC(byte data){ this.registers[5] = data; }

  public void setDMA(byte data){ this.registers[6] = data; }

  public void setBGP(byte data){ this.registers[7] = data; }

  public void setOBP0(byte data){ this.registers[8] = data; }

  public void setOBP1(byte data){ this.registers[9] = data; }

  public void setWY(byte data){ this.registers[10] = data; }

  public void setWX(byte data){ this.registers[11] = data; }

  public int getLcdc(){ return this.registers[0] & 0xFF; }

  public int getStat(){ return this.registers[1] & 0xFF; }

  public int getSCX(){ return this.registers[3] & 0xFF; }

  public int getSCY(){ return this.registers[2] & 0xFF; }

  public int getLY(){ return this.registers[4] & 0xFF; }

  public int getLYC(){ return this.registers[5] & 0xFF; }

  public int getDMA(){ return this.registers[6] & 0xFF; }

  public int getBGP(){ return this.registers[7] & 0xFF; }

  public int getOBP0(){ return this.registers[8] & 0xFF; }

  public int getOBP1(){ return this.registers[9] & 0xFF; }

  public int getWY(){ return this.registers[10] & 0xFF; }

  public int getWX(){ return this.registers[11] & 0xFF; }

  private boolean checkBit(int bit){
    return (((registers[0] & 0xFF) & (1 << bit)) != 0);
  }

  public byte[] getRegisters(){
    return this.registers;
  }

  public void setByte(int address, byte data){
    switch(address){

      case 0xFF40: setLcdc(data);break;

      case 0xFF41: setStat(data);break;

      case 0xFF42: setSCY(data);break;

      case 0xFF43: setSCX(data);break;

      case 0xFF44: setLY(data);break;

      case 0xFF45: setLYC(data);break;

      case 0xFF46: setDMA(data);break;

      case 0xFF47: setBGP(data);break;

      case 0xFF48: setOBP0(data);break;

      case 0xFF49: setOBP1(data);break;

      case 0xFF4A: setWY(data);break;

      case 0xFF4B: setWX(data);break;

      default: System.out.println("error - setting LCDC register, address range, address: "+(address));
    }
  }

}
