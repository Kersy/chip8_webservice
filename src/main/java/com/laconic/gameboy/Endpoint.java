package com.laconic.gameboy;

public interface Endpoint {
    public int id = 0;
    public byte transfer(byte sb);
}