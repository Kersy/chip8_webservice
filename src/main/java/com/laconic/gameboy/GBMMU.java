package com.laconic.gameboy;

import java.nio.ByteBuffer;
import com.laconic.emulator.StatefulComponent;
import com.laconic.gameboy.controller.Cart;
import com.laconic.gameboy.gpu.PPU;
import com.laconic.gameboy.sound.Sound;

public class GBMMU implements StatefulComponent {

  public byte[] specialRegisters = new byte[0x100];
  private byte[] internalRAM = new byte[8192];
  private byte[] echoRAM = new byte[8192];
  private byte[] HRAM = new byte[128];
  private byte[] OAM = new byte[160];
  // private GBGPU gpu;

  private PPU ppu;
  private Sound apu;
  private Cart cart;
  private GBJoypad joypad;
  private GBTimer timer;
  private GBSerial serial;
  private GBInterruptManager ime;

  private int bank = 0;

  public GBMMU(Cart cart, GBJoypad joypad) {
    this.cart = cart;
    this.joypad = joypad;
  }

  public GBMMU(Cart cart, GBJoypad joypad, GBTimer timer, GBSerial serial, GBInterruptManager ime) {
    this.cart = cart;
    this.joypad = joypad;
    this.timer = timer;
    this.serial = serial;
    this.ime = ime;
  }

  // public void setPPU(GBGPU gpu){ this.gpu = gpu; }
  public void setPPU(PPU ppu) {
    this.ppu = ppu;
  }

  public void setAPU(Sound apu){
    this.apu = apu;
  }

    /*
   *
   * MMU WRITE
   *
   */

  public void write(int pos, int data) {
    int position = pos & 0xFFFF;

    if(position < 0x8000){

        this.cart.write(position, data);

    }else if ((position == 0xFF46)) { // DMA Transfer, needs to be done before writing to other GPU registers (I

      System.out.println("requesting DMA transfer from: " + (data & 0xFF));

    }else if((position >= 0xFF10) && (position < 0xFF40)){
      
      apu.write(position,(data & 0xFF));
  
    }else if ((position >= 0xFF40) && (position < 0xFF4C)) { // GPU registers

      switch (position) {
      case 0xFF40:
        this.ppu.LCDC = (byte) data;
        break;

      case 0xFF41:
        // System.out.println("before STAT change: "+this.ppu.STAT);
        this.ppu.STAT |= (byte)(data & 0b11111000);
        // System.out.println("after STAT change: "+this.ppu.STAT);
        break;

      case 0xFF42:
        this.ppu.SCY = (byte) data;
        break;

      case 0xFF43:
        this.ppu.SCX = (byte) data;
        break;

      case 0xFF44:
        this.ppu.LY = (byte) data;
        break;

      case 0xFF45:
        // System.out.println("writing lyc: "+((byte)data));
        // if((data == 63)){
          this.ppu.LYC = (byte) data;
        // }
        break;

      case 0xFF46:
        this.ppu.DMA = (byte) data;
        break;

      case 0xFF47:
        this.ppu.BGP = (byte) data;
        break;

      case 0xFF48:
        this.ppu.OBP0 = (byte) data;
        break;

      case 0xFF49:
        this.ppu.OBP1 = (byte) data;
        break;

      case 0xFF4A:
        this.ppu.WY = (byte) data;
        break;

      case 0xFF4B:
        this.ppu.WX = (byte) data;
        break;
      }

    } else if ((position >= 0xFE00) && (position < 0xFEA0)) {

      // this.gpu.write(position, (byte) data);
      this.ppu.write(position, (byte) data);

    } else if ((position >= 0xFEA0) && (position <= 0xFEFF)) {// NO IO

    } else if ((position >= 0x8000) && (position <= 0x9FFF)) { // tile area

      // this.gpu.write(position, (byte) data);
      this.ppu.write(position, (byte) data);

    } else if ((position >= 0xFF80) && (position < 0xFFFF)) {// this is for the stack, derp

      int index = position - 0xFF80;
      this.HRAM[index] = (byte) data;

    } else if ((position >= 0xC000) && (position < 0xE000)) { // internal RAM

      int RAMLocation = position - 0xC000;
      internalRAM[RAMLocation] = (byte) data;
      echoRAM[RAMLocation] = (byte) data;

    } else if ((position >= 0xE000) && (position <= 0xFE00)) {// echo RAM

      int RAMLocation = position - 0xE000;
      echoRAM[RAMLocation] = (byte) data;
      internalRAM[RAMLocation] = (byte) data;

    } else if ((position >= 0xA000) && (position <= 0xC000)) {

      this.cart.writeRAM(position, data);

    } else if ((position >= 0xFF00) && (position <= 0xFFFF)) { // special Registers

      if (position == 0xFF00) {
        joypad.setState((byte) data);
      }else if(position == 0xFF01 || position == 0xFF02){ 
        // System.out.println("address: "+position+", data: "+ String.format("0x%02X", data));
        this.serial.write(position, (byte)data);
      }else {
        if (position == 0xFF07) {
          System.out.println("changing clock speed: "+String.format("0x%02X", (byte)data));
          this.timer.setFreq((byte)data);
        }else if(position == 0xFF04){
          System.out.println("write DIV "+data);
          this.timer.DIV = 0;
        }else if(position == 0xFF05){
          System.out.println("writing TIMA "+data);
          this.timer.TIMA = (byte) data;
        }else if(position == 0xFF06){
          System.out.println("writing TMA "+data);
          this.timer.TMA = (byte) data;
        }else if(position == 0xFFFF || position == 0xFF0F){
          this.ime.write(position, (byte)data);
        }else{
          this.specialRegisters[position - 0xFF00] = (byte) data;
        }
      }

    } else {
//      System.out.println("can't write here: " + String.format("0x%02X", position)+", data: "+data);
    }
  }

  /*
   *
   * MMU READ
   *
   */

  public byte read(int pos) {
    int position = pos & 0xFFFF;

    if ((position >= 0xFF80) && (position <= 0xFFFE)) {// Stack, IE flag

      int index = position - 0xFF80;
      return this.HRAM[index];

    } else if (position == 0xFF00) {// requesting JOYPAD

      return this.joypad.getInput();

    } else if ((position >= 0x8000) && (position < 0xA000)) {// GPU AGAIN?

      return this.ppu.read(position);
      // return -1;
      // byte b = this.gpu.readFromVram(position);
      // return b;

    } else if ((position >= 0xC000) && (position < 0xE000)) {// INTERNAL RAM

      return this.internalRAM[position - 0xC000];

    } else if ((position >= 0xE000) && (position < 0xFE00)) {// ECHO RAM

      return this.echoRAM[position - 0xE000];

    } else if ((position < 0x8000)) {// ROM MEMORY

        return this.cart.read(position);

    }else if((position >= 0xFF10) && (position < 0xFF40 )){//APU

      return (byte)apu.read(position);

    }else if ((position >= 0xFF40) && (position <= 0xFF4B)) {// GPU

      byte result = -1;

      switch (position) {
      case 0xFF40:
        result = this.ppu.LCDC;
        break;

      case 0xFF41:
        result = this.ppu.STAT;
        break;

      case 0xFF42:
        result = this.ppu.SCY;
        break;

      case 0xFF43:
        result = this.ppu.SCX;
        break;

      case 0xFF44:
        result = this.ppu.LY;
        break;

      case 0xFF45:
        result = this.ppu.LYC;
        break;

      case 0xFF46:
        result = this.ppu.DMA;
        break;

      case 0xFF47:
        result = this.ppu.BGP;
        break;

      case 0xFF48:
        result = this.ppu.OBP0;
        break;

      case 0xFF49:
        result = this.ppu.OBP1;
        break;

      case 0xFF4A:
        result = this.ppu.WY;
        break;

      case 0xFF4B:
        result = this.ppu.WX;
        break;

      default:
        result = -1;
        break;
      }
      // System.out.println("reading from gpu registers:
      // "+String.format("0x%02X",position)+" -> "+String.format("0x%02X",result));
      return result;

    } else if ((position >= 0xA000) && (position < 0xC000)) {

      return this.cart.readRAM(position);

    } else if ((position > 0xFF00) && (position <= 0xFFFF)) {// INTERRUPTS

      if (position == 0xFF07) {
        System.out.println("reading clock speed");
        return this.timer.TAC;
      }else if(position == 0xFF04){
        // System.out.println("reading DIV");
        return this.timer.DIV;
      }else if(position == 0xFF05){
        System.out.println("reading TIMA");
        return this.timer.TIMA;
      }else if(position == 0xFF06){
        System.out.println("reading TMA");
        return this.timer.TMA;
      }else if(position == 0xFF01 || position == 0xFF02){
        return this.serial.read(position);
      }else if(position == 0xFF0F || position == 0xFFFF){
        return this.ime.read(position);
      }else{
        return this.specialRegisters[position - 0xFF00];
      }
    } else {
      // System.out.println("bad address readz: "+position);
      return (byte) 0x00;
    }
  }

  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(100_000);
    //write header
    //then length of data we are going to write
    //then all of the data

    String header = "MMU";
    String specregHeader = "SPR";
    String internalRAMHeader = "IRH";
    String echoRAMHeader = "ERM";
    String HRAMHeader = "HRH";

    appendBuffer(header, new byte[4], buffer);
    appendBuffer(specregHeader, this.specialRegisters, buffer);
    appendBuffer(internalRAMHeader, this.internalRAM, buffer);
    appendBuffer(echoRAMHeader, this.echoRAM, buffer);
    appendBuffer(HRAMHeader, this.HRAM, buffer);

    return buffer.array();
  }

  public void load(byte[] data){

    byte[] bytelength = new byte[4];
    ByteBuffer buffer = ByteBuffer.wrap(data);
    
    String head = "MMU";
    String specregHeader = "SPR";
    String internalRAMHeader = "IRH";
    String echoRAMHeader = "ERM";
    String HRAMHeader = "HRH";

    loadData(head, buffer, bytelength);
    loadData(specregHeader, buffer, this.specialRegisters);
    loadData(internalRAMHeader, buffer, this.internalRAM);
    loadData(echoRAMHeader, buffer, this.echoRAM);
    loadData(HRAMHeader, buffer, this.HRAM);

    // buffer.get(header); //MMU
    // length = buffer.getInt(); //0
    // buffer.get(bytelength);
    
    // buffer.get(header); //SPR
    // length = buffer.getInt(); //??
    // buffer.get(this.specialRegisters);

    // buffer.get(header); //IRH
    // length = buffer.getInt(); //??
    // buffer.get(this.internalRAM);

    // buffer.get(header); //ERM
    // length = buffer.getInt(); //??
    // buffer.get(this.echoRAM);

    // buffer.get(header); //HRH
    // length = buffer.getInt(); //??
    // buffer.get(this.HRAM);
  }
}
