package com.laconic.gameboy;

import com.laconic.util.*;

import java.util.Map;

public class Tile{

  byte[] data;
  Palette p; //just start with default

  public Tile(byte[] data){
    this.data = data;
    this.p = new Palette((byte)0xE4);
  }

  public Tile(byte[] data, byte b){ //might need this for testing purposes
    this.data = data;
    this.p = new Palette(b);
  }

  public void setPalette(byte b){
    this.p = new Palette(b);
  }

  public byte[][] convertRawData(){
    byte[][] tmp = new byte[8][8];
    for(int i=0; i<data.length; i+=2){
      tmp[(i/2)] = bytesToLine(data[i],data[i+1]);
    }
    return tmp;
  }

  public byte[][] flipX(byte[][] sprite){
    byte[][] result = new byte[data.length/2][8];
    for(int i=0; i<data.length/2; i++){
      for(int j=0; j<8; j++){
        result[i][j] = sprite[i][7-j];
      }
    }
    return result;
  }

  public byte[][] flipY(byte[][] sprite){
    byte[][] result = new byte[data.length/2][8];
    for(int i=0; i<data.length/2; i++){
      for(int j=0; j<8; j++){
        result[i][j] = sprite[(data.length/2-1)-i][j];
      }
    }
    return result;
  }

  public byte[] bytesToLine(byte l1, byte l2){

    byte[] colors = new byte[8];
    int colorSet = -1;
    Map m = p.getNumMap();

    for(int i=0; i<8; i++){
      int bit1 = (((1 << i) & l2) == 0)? 0 : 1;
      int bit2 = (((1 << i) & l1) == 0)? 0 : 1;

      if((bit1 == 1) && (bit2 == 1)){
        colorSet = (byte) m.get((byte)3);
      }else if((bit1 == 1) && (bit2 == 0)){
        colorSet = (byte) m.get((byte)2);
      }else if((bit1 == 0) && (bit2 == 1)){
        colorSet = (byte) m.get((byte)1);
      }else if((bit1 == 0) && (bit2 == 0)){
        colorSet = (byte) m.get((byte)0);
      }else{
        colorSet = -1;
      }
      colors[7-i] = (byte)colorSet;
    }
    return colors;
  }

  public int getBit(int n, int bitComp){
    int highBit = (1 << bitComp) & (n & 0xFF);
    return highBit >> bitComp;
  }

  public void printFormattedTile(){}

}
