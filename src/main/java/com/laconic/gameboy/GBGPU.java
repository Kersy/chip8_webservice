package com.laconic.gameboy;

import com.laconic.util.Palette;

import java.io.FileInputStream;
import java.io.File;
import java.util.*;

public class GBGPU{

  private final static int MAX_MEM = 8192;
  private final static int BX = 256;
  private final static int BY = 256;
  private final static int X = 160;
  private final static int Y = 144;
  private final static int MAX_SPRITES = 40;
  private final static int MAX_SPRITES_PER_LINE = 10;
  private final static int MAX_SPRITE_SIZE_X = 8;
  private final static int MAX_SPRITE_SIZE_Y = 16;
  private final static int MIN_SPRITE_SIZE_X = 8;
  private final static int MIN_SPRITE_SIZE_Y= 8;
  private final static int HORI_SCAN = 9198; //KHz
  private final static double VERT_SCAN= 59.73; //Hz
  private final static double MACHINE_CYCLE = 1.05;
  private final static double CLOCK_CYCLE = 4.19;
  private final static int SCROLLX = 0;
  private final static int SCROLLY = 0;

  private final static int TILE_DATA_RANGE = 8191;

  //Tile Data Table is located at either 0x8000 - 0x8FFF or 0x8800 - 0x97FF
  //Background Tile Map contains the numbers of tiles to be displayed. 32 rows of 32 bytes each
  //Eeach byte contains a number of a tile background to be displayed
  //Tile Data Table address is selected in the LCDC register - bit 4

  //Sprite Attribute Table, OAM is located at 0x8000 - 0x8FFF
  //sprites use palette 0xFE00(65,024) - 0xFE9F (65,183) -- 160 bytes of data
  //OAM is 40, 4-byte values

  private byte[] VRAM = new byte[MAX_MEM - TILE_DATA_RANGE]; //BG Tile map 0x8000(32768) - 0x8FFF(36863) or 0x8800(34816) - 0x97FF(38911)
  private byte[] tileRAM = new byte[TILE_DATA_RANGE];

  //private byte[] tileDataTableOne = new byte[]; //0x8000, Tile pattern table one
  //private byte[] tileDataTableTwo = new byte[]; //0x8800, Tile pattern table two
  private byte[] OAM = new byte[160]; //0xFE00 - 0xFE9F
  private byte[] tileMapOne = new byte[4096];
  private byte[] tileMapTwo = new byte[4096];
  private byte[] bgMapOne = new byte[1024]; //0x9800 - 0x9BFF
  private byte[] bgMapTwo = new byte[1024]; //0x9C00 - 0x9FFF

  private GBInterruptManager ime;
  byte[][] display = new byte[X][Y];
  byte[][] backgroundBuffer = new byte[BX][BY];
  byte[][] spriteBuffer = new byte[BX][BY];
  byte[][] screenBuffer = new byte[BX][BY];
  byte[][] windowBuffer = new byte[BX][BY];
  byte[] gpuRegisters = new byte[11];

  private boolean windowDisplay;
  private boolean spriteDisplay;
  private boolean bgAndWindowDisplay;
  private boolean LCDCisOn;

  private GPU_STATE mode;

  private int scanLine = 456;
  private int line = 0;
//  private int scanLine = 228;
  public LCDController lcdc;
  private GBMMU mmu;
  int totalClocks = 0;

  // private byte[][] canvasBuffer = new byte[160][144];

  public enum GPU_STATE{
    VBLANK, HBLANK, OAM, BOTH, VRAM; //THINK THERE'S ANOTHER
  }

  public GBGPU(GBMMU mmu, GBInterruptManager ime){
    this.ime = ime;
    this.windowDisplay = false;
    this.spriteDisplay = false;
    this.bgAndWindowDisplay = false;
    this.LCDCisOn = true;
    this.mode = GPU_STATE.VRAM; //I guess?
    this.lcdc = new LCDController();
  }

  public GBGPU(){
    this.windowDisplay = false;
    this.spriteDisplay = false;
    this.bgAndWindowDisplay = false;
    this.LCDCisOn = true;
    this.mode = GPU_STATE.VRAM; //I guess?
    this.lcdc = new LCDController();
    // this.lcdc.setLcdc((byte)0x91);
  }

  public byte getTileAddress(int address){
    return (byte) 0;
  }

  public boolean displayOn(){
    return ((this.lcdc.getLcdc() & 0b10000000) == 128);
  }

  public LCDController getLcdc(){
    return this.lcdc;
  }

  private boolean VBlankIsSet(){ return false; }

  public void setGPURegister(int address, byte n){
    this.lcdc.setByte(address,n);
  }

  public byte[] getOAM(){
    return this.OAM;
  }

  public void cycle(int cycles){
    totalClocks += (cycles/4);
//    int lyc = this.lcdc.getLYC();

//    if(lyc == this.lcdc.getLY()){
//      this.ime.requestInterrupt(GBInterruptManager.InterruptType.LCDC);
//      this.lcdc.setStat(((byte)(this.lcdc.getStat() | 0b100)));
//    }else{
//      this.lcdc.setStat(((byte)(this.lcdc.getStat() ^ 0b100)));
//    }

//    if(line > 144){
//      if(mode != GPU_STATE.VBLANK){
//        mode = GPU_STATE.VBLANK;
//        this.lcdc.setStat(((byte)((this.lcdc.getStat() & ~3) | 0x01)));
//        this.ime.requestInterrupt(GBInterruptManager.InterruptType.VBLANK);
//      }
//      if(totalClocks >= (114 * 4)){
//        totalClocks = 0;
//        line += 1;
//        this.lcdc.setLY((byte)line);
//      }
//      if(line >= 153){
//        line = 0;
//        this.lcdc.setLY((byte)0);
//        totalClocks = 0;
//      }
//    }else{
//      if(totalClocks < 20){
//        this.lcdc.setStat(((byte)((this.lcdc.getStat() & ~3) | 0x02)));
//        mode = GPU_STATE.OAM;
//      }else if(totalClocks < 63){
//        this.lcdc.setStat(((byte)((this.lcdc.getStat() & ~3) | 0x03)));
//        mode = GPU_STATE.BOTH;
//      }else if(totalClocks < 114){
//        this.lcdc.setStat(((byte)((this.lcdc.getStat() & ~3) | 0x00)));
//        mode = GPU_STATE.HBLANK;
//      }else if(totalClocks >= 114){
//        this.lcdc.setStat(((byte)((this.lcdc.getStat() & ~3) | 0x02)));
//        mode = GPU_STATE.OAM;
//
//        line += 1;
//        this.lcdc.setLY((byte)line);
//        totalClocks = 0;
//      }
//    }

    // GPU_STATE oldmode = this.mode;
    int lyc = this.lcdc.getLYC();
    if(line++ > 144){
      this.line=0;
      this.lcdc.setLY((byte)(this.lcdc.getLY()+1));
    }

    if(this.lcdc.getLY() == 144 && this.mode != GPU_STATE.VBLANK){
      this.mode = GPU_STATE.VBLANK;
      this.ime.requestInterrupt(GBInterruptManager.InterruptType.VBLANK);
      byte vblank = 0b01;
      int stat = this.lcdc.getStat();
      stat &= 0xFC;
      stat |= vblank;
      this.lcdc.setStat((byte)stat);
    }

    if(this.lcdc.getLY() >= 153 && this.mode != GPU_STATE.HBLANK){
      this.lcdc.setLY((byte)0);
      line=0;
      byte hblank = 0b00;
      this.mode = GPU_STATE.HBLANK;
      int stat = this.lcdc.getStat();
      stat &= 0xFC;
      stat |= hblank;
      this.lcdc.setStat((byte)stat);
    }

    if(lyc == this.lcdc.getLY()){
      byte stat = (byte) this.lcdc.getStat();
      this.lcdc.setStat((byte)(stat | 0b100));
      this.ime.requestInterrupt(GBInterruptManager.InterruptType.LCDC);
    }else{
      byte stat = (byte) this.lcdc.getStat();
      this.lcdc.setStat((byte)(stat ^ 0b100));
    }
  }

  private byte[] drawScanLine(){
    int y = this.gpuRegisters[4] & 0xFF;
    return new byte[160];
  }

  public void write(int address, byte data){
    if((address >= 0xFE00) && (address < 0xFEA0)){
      this.OAM[address - 0xFE00] = data;
    }else if((address >= 0x8000) && (address <= 0x8FFF)){
      this.tileMapOne[address - 0x8000] = data;
    }else if(address <= 0x97FF){
      this.tileMapTwo[(address - 0x9000)] = data;
    }else if ((address >= 0x9800) && (address < 0x9C00)){
      this.bgMapOne[address - 0x9800] = data;
    }else if ((address >= 0x9C00) && (address < 0xA000)){
      this.bgMapTwo[address - 0x9C00] = data;
    }else{
      System.out.println("GPU - attempting to write outside of VRAM: "+address);
    }
  }

  public byte readFromVram(int address){
    if((address >= 0xFE00) && (address < 0xFEA0)){

      return (byte) (this.OAM[address - 0xFE00]);

    }else if((address >= 0x8000) && (address <= 0x8FFF)){

      return this.tileMapOne[address - 0x8000];

    }else if((address >= 0x8800) && (address <=0x8FFF)){

      return this.tileMapTwo[address - 0x8000];

    }else if(address <= 0x97FF){

      return this.tileMapTwo[(address - 0x8FFF) + 2047];

    }else if ((address >= 0x9800) && (address < 0x9C00)){

      return this.bgMapOne[address - 0x9800];

    }else if ((address >= 0x9C00) && (address < 0xA000)){

      return this.bgMapTwo[address - 0x9C00];

    }else if((address >= 0xFF40) && (address <=0xFF4B)){

      switch(address){

        case 0xFF40: return (byte) this.lcdc.getLcdc();

        case 0xFF41: return (byte) this.lcdc.getStat();

        case 0xFF42: return (byte) this.lcdc.getSCY();

        case 0xFF43: return (byte) this.lcdc.getSCX();

        case 0xFF44: return (byte) this.lcdc.getLY();

        case 0xFF45: return (byte) this.lcdc.getLYC();

        case 0xFF46: return (byte) this.lcdc.getDMA();

        case 0xFF47: return (byte) this.lcdc.getBGP();

        case 0xFF48: return (byte) this.lcdc.getOBP0();

        case 0xFF49: return (byte) this.lcdc.getOBP1();

        case 0xFF4A: return (byte) this.lcdc.getWY();

        case 0xFF4B: return (byte) this.lcdc.getWX();

        default: return (byte) 0;

      }
    }
    return (byte) 0;
    // return this.VRAM[address];
  }
  private boolean isLcdEnabled(){ return ((this.lcdc.getLcdc() & 0b10000000) == 1); }

  public Tile getTile(int tileNumber, int table,byte b){
    byte[] tile = new byte[16];
    byte[] tileMap = new byte[1024];
    switch(table){

      case 0:
      tileMap = this.tileMapOne;
      break;

      case 1:
      tileMap = this.tileMapTwo;
      break;

    }
    if(tileNumber < 0){
      tileMap = this.tileMapOne;
    }

    for(int i=0; i<16; i++){
      tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
    }
    return new Tile(tile,b);
  }

  public Sprite getSprite(int tileNumber, int table,byte b){
    byte[] tile = new byte[16];
    byte[] tileMap = new byte[1024];
    switch(table){

      case 0:
        tileMap = this.tileMapOne;
        break;

      case 1:
        tileMap = this.tileMapTwo;
        break;

    }
    if(tileNumber < 0){
      tileMap = this.tileMapOne;
    }

    for(int i=0; i<16; i++){
      tile[i] = tileMap[i+((tileNumber & 0xFF) * 16)];
    }
    return new Sprite(tile,b);
  }

  public Tile[] getTileTable(byte by){
    int tableNumber = (this.lcdc.bgAndWinTileDisplay()? 0 : 1);
    Tile[] bgTiles = new Tile[1024];
    int num = 0;
    byte[] bgmap;
    if(lcdc.bgTileDisplay()){
      bgmap = bgMapTwo;
    }else{
      bgmap = bgMapOne;
    }
    for(byte b : bgmap){
      bgTiles[num] = getTile(b,tableNumber,by);
      num++;
    }
    return bgTiles;
  }

  public Tile[] getWindowTable(){
    int tableNumber = (this.lcdc.winTileSelect()? 1 : 0);
    Tile[] bgTiles = new Tile[1024];
    int num = 0;
    byte[] bgmap;
    if(lcdc.bgAndWindowDisplay()){
      bgmap = bgMapTwo;
    }else{
      bgmap = bgMapOne;
    }
    for(byte b : bgmap){
      bgTiles[num] = getTile(b,tableNumber,(byte)this.lcdc.getBGP());
      num++;
    }
    return bgTiles;
  }

  public void updateBG(){
    Tile[] tiles = getTileTable((byte)this.lcdc.getBGP());

    int num = 0;
    for(Tile t : tiles){
      loadTileToDisplay(num++,t.convertRawData());
    }
  }

  public byte[][] getBackgroundBuffer(){

    if(this.lcdc.bgAndWindowDisplay()){
      updateBG();
      this.screenBuffer = this.backgroundBuffer;
    }else{
      System.out.println("not on");
    }

    cropBackground();
    return this.display;
  }

  private void loadTileToDisplay(int tileNumber, byte[][] tile){
    int row = (tileNumber / 32) * 8; //row start
    int column = (tileNumber % 32) * 8; //column start
    for(int i=0; i<8; i++){
      for(int j=0; j<8; j++){
        this.backgroundBuffer[j+column][i+row] = tile[i][j];
      }
    }
  }

  public void loadSpritesToDisplay(){
      byte bgp = (byte)this.lcdc.getBGP();
      byte paletteZero = (byte)(bgp & 3);

//      this.spriteBuffer = new byte[BX][BY];
      byte[] oam = getOAM();
      for(int i=0; i<40; i++){
        int y = ((oam[i * 4] & 0xFF) - 16); //y pos of sprite
        int x = ((oam[i * 4 + 1] & 0xFF) - 8); //x pos of sprite
        int tile = (oam[i * 4 + 2] & 0xFF); //tile to use for sprite
        int attr = oam[i * 4 + 3]; //attributes of sprite
        boolean flipX = ((attr & 0b00100000) != 0); //ignoring for now
        boolean flipY = ((attr & 0b01000000) != 0); //ignoring for now
        boolean behindBG = ((attr & 0b10000000) != 0); //I guess not paint it if it's behind?
        byte objp = (byte)(((attr & 0b00010000) != 0)? this.lcdc.getOBP1(): this.lcdc.getOBP0()); //ignoring for now E4 then C4

//        Tile s = getTile(tile,0,objp);
        Sprite s = getSprite(tile,0,objp);
        if((x < 0 || x >= 160) || (y < 0 || y > 144)){
          //don't draw
        }else{
          byte[][] sprite = (flipX)? s.flipX(s.convertRawData()) : s.convertRawData();
          sprite = (flipY)? s.flipY(sprite) : sprite;
          for(int z=0; z<8; z++){
            for(int j=0; j<8; j++){
              byte pixel = sprite[z][j];
//              this.spriteBuffer[(j+x)%160][(z+y)%144] = pixel;
              if(behindBG){
                if(display[(j+x)%160][(z+y)%144] == paletteZero && pixel != 4){
                  display[(j+x)%160][(z+y)%144] = pixel;
                }
              }else{
                if(pixel != 4){
                  display[(j+x)%160][(z+y)%144] = pixel;
                }
              }
            }
          }
        }
      }
  }

  public byte[][] getSpriteBuffer(){
    loadSpritesToDisplay();
    return this.spriteBuffer;
  }

  private void loadWindowToDisplay(){
    Tile[] tiles = getWindowTable();
    int wy = this.lcdc.getWY();
    int wx = this.lcdc.getWX();
    int num = 0;
    for(Tile t : tiles){
      int row = (num / 32) * 8; //row start
      int column = (num % 32) * 8; //column start
      num++;
      for(int i=0; i<8; i++){
        for(int j=0; j<8; j++){
          int x = (j + column - 7 + wx);
          int y = (i + row + wy);
          if((x >= 0 && x < 160) && (y >= 0 && y < 144)){
            this.display[x][y] = t.convertRawData()[i][j];
            // this.windowBuffer[i + column][j + row] = t.convertRawData()[i][j];
          }
        }
      }
    }
  }

  private void cropBackground(){
    int sx = lcdc.getSCX();
    int sy = lcdc.getSCY();
    for(int i=0+sx; i<X+sx; i++){
      for(int j=0+sy; j<Y+sy; j++){
        display[(i-sx)%160][(j-sy)%144] = screenBuffer[(i)%256][(j)%256];
      }
    }

    if(this.lcdc.windowOn()){
      loadWindowToDisplay();
    }

    if(this.lcdc.spriteDisplayOn()){
      loadSpritesToDisplay();
    }
  }

}
