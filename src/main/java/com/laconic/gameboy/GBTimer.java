package com.laconic.gameboy;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;

public class GBTimer implements StatefulComponent{

  private final static int CLOCK_SPEED = (4_194_304 / 4);

  private final static int TIMER_ONE = 4096;
  private final static int TIMER_TWO = 16780;
  private final static int TIMER_THREE = 65536;
  private final static int TIMER_FOUR = 262144;

  public byte DIV = 0;
  public byte TIMA = 0;
  public byte TMA = 0;
  public byte TAC = 0;

  // private final static byte timerRequest = 0x04;

  //private final static int DIV_FRQ = CLOCK_SPEED/16384; //256

  private int cycleFreq = TIMER_ONE; //always start with this?
  private int timerCounter = CLOCK_SPEED/cycleFreq;
  private int divCounter = 0;
  private int MAXCYCLES = 69905;
  public int currentCycles = 0;

  // private GBMMU mmu;

  GBInterruptManager ime;
  GBGPU gpu;

  public GBTimer(GBInterruptManager interruptManager){
    // this.mmu = mmu;
    this.ime = interruptManager;
  }

  public GBTimer(GBGPU gpu){
    this.gpu = gpu;
  }

  public void cycle(int numberOfCycles){

    this.currentCycles = numberOfCycles;
    this.divCounter += numberOfCycles;

    updateDIV();

    if(getFreq() != this.cycleFreq){
      System.out.println("Frequency change "+getFreq());
      this.cycleFreq = getFreq();
      resetClock();
    }

    if(timerOn()){
      this.timerCounter -= numberOfCycles;

      if(timerCounter <= 0){
        resetClock();
        this.TIMA++; //where to restart the TMA register to
        if(this.TIMA == 0){
          this.TIMA = this.TMA;
        }
      }
    }
  }

  private void updateDIV(){
    if(this.divCounter>256){
      this.divCounter = 0;
      this.DIV++;
    }
  }

  public void clearDIV(){
    this.DIV = 0;
  }

  public byte getDIV(){
    return this.DIV;
  }

  public boolean isStopped(){ return false; }

  public void setFreq(byte n){
    this.TAC = n;
    // this.mmu.write(0xFF07, n);
  }

  public byte readFreq(){ //TODO: SET BACK TO PRIVATE I THINK
    return (byte)(this.TAC & 0b011);
  }

  private int getFreq(){
    switch(readFreq()){

      case 0b00: return TIMER_ONE;

      case 0b01: return TIMER_FOUR;

      case 0b10: return TIMER_THREE;

      case 0b11: return TIMER_TWO;
    }
    return -1;
  }

  public void switchFreq(){
    //TODO: FIX THIS, WE COULD JUST MAKE N=64, 16, 4...
    byte freq = readFreq();
    int n = 0;
    switch(freq & 0xFF){

      case 0b00: n = 0;

      case 0b01: n = 3;

      case 0b10: n = 2;

      case 0b11: n = 1;
    }
    this.cycleFreq = TIMER_ONE * (4^n);
    this.timerCounter = CLOCK_SPEED/this.cycleFreq;
  }

  public void sendIR(){ //TODO: SWAP TO PRIVATE
    this.ime.requestInterrupt(GBInterruptManager.InterruptType.TIMER);
  }

  private void resetClock(){
    this.timerCounter = CLOCK_SPEED/this.cycleFreq;
  }

  private boolean timerOn(){
    return ((this.TAC & 0b100) != 0);
  }

  @Override
  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(1_000);

    // divCounter
    // TIMA
    // DIV

    String header = "TMR";
    String cyclfrq = "CFR"; //always start with this?
    String counter = "CNT";
    String max = "MAX";
    String current = "CUR";
    String div = "DIV";
    String tima = "TIM";

    appendBuffer(header, new byte[4], buffer);
    appendInt(cyclfrq, this.cycleFreq, buffer);
    appendInt(counter, this.timerCounter, buffer);
    appendInt(max, this.MAXCYCLES, buffer);
    appendInt(current, this.currentCycles, buffer);
    appendByte(div, this.DIV, buffer);
    appendByte(tima, this.TIMA, buffer);

    return buffer.array();
  }

  @Override
  public void load(byte[] data) {
    ByteBuffer buffer = ByteBuffer.wrap(data);

    String header = "TMR";
    String cyclfrq = "CFR"; //always start with this?
    String counter = "CNT";
    String max = "MAX";
    String current = "CUR";
    String div = "DIV";
    String tima = "TIM";

    loadData(header, buffer, new byte[4]);
    this.cycleFreq = loadInt(cyclfrq, buffer);
    this.timerCounter = loadInt(counter, buffer);
    this.MAXCYCLES = loadInt(max, buffer);
    this.currentCycles = loadInt(current, buffer);
    this.DIV = loadByte(div, buffer);
    this.TIMA = loadByte(tima, buffer);

  }




}
