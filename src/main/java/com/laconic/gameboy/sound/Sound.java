package com.laconic.gameboy.sound;

public class Sound {

    private static final int[] MASKS = new int[] {
            0x80, 0x3f, 0x00, 0xff, 0xbf,
            0xff, 0x3f, 0x00, 0xff, 0xbf,
            0x7f, 0xff, 0x9f, 0xff, 0xbf,
            0xff, 0xff, 0x00, 0x00, 0xbf,
            0x00, 0x00, 0x70,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };

    private final AbstractSoundMode[] allModes = new AbstractSoundMode[4];

    private int r1 = 0; //0xFF24;
    private int r2 = 0; //0xFF25;
    private int r3 = 0; //0xFF26;

    // private final Ram r = new Ram(0xff24, 0x03);

    private AudioSystemSoundOutput output;

    private int[] channels = new int[4];

    private boolean enabled;

    private boolean[] overridenEnabled = {true, true, true, true};

    public Sound(AudioSystemSoundOutput output, boolean gbc) {
        allModes[0] = new SoundMode1(gbc);
        allModes[1] = new SoundMode2(gbc);
        allModes[2] = new SoundMode3(gbc);
        allModes[3] = new SoundMode4(gbc);
        this.output = output;
    }

    public void tick() {
        if (!enabled) {
            return;
        }
        for (int i = 0; i < allModes.length; i++) {
            AbstractSoundMode m = allModes[i];
            channels[i] = m.tick();
        }

        int selection = r2;//r.getByte(0xff25);
        int left = 0;
        int right = 0;
        for (int i = 0; i < 4; i++) {
            if (!overridenEnabled[i]) {
                continue;
            }
            if ((selection & (1 << i + 4)) != 0) {
                left += channels[i];
            }
            if ((selection & (1 << i)) != 0) {
                right += channels[i];
            }
        }
        left /= 4;
        right /= 4;

        int volumes = r1;//r.getByte(0xff24);
        left *= ((volumes >> 4) & 0b111);
        right *= (volumes & 0b111);

        output.play((byte) left, (byte) right);
    }

    public byte[] getBuffer(){
        return this.output.getBuffer();
    }


    public void write(int address, int data){
        switch(address & 0xFF){
            //channel one
            case 0x10: 
                allModes[0].write(address,data);
                break;

            case 0x11:
                allModes[0].write(address,data);
                break;

            case 0x12: 
                allModes[0].write(address,data);
                break;

            case 0x13:
                allModes[0].write(address,data);
                break;
            
            case 0x14: 
                allModes[0].write(address,data);
                break;

                //channel two
            case 0x16:
                allModes[1].write(address,data);
                break;

            case 0x17: 
                allModes[1].write(address,data);
                break;

            case 0x18:
                allModes[1].write(address,data);
                break;

            case 0x19:
                allModes[1].write(address,data);
                break;

            //channel three
            case 0x1A:
                allModes[2].write(address,data & 0b10000000);
                break;

            case 0x1B: 
                allModes[2].write(address,data);
                break;

            case 0x1C:
                allModes[2].write(address,(data & 0b01100000));
                break;

            case 0x1D: 
                allModes[2].write(address,data);
                break;

            case 0x1E:
                allModes[2].write(address,data);
                break;
                
            //channel four
            case 0x20:
                allModes[3].write(address,(data & 0b00111111));
                break;
                
            case 0x21:
                allModes[3].write(address,data);
                break;

            case 0x22:
                allModes[3].write(address,data);
                break;

            case 0x23:
                allModes[3].write(address,data);
                break;

            case 0x24:
                r1 = data;
                break;

            case 0x25:
                r2 = data;
                break;

            case 0x26:
                r3 = data;
                if (address == 0xff26) {
                    if ((data & (1 << 7)) == 0) {
                        if (enabled) {
                            enabled = false;
                            stop();
                        }
                    } else {
                        if (!enabled) {
                            enabled = true;
                            start();
                        }
                    }
                    return;
                }
                break;
        }
    }
    
    public int read(int address) {
        int result;
        if (address == 0xff26) {
            result = 0;
            for (int i = 0; i < allModes.length; i++) {
                result |= allModes[i].isEnabled() ? (1 << i) : 0;
            }
            result |= enabled ? (1 << 7) : 0;
        } else {
            result = getUnmaskedByte(address);
        }
        return result | MASKS[address - 0xff10];
    }

    private int getUnmaskedByte(int address) {
        switch(address & 0xFF){
            //channel one
            case 0x10: 
                return allModes[0].read(address);

            case 0x11:
                return allModes[0].read(address);

            case 0x12: 
                return allModes[0].read(address);

            case 0x13:
                return allModes[0].read(address);
            
            case 0x14: 
                return allModes[0].read(address);

            //channel two
            case 0x16:
                return allModes[1].read(address);

            case 0x17: 
                return allModes[1].read(address);

            case 0x18:
                return allModes[1].read(address);

            case 0x19:
                return allModes[1].read(address);

            //channel three
            case 0x1A:
                return allModes[2].read(address);

            case 0x1B: 
                return allModes[2].read(address);

            case 0x1C:
                return allModes[2].read(address);

            case 0x1D: 
                return allModes[2].read(address);

            case 0x1E:
                return allModes[2].read(address);
                
            //channel four
            case 0x20:
                return allModes[3].read(address);
                
            case 0x21:
                return allModes[3].read(address);

            case 0x22:
                return allModes[3].read(address);

            case 0x23:
                return allModes[3].read(address);

            case 0x24: 
                return r1;
            
            case 0x25:
                return r2;

            case 0x26:
                return r3;

            default:
                return -1;
        }
    }

    private void start() {
        for (int i = 0xff10; i <= 0xff25; i++) {
            int v = 0;
            // lengths should be preserved
            if (i == 0xff11 || i == 0xff16 || i == 0xff20) { // channel 1, 2, 4 lengths
                v = getUnmaskedByte(i) & 0b00111111;
            } else if (i == 0xff1b) { // channel 3 length
                v = getUnmaskedByte(i);
            }
            write(i, v);
        }
        for (AbstractSoundMode m : allModes) {
            m.start();
        }
        output.start();
    }

    private void stop() {
        output.stop();
        for (AbstractSoundMode s : allModes) {
            s.stop();
        }
    }

    public void enableChannel(int i, boolean enabled) {
        overridenEnabled[i] = enabled;
    }
}