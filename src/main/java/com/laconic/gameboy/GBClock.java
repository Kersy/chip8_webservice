package com.laconic.gameboy;

public class GBClock{

  private GBProcessor cpu;
  private GBGPU gpu;
  int numOfCycles = 0;

  public GBClock(GBProcessor cpu, GBGPU gpu){
    this.cpu=cpu;
    this.gpu=gpu;
  }

  public void tick(){
    this.cpu.decode();
    this.numOfCycles = this.cpu.getCycleCount();
    System.out.println("number of cycles: "+this.numOfCycles);
    this.gpu.cycle(this.numOfCycles);
  }
}
