package com.laconic.gameboy.controller;

public abstract class Cart {

  protected byte[] ROM;
  protected byte[] RAM;

  protected byte bLo = 1;
  protected byte bHi = 0;

  protected CartType cartType;
  protected String title;

  int length = 0;
  int romBanks = 32; //need to fix this
  int ramBanks = 0;

//   byte[] BOOT = new byte[0x100];

  public Cart(byte[] rom, int ramSize, int romBanks, int ramBanks, CartType cartType, String title){
      this.ROM = rom;
      this.RAM = new byte[ramSize];
      this.romBanks = romBanks;
      this.ramBanks = ramBanks;
      this.cartType = cartType;
      this.title = title;
      this.length = rom.length;
  }

  public abstract byte read(int address);
  public abstract void write(int address, int data);

  public abstract void writeRAM(int address, int data);
  public abstract byte readRAM(int address);
  public abstract void swapRamBank(byte bank);
  public abstract void swapBank(byte bank);
  public abstract byte readBank(int address);
  public abstract byte getByte(int address);
  public abstract CartType getCartType();
}