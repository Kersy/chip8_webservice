package com.laconic.gameboy.controller;

public class MBC3 extends MBC1{

    public MBC3(byte[] rom, int ramSize, int romBanks, int ramBanks, CartType cartType, String title) {
        super(rom, ramSize, romBanks, ramBanks, cartType, title);
    }

    @Override
    public void swapBank(byte bank) {
        this.bLo = (byte)(bank & 0x7F);
        if(this.bLo == 0){
            this.bLo = 1;
        }
    }

    @Override
    public byte readBank(int address) {
        int offSet;
        if(bHi > 0){
            offSet = ((bLo & 0x7F) + 0b100000000) * 0x4000;
        }else{
            offSet = (bLo & 0x7F) * 0x4000;
        }
        try{
            if((offSet + (address - 0x4000)) < this.length){
                return this.ROM[(offSet + (address - 0x4000))];
            }else{
                return (byte)0xFF;
            }
        }catch(Exception e){
            System.out.println("cannot read rom at bank: "+(bLo & 0xFF)+", bank hi = "+bHi+", address = "+(offSet+address));
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public CartType getCartType() {
        return CartType.MBC3;
    }
}
