package com.laconic.gameboy.controller;

public enum CartType{
    MBC1, MBC2, MBC3, MBC5;
}