package com.laconic.gameboy.controller;

public class MBC1 extends Cart{

    int ramBank = 0;
    
    public MBC1(byte[] rom, int ramSize, int romBanks, int ramBanks, CartType cartType, String title){
        super(rom,ramSize,romBanks,ramBanks,cartType,title);
    }

    @Override
    public void writeRAM(int address, int data) { //0xA000 - 0xBFFF
        int offSet = ramBank * 0x2000;
        this.RAM[(address - 0xA000) + offSet] = (byte) data;
    }

    @Override
    public byte readRAM(int address) {
        if(ramBanks > 0){
            int offSet = ramBank * 0x2000;
            return this.RAM[(address - 0xA000) + offSet];
        }else{
            return (byte)0;
        }
    }

    @Override
    public void swapRamBank(byte bank){
        this.ramBank = (bank & 0xFF);
    }

    @Override
    public void swapBank(byte bank) {
        this.bLo = (byte)(bank & 0x1F);
        if(this.bLo == 0){
            this.bLo = 1;
        }
    }

    @Override
    public byte readBank(int address) {
      int offSet;
      if(bHi > 0){
        offSet = ((bLo & 0x1F) + 0b100000000) * 0x4000;
      }else{
        offSet = ((bLo%romBanks) & 0x1F) * 0x4000;
      }
      try{
        if((offSet + (address - 0x4000)) < this.length){
          return this.ROM[(offSet + (address - 0x4000))];
        }else{
          return (byte)0xFF;
        }
      }catch(Exception e){
        System.out.println("cannot read rom at bank: "+(bLo & 0xFF)+", bank hi = "+bHi+", address = "+(offSet+address));
        e.printStackTrace();
        return -1;
      }
    }

    @Override
    public byte getByte(int address) {
        return this.ROM[address];
    }

    @Override
    public CartType getCartType() {
        return CartType.MBC1;
    }

    @Override
    public void write(int address, int data){
        if(address < 0x2000){ //enable RAM read/write

        }else if(address < 0x4000){
            swapBank((byte)data);
        }else if(address < 0x6000){
            swapRamBank((byte)data);
        }else if(address < 0x8000){ //swap rom/ram mode

        }else{
            System.out.println("errorneous cart write: "+address);
        }
    }

    @Override
    public byte read(int address){
        if(address < 0x4000){
            return getByte(address);
        }else if(address < 0x8000){
            return readBank(address);
        }else{
            System.out.println("errorneous cart read: "+address);
            return -1;
        }
    }
    
}