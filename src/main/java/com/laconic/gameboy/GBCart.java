package com.laconic.gameboy;

import java.io.FileInputStream;
import java.io.File;
import java.util.*;

public class GBCart{

  public final static int bankOffset = 16000;

  private byte[] ROM;
  private byte[] RAM;

  private byte bLo = 1;
  private byte bHi = 0;

  int length = 0;
  int romBanks = 4;
  int ramBanks = 0;

  private int bankPointer = 0x4000;
  private String model = "";
  private String filename;
  byte[] BOOT = new byte[0x100];

  public void writeRAM(int position, int data){
    this.RAM[position - 0xA000] = (byte) data;
  }

  public byte readRAM(int position){
    return this.RAM[position - 0xA000];
  }

  public GBCart(String url){
    this.filename = url;
    loadCart();
    getCartMetaData();
    this.RAM = new byte[0x2000];
    // loadBootROM();
  }

  public GBCart(File file){
    loadFile(file);
    getCartMetaData();
    bankPointer = 0x4000;
  }

  public void swapBank(byte bank){
    this.bLo = (byte)(bank & 0x1F);
    if(this.bLo == 0){
      this.bLo = 1;
    }
  }

  public void swapHighBank(byte hi){
    bHi = hi;
  }

  public void swapRamBank(int bank){
  }

  public byte readBank(int pos){
//    if(bLo == 0 && bHi == 0){
//      return this.ROM[pos + 0x4000];
//    }else{

      int offSet;
      if(bHi > 0){
        offSet = ((bLo & 0x1F) + 0b100000000) * 0x4000;
      }else{
        offSet = ((bLo%romBanks) & 0x1F) * 0x4000;
      }
      try{
        if((offSet + (pos - 0x4000)) < this.length){
          return this.ROM[(offSet + (pos - 0x4000))];
        }else{
          return (byte)0xFF;
        }
      }catch(Exception e){
        System.out.println("cannot read rom at bank: "+(bLo & 0xFF)+", bank hi = "+bHi+", address = "+(offSet+pos));
        e.printStackTrace();
//        System.exit(-1);
        return -1;
      }
//    }
  }

  public GBCart(int[] cartData){
    for(int i=0; i<cartData.length; i++){
      this.ROM[i] = (byte)cartData[i];
    }
  }

  public byte[] getLogo(){
    byte[] tile = new byte[48];
    for(int i=0; i<tile.length; i++){
      tile[i] = this.ROM[i];
    }
    return tile;
  }

  private void loadBootROM(){
    int[] boot = Boot.GAMEBOY_CLASSIC;
    for(int i=0; i<0x100; i++){
      BOOT[i] = (byte)boot[i];
    }
    for(int i=0; i<0x100; i++){
      this.ROM[i] = this.BOOT[i];
    }
  }

  public void loadFile(File file){
    try{
      FileInputStream fis = new FileInputStream(file);
      long len = fis.getChannel().size();
      this.ROM = new byte[(int)len];
      fis.read(this.ROM);

      fis.close();
      this.length = (int)len;
      System.out.println("successfully loaded cart, bytes: "+len);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public void loadCart(){
    //should start by checking cart type, but right now we are only using MBC1
    try{
      File file = new File(this.filename);
      // System.out.println("is file readable: "+Files.isReadable(file.toPath()));
      FileInputStream fis = new FileInputStream(file);
      long len = fis.getChannel().size();
      this.ROM = new byte[(int)len];
      fis.read(this.ROM);

      fis.close();

//      this.romBanks = (this.ROM[0x148] & 0xFF);
      this.length = (int)len;

      System.out.println("successfully loaded cart, bytes: "+len);

    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public byte getByte(int address){
    return this.ROM[address];
  }

  public String getModel(){
    return this.model;
  }

  private void getCartMetaData(){
    System.out.println("~!~!~Cart metadata~!~!~");
    System.out.println("color gb flag: "+String.format("0x%02X",this.ROM[0x143]));
    System.out.println("Game mfg code: "+String.format("0x%02X",this.ROM[0x144])+":"+String.format("0x%02X",this.ROM[0x145]));
    System.out.println("Cart type: "+String.format("0x%02X",this.ROM[0x147]));
    System.out.println("ROM size: "+String.format("0x%02X",this.ROM[0x148]));
    System.out.println("RAM size: "+String.format("0x%02X",this.ROM[0x149]));
    System.out.println("~!~!~Cart metadata end~!~!~");
  }

}
