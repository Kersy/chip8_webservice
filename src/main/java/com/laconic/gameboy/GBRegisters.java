package com.laconic.gameboy;

import java.nio.ByteBuffer;

import com.laconic.emulator.StatefulComponent;

public class GBRegisters implements StatefulComponent{

  public byte A;
  public byte B;
  public byte D;
  public byte H;
  public byte C;
  public byte F; //Flag register
  public byte E;
  public byte L;
  // short HL;

  public short SP;
  public short PC; //program counter?

  public short getBC(){
    return (short)GBUtils.joinBytes(this.B,this.C);
  }

  public short getDE(){
    return (short)GBUtils.joinBytes(this.D,this.E);
  }

  public short getAF(){
    return (short)GBUtils.joinBytes(this.A,this.F);
  }

  public void writeAF(int data){
    byte[] bytes = GBUtils.splitBytes(data);
    this.A = bytes[0];
    this.F = bytes[1];
  }

  public void writeBC(int data){
    byte[] bytes = GBUtils.splitBytes(data);
    this.B = bytes[0];
    this.C = bytes[1];
  }

  public void writeDE(int data){
    byte[] bytes = GBUtils.splitBytes(data);
    this.D = bytes[0];
    this.E = bytes[1];
  }

  public void writeHL(int data){
    byte[] bytes = GBUtils.splitBytes(data);
    this.H = bytes[0];
    this.L = bytes[1];
  }

  public void writeAF(byte b1, byte b2){
    this.A = b1;
    this.F = b2;
  }

  public void writeBC(byte b1, byte b2){
    this.B = b1;
    this.C = b2;
  }

  public void writeDE(byte b1, byte b2){
    this.D = b1;
    this.E = b2;
  }

  public void writeHL(byte b1, byte b2){
    this.H = b1;
    this.L = b2;
  }


  public short getHL(){
    return (short) GBUtils.joinBytes(this.H,this.L);
  }

  public void setZeroFlag(){
    this.F |= (byte) 0x80;
  }

  public void setSubtractFlag(){
    this.F |= (byte) 0x40;
  }

  public void setHalfCarryFlag(){
    this.F |= (byte) 0x20;
  }

  public void setCarryFlag(){
    this.F |= (byte) 0x10;
  }

  public void resetZeroFlag(){
    this.F &= 0x70;
  }

  public void resetSubtractFlag(){
    this.F &= 0xB0;
  }

  public void resetHalfCarryFlag(){
    this.F &= 0xD0;
  }

  public void resetCarryFlag(){
    this.F &= 0xE0;
  }

  public boolean isZeroFlagSet(){
    return ((this.F & 0xFF) & 0x80) != 0;
  }

  public boolean isSubFlagSet(){
    return ((this.F & 0xFF) & 0x40) != 0;
  }

  public boolean isHalfCarryFlagSet(){
    return ((this.F & 0xFF) & 0x20) != 0;
  }

  public boolean isCarryFlagSet(){
    return ((this.F & 0xFF) & 0x10) != 0;
  }

  public void decHL(){
    writeHL((getHL() & 0xFFFF) - 1);
  }

  public void decSP(){ SP--; }

  public void decPC(){ PC--; }

  public void incHL(){
      writeHL((short)(getHL() + 1));
  }

  public void incSP(){ SP++; }

  public void incPC(){ PC++; }

  @Override
  public byte[] save() {
    ByteBuffer buffer = ByteBuffer.allocate(100_000);

    String header = "REG";
    String a = "AAA";
    String b = "BBB";
    String d = "DDD";
    String h = "HHH";
    String c = "CCC";
    String f = "FFF";
    String e = "EEE";
    String l = "LLL";
  
    String sp = "SPx";
    String pc = "PCx";

    appendBuffer(header, new byte[4], buffer);
    appendByte(a, this.A, buffer);
    appendByte(b, this.B, buffer);
    appendByte(d, this.D, buffer);
    appendByte(h, this.H, buffer);
    appendByte(c, this.C, buffer);
    appendByte(f, this.F, buffer);
    appendByte(e, this.E, buffer);
    appendByte(l, this.L, buffer);
    appendInt(sp, this.SP, buffer);
    appendInt(pc, this.PC, buffer);

    return buffer.array();
  }

  @Override
  public void load(byte[] data) {
    ByteBuffer buffer = ByteBuffer.wrap(data);

    String header = "REG";
    String a = "AAA";
    String b = "BBB";
    String d = "DDD";
    String h = "HHH";
    String c = "CCC";
    String f = "FFF";
    String e = "EEE";
    String l = "LLL";
  
    String sp = "SPx";
    String pc = "PCx";

    loadData(header, buffer, new byte[4]);
    this.A = loadByte(a, buffer);
    this.B = loadByte(b, buffer);
    this.D = loadByte(d, buffer);
    this.H = loadByte(h, buffer);
    this.C = loadByte(c, buffer);
    this.F = loadByte(f, buffer);
    this.E = loadByte(e, buffer);
    this.L = loadByte(l, buffer);
    this.SP = (short) loadInt(sp, buffer);
    this.PC = (short) loadInt(pc, buffer);

  }

}
