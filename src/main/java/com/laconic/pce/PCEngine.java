package com.laconic.pce;

import com.laconic.pce.cpu.HuC6280;
import com.laconic.pce.hucard.Hucard;
import com.laconic.util.PceReader;
import com.laconic.pce.gpu.InterruptControl;
import com.laconic.pce.gpu.Timer;

public class PCEngine {
    
    HuC6280 cpu;
    PCEMMU mmu;
    Hucard hucard;
    InterruptControl ic;
    Timer timer;

    public PCEngine(){
        this.hucard = new Hucard(PceReader.readROM("/home/matthew/projects/emulation_server/ROMS/pce/jjandjeff.pce"));
        this.ic = new InterruptControl();
        // this.hucard = new Hucard(PceReader.readROM("/home/matthew/Downloads/pce_test.pce"));
        this.timer = new Timer(ic);
        this.mmu = new PCEMMU(this.hucard,timer,ic);
        this.cpu = new HuC6280(this.mmu,ic);
    }

    public void step(){
        this.cpu.decode();
        timer.cycle(4);
        // this.mmu.zeroPageWrite((byte)0, (byte)1);
        // System.out.println(this.mmu.zeroPageRead((byte)0));
        // return this.cpu.fetch();
    }
}