package com.laconic.pce;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.laconic.emulator.Display;
import com.laconic.emulator.Emulator;
import com.laconic.util.*;

import com.laconic.pce.hucard.Hucard;
import com.laconic.pce.PCEMMU;
import com.laconic.pce.cpu.HuC6280;
import com.laconic.pce.gpu.InterruptControl;
import com.laconic.pce.gpu.Timer;
import com.laconic.pce.gpu.VCE;
import com.laconic.pce.gpu.VDC;

public class GUITester extends JFrame{

    private static boolean running = true;

    private static JPanel textPanel;
    private static JTextArea sourceArea;//, listingArea;
    private JButton button, butt, tile;
    private JMenuItem saveMI, resetMI, loadMI, quitMI;
    private static Display buttonPanel;
    private static VDC vdc;
    
    public GUITester(VDC videocController){
        vdc = videocController;
        setTitle("Probably an Emulator");
        sourceArea = new JTextArea();
        sourceArea.setEditable(false);

        button = new JButton("step");
        butt = new JButton("load");
        tile = new JButton("save");

        buttonPanel = new Display(new short[256][512],512,256,3);

        textPanel = new JPanel(new GridLayout(1, 2));
        textPanel.add(buttonPanel);
        textPanel.add(new JScrollPane(sourceArea));

        Container container = getContentPane();
        container.add(textPanel, BorderLayout.CENTER);
        container.add(button,BorderLayout.SOUTH);
        container.add(tile,BorderLayout.WEST);
        container.add(butt,BorderLayout.EAST);

        saveMI = new JMenuItem("Save");
        resetMI = new JMenuItem("Reset");
        loadMI = new JMenuItem("Load ROM");
        quitMI = new JMenuItem("Quit SkinnyBASIC parser");
        JMenu fileMenu = new JMenu("File");
        fileMenu.add(saveMI);
        fileMenu.add(resetMI);
        fileMenu.add(loadMI);
        fileMenu.addSeparator();

        fileMenu.addSeparator();
        fileMenu.add(quitMI);
        JMenuBar bar = new JMenuBar();
        bar.add(fileMenu);
        setJMenuBar(bar);
        // butt.addActionListener(new LoadListingListener());
        // button.addActionListener(new SaveListingListener());
        // tile.addActionListener(new TileListener());
        // saveMI.addActionListener(new SaveListener());
        // resetMI.addActionListener(new ResetListener());
        // loadMI.addActionListener(new LoadListener());

        // buttonPanel.addKeyListener(new KeyboardListener());
        buttonPanel.requestFocus();
        this.pack();
    }

    public static void main(String args[]){
        init();
        imageDataRoutine();
        updateDisplay(vdc.getDisplay());
    }

    public static void init(){
        GUITester frm = new GUITester(new VDC(new InterruptControl(),new VCE()));
        frm.setSize(1200,500);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
        running = true;
        buttonPanel.requestFocus();
    }

    public static void updateDisplay(short[][] data){
        buttonPanel.refreshDisplay(data);
    }

    
    public static void sendDataToVDC(int address, int datum){
        vdc.write(address, (byte)datum);
    }
    
    public static void sendDataToVDC(short[] data){
        byte LSB;
        byte MSB;
        for(int i=0; i<data.length; i++){
            MSB = (byte)((data[i] & 0xFF00) >> 8);
            LSB = (byte)(data[i] & 0xFF);
            //LSB first
            sendDataToVDC(2, LSB);
            sendDataToVDC(3, MSB);
        }
    }

    public static void setPointers(){
        sendDataToVDC(0,0);
        sendDataToVDC(2,0);
        sendDataToVDC(3,0);
        int startingAddress = 0x200;
        int combinedAddress;
        sendDataToVDC(0, 0x2);
        for(int i=0; i<2048; i++){
            if(((i/64)%2)==0){
                combinedAddress = startingAddress + (i%2);
                sendDataToVDC(2, (combinedAddress)&0xFF);
                sendDataToVDC(3, (combinedAddress&0xFF00)>>8);
            }else{
                combinedAddress = (startingAddress + (i%2)) + 2;
                sendDataToVDC(2, (combinedAddress)&0xFF);
                sendDataToVDC(3, (combinedAddress&0xFF00)>>8);
            }
    
        }
    }

    public static void fillVRAMPointers(){
        short[] blankPointers = new short[2048];
        for(int i=0; i<blankPointers.length; i++){
            blankPointers[i] = 0x4060/32;
        }
        sendDataToVDC(0,0);//select MAWR, set it to zero
        sendDataToVDC(2,0);
        sendDataToVDC(3,0);

        sendDataToVDC(0,0x2);//select VWR
        sendDataToVDC(blankPointers);//start writing data to VWR
    }

    public static void imageDataRoutine(){
        setPointers();
        //select the MAWR, then write the tile data (should be able to do consecutive writes now)
        sendDataToVDC(0,0); //select MAWR

        sendDataToVDC(2,0x00); //set MAWR begin address
        sendDataToVDC(3,0x20);

        sendDataToVDC(0,0x2); //select VWR

        //tile 0
        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0xF);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0x7);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0x33);
        
        sendDataToVDC(2,0x03);//6
        sendDataToVDC(3,0x1C);
        
        sendDataToVDC(2,0x0D);//7
        sendDataToVDC(3,0x12);

        sendDataToVDC(2,0x09);//8
        sendDataToVDC(3,0xF6);

        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//6
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//7
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//8
        sendDataToVDC(3,0);

        //tile 1
        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0xF0);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0xE0);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0xCC);
        
        sendDataToVDC(2,0xC0);//6
        sendDataToVDC(3,0x38);
        
        sendDataToVDC(2,0xB0);//7
        sendDataToVDC(3,0x48);

        sendDataToVDC(2,0x90);//8
        sendDataToVDC(3,0x6F);

        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//6
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//7
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//8
        sendDataToVDC(3,0);

        //tile 64
        sendDataToVDC(2,0x19);
        sendDataToVDC(3,0x66); //line 1

        sendDataToVDC(2,0x1B);
        sendDataToVDC(3,0x24);//2
        
        sendDataToVDC(2,0x0C);
        sendDataToVDC(3,0x73);//3

        sendDataToVDC(2,0x08);
        sendDataToVDC(3,0xFF);//4

        sendDataToVDC(2,0x02);
        sendDataToVDC(3,0x3F);//5

        sendDataToVDC(2,0x10);
        sendDataToVDC(3,0x7F);//6

        sendDataToVDC(2,0x04);
        sendDataToVDC(3,0xDF);//7

        sendDataToVDC(2,0x01);
        sendDataToVDC(3,0x07);//8
        
        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//6
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//7
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//8
        sendDataToVDC(3,0);


        //tile 65

        sendDataToVDC(2, 0x98);
        sendDataToVDC(3, 0x66);

        sendDataToVDC(2, 0xD8);
        sendDataToVDC(3, 0x24);

        sendDataToVDC(2, 0x30);
        sendDataToVDC(3, 0xCE);

        sendDataToVDC(2, 0x10);
        sendDataToVDC(3, 0xFF);

        sendDataToVDC(2, 0x40);
        sendDataToVDC(3, 0xFC);

        sendDataToVDC(2, 0x08);
        sendDataToVDC(3, 0xFE);

        sendDataToVDC(2, 0x20);
        sendDataToVDC(3, 0xFB);

        sendDataToVDC(2, 0x80);
        sendDataToVDC(3, 0xE0);

        sendDataToVDC(2,0);//1
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//2
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//3
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//4
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//5
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//6
        sendDataToVDC(3,0);
        
        sendDataToVDC(2,0);//7
        sendDataToVDC(3,0);

        sendDataToVDC(2,0);//8
        sendDataToVDC(3,0);
    }

}