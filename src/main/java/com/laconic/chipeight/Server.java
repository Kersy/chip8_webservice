package com.laconic.chipeight;

import java.net.Socket;

import java.net.ServerSocket;
import com.laconic.emulator.*;
import com.laconic.gameboy.*;

public class Server{

  private static LinkerListener linker;
  private static VMScheduler scheduler;

  public static void main(String args[]){
    try{

      linker = new LinkerListener();
      scheduler = new VMScheduler(10,linker);
      ServerSocket s = new ServerSocket(60008);
      scheduler.start();
      scheduler.setPriority(Thread.MAX_PRIORITY);

      while(true){
        // Socket listener = s.accept();
        handShake(s.accept());
      }

    }catch(Exception e){
      e.printStackTrace();
    }
  }

  private static void handShake(Socket listener){
    try{

      byte[] vmid = new byte[4];

      listener.getInputStream().read(vmid); //read first two bytes for id
      int id = ((vmid[0] << 24) | (vmid[1] << 16) | vmid[2] << 8) | (vmid[3]);
      System.out.println("got id from handshake: "+id);

      // GBInterruptManager ime = new GBInterruptManager();
      // GBJoypad joypad = new GBJoypad(ime);
      GBProcessor cpu = new GBProcessor();

      VM vm = new VM(cpu,listener);
      vm.id = id;
      new Input(listener, vm, linker).start();

      // linker.makeMaster(vm.id);
      scheduler.enqueue(vm);

    }catch(Exception e){
      e.printStackTrace();
    }
  }

  private static LinkCable createLink(){
    return new LinkCable();
  }

  private static String getUserName(){
    return "admin";
  }

}
