package com.laconic.chipeight;

public class Sprite{
  int x=0;
  int y=0;
  int palette=0; //the palette tileset it's using

  boolean xFlip = false;
  boolean yFlip = false;
  boolean priority;
  boolean pal1;

  public Sprite(byte[] data){
    // this.sprite = data;
    this.y = data[0] & 0xFF;
    this.x = data[1] & 0xFF;
    this.palette = data[2] & 0xFF;
    this.priority = ((data[3] & (1 << 7)) !=0);
    this.yFlip = ((data[3] & (1 << 6)) !=0);
    this.xFlip = ((data[3] & (1 << 5)) !=0);
    this.pal1 = ((data[3] & (1 << 4)) !=0);
  }

}
