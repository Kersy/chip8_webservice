package com.laconic.chipeight;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.laconic.emulator.LinkedVM;
import com.laconic.emulator.LinkerListener;
import com.laconic.emulator.VM;
import com.laconic.emulator.VirtualMachine;

public class VMScheduler extends Thread{

    final static int MAX_VMS = 10;
    final static int SECOND = 1_000_000_000;
    static int numberOfVMs;
    static int FPS_PER_VM = 60;

    private static int MAX_CYCLES = 69905;
    private LinkerListener linker;

    private int refreshcounter = 0;

    int currentVMIndex = 0;

    long timeToRefresh;

    VM[] VMqueue; //let's see if we can hit a goal of 20 per server

    public VMScheduler(int vms, LinkerListener linker){
        this.linker = linker;
        if(vms <= MAX_VMS){
            numberOfVMs = vms;
            VMqueue = new VM[numberOfVMs];
        }else{
            System.out.println("invalid number of VMS allowed: "+vms+" is greater than: "+MAX_VMS);
        }
        timeToRefresh = System.nanoTime();
    }

    @Override
    public void run(){

        while(true){
            if(currentVMIndex > 0){
                updateAllVMs();
                updateDisplays();
                linkCheck();
            }

            try{
                Thread.sleep(1);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }
 
    private void updateAllVMs(){
        for(int i=0; i<currentVMIndex; i++){
            if(VMqueue[i].isAlive){
                VMqueue[i].sendHostList(linker.getHosts());
            }else{
                dequeue(i); 
            }
        }

        for(int j=0; j<currentVMIndex; j++){
            if(VMqueue[j].isAlive){
                VMqueue[j].stepToScreenUpdate();
            }else{
                dequeue(j); 
            }
        }
    }

    private void updateDisplays(){
        for(int i=0; i<currentVMIndex; i++){
            if(VMqueue[i].isAlive){
                VMqueue[i].updateVideoDisplay();
            }else{
                dequeue(i); //THIS ISN'T DEQUEING
            }
        }
    }

    public void enqueue(VM vm){
        if(this.currentVMIndex < MAX_VMS){
            System.out.println("VM enqueued current queued: "+this.currentVMIndex);
            VMqueue[currentVMIndex++] = vm;
        }else{
            System.out.println("VM queue is full at "+currentVMIndex);
        }
    }

    public VM dequeue(int index){
        if(currentVMIndex > 0){
            VM vm = this.VMqueue[index];
            for(int i=index+1; i<currentVMIndex; i++){
                this.VMqueue[i-1] = this.VMqueue[i];
            }
            this.currentVMIndex -= 1;
            linker.removeHost(vm.id);
            return vm;
        }else{
            System.out.println("VM queue is empty!");
            return null;
        }
    }

    public synchronized void link(int slaveid, int masterid){
        VM master = null;
        VM slave = null;

        int slaveindex = 0;
        int masterindex = 0;

        for(int i=0; i<currentVMIndex; i++){
            int id = VMqueue[i].id;
            if(id == slaveid){
                // System.out.println("dequeing slave: "+id);
                slaveindex = i;
                // slave = dequeue(i);
            }else if(id == masterid){
                masterindex = i;
                // System.out.println("dequeing master: "+id);
                // master = dequeue(i);
            }
        }
        slave = dequeue(slaveindex);
        master = dequeue(masterindex);
        LinkedVM linkedvm = new LinkedVM(master,slave);
        enqueue(linkedvm);
    }

    private void linkCheck(){
        if(linker.hasLinked()){
            for(Map.Entry<Integer,Integer> set : linker.linked.entrySet()){
                int host = set.getKey();
                int slave = set.getValue();
                link(slave,host);
                linker.linked.remove(host); //this may not work as intended
                System.out.println("connecting "+host+" and "+slave);
            }
        }
    }

}