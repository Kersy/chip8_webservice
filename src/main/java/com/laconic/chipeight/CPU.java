package com.laconic.chipeight;

import com.laconic.emulator.Emulator;
import com.laconic.util.SmallStack;
import com.laconic.emulator.EmulatorType;

import java.io.File;
import java.io.FileInputStream;
import java.util.Random;

public class CPU extends Chip8State implements Emulator{

  public int memoryStartLocation=0x200;

  byte[][] display = new byte[X][Y];

  private byte[] keySet = new byte[16];
  private byte soundTime = 0;

  private byte[] memory = new byte[MAX_MEM]; //max 4096
  private byte registers[] = new byte[16];
  private short addressReg; //pretty sure this is addr_I
  private short counter;

  private SmallStack stack = new SmallStack(16);

  private byte delayTimer = 60;
  private byte soundTimer = 60;

  short currentInstr = 0;

  public void decrementTimes(){
    if(this.delayTimer > 0){
      this.delayTimer--;
    }

    if(this.soundTimer > 0){
      this.soundTimer--;
    }

    if(this.soundTimer != 0){
      playSound();
      this.soundTimer=60;
    }
  }
  
  @Override
  public byte[] getAudioBuffer(){
    return new byte[]{-1};
  }

  public void loadState(String filename){
    try{
      this.inputstream = new FileInputStream(new File(this.path+filename));
    }catch(Exception e){
      System.out.println("error reading into fileinputstream: CPU");
      e.printStackTrace();
    }
    this.display = loadDisplay();
    this.counter = loadCounter();
    this.registers = loadRegisters();
    this.addressReg = loadAddressReg();
    this.currentInstr = loadCurrentInstruction();
    this.memory = loadMemory();
    this.stack = loadStack();
    this.delayTimer = loadDelayTimer();
    this.soundTimer = loadSoundTimer();
    try{
      this.inputstream.close();
    }catch(Exception e){
      System.out.println("error closing fileinputstream: CPU");
      e.printStackTrace();
    }
  }

  public CPU(String filename){
    super(filename);
  }

  public boolean save(){ return false; }

  @Override
  public String saveState(String username){
    String file = this.filename+Integer.toString(this.stateCounter)+".ch8";
    writeDisplayToFile(this.display);
    writeCounterToFile(this.counter);
    writeReigstersToFile(this.registers);
    writeAddrRegToFile(this.addressReg);
    writeInstrToFile(this.currentInstr);
    mapMemoryToFile(this.memory);
    this.stack = mapStackToFile(this.stack);
    writeDelayTime(this.delayTimer);
    writeSoundTimer(this.soundTimer);

    resetFileState(this.filename+Integer.toString(++this.stateCounter));
    return file;
  }

  public int getMaxWidth(){
    return X;
  }
  public int getMaxHeight(){
    return Y;
  }
  public EmulatorType getEmulatorType(){
    return EmulatorType.CHIP8;
  }
  private void playSound(){
    //boop
  }
  public byte[] getMemory(){
    return this.memory;
  }
  private void loadCharacters(){
    for(int i=0; i<this.chars.length; i++){
      this.memory[i] = (byte)this.chars[i];
    }
  }

  public void clearDisplay(){
    this.display = new byte[64][32];
  }
  public void setKey(int k){
    this.keySet[k] = 1;
  }
  public void releaseKey(int k){
    this.keySet[k] = 0;
  }
  public int getCurrentCycles(){ return -1; }
  public byte[] getKeySet(){
    return this.keySet;
  }

  public void CPUReset(){
    this.counter = 0x200;
    this.addressReg = 0;

    loadCharacters(); //let's not load characters every time
    clearDisplay();
    try{
      File file = new File("C:\\Users\\Matth\\Work\\chip8_webservice\\ROMS\\ch8\\Landing.ch8");
      getRomData(file);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public void reset(){
    this.counter = 0x200;
    this.addressReg = 0;
    this.memory = new byte[4096];
    this.currentInstr = 0;
    this.display = new byte[64][32];
    this.counter = 0;
    this.registers = new byte[16];
    this.stack = new SmallStack(16);
    this.delayTimer = 60;
    this.soundTimer = 60;
    loadCharacters(); //let's not load characters every time
    clearDisplay();
  }

  public void loadToMemory(byte[] data,int romLength){
      reset();
      int len = data.length;
      // CPUReset();
      System.arraycopy(data, 0, this.memory, this.memoryStartLocation, romLength);
      System.out.println("copy complete");
  }

  public String getCPUData(){
    String data = "Counter: "+this.counter+ " \n"+
                  "address_I: "+this.addressReg+" \n"+
                  "instruction: "+((this.currentInstr & 0xFF00) >> 8)+ ": "+ (this.currentInstr & 0xFF) + " \n"+
                  // "key: "+this.key+" \n"+
                  "\n";
    return data;
  }

  public void memSet(int counter, int op){
    this.memory[counter] = (byte) op;
  }

  public void setRegister(int reg, int value){
    this.registers[reg] = (byte) value;
  }

  // @Override
  public int getRegister(int getNumber){
    return this.registers[getNumber];
  }

  private void getRomData(File file){
    try{
      FileInputStream fis = new FileInputStream(file);
      byte[] romData = new byte[MAX_MEM];
      int len = fis.read(romData);
      System.arraycopy(romData, 0, memory, this.memoryStartLocation, len);
      System.out.println("copy complete");
      fis.close();
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  public byte[][] getDisplay(){
    return display;
  }

  short getNextOPCode(){
    decrementTimes();
    if(this.delayTimer < 0){//probably isn't right
      this.delayTimer=60;
    }
    if(this.soundTimer<0){
      this.soundTimer=60;
    }
    short op = -1;
    // System.out.println("counter: "+this.counter);
    op = (short) ((this.memory[this.counter++] & 0xFF) << 8);
    op |= (short) (this.memory[this.counter++] & 0xFF);
    return op;
  }

  public void decode(){
    short op = getNextOPCode();
    currentInstr = op;

    switch(op & 0xF000){

      case 0x0000: //System.out.println("0");
                   switch(op & 0x000F){

                     case 0x0: //System.out.println(op & 0x000F);
                               op_00E0();break;

                     case 0xE: //System.out.println(op & 0x000F);
                               op_00EE();break;

                     default: System.out.println("error - "+0); break;

                   }break;

      case 0x1000: //System.out.println("1");
                   op_1NNN(op);
                   break;

      case 0x2000: //System.out.println("2");
                   op_2NNN(op);
                   break;

      case 0x3000: //System.out.println("3");
                   op_3XNN(op);
                   break;

      case 0x4000: //System.out.println("4");
                   op_4XNN(op);
                   break;

      case 0x5000: //System.out.println("5");
                   op_5XY0(op);
                   break;

      case 0x6000: //System.out.println("6");
                   op_6XNN(op); break;

      case 0x7000: //System.out.println("7");
                   op_7XNN(op); break;

      case 0x8000: //System.out.println("8");
                   switch(op & 0x00F){

                    case 0x0: op_8XY0(op); break;

                    case 0x1: op_8XY1(op); break;

                    case 0x2: op_8XY2(op); break;

                    case 0x3: op_8XY3(op); break;

                    case 0x4: op_8XY4(op); break;

                    case 0x5: op_8XY5(op); break;

                    case 0x6: op_8XY6(op); break;

                    case 0x7: op_8XY7(op); break;

                    case 0x8: op_8XYE(op); break;

                    default: System.out.println(op&0xFFFF);

                  }break;

      case 0x9000: //System.out.println("9");
                   op_9XY0(op); break;

      case 0xA000: //System.out.println("A");
                   op_ANNN(op); break;

      case 0xB000: //System.out.println("B");
                   op_BNNN(op); break;

      case 0xC000: //System.out.println("C");
                   op_CXNN(op); break;

      case 0xD000: //System.out.println("D");
                   op_DXYN(op); break;

      case 0xE000: //System.out.println("E");
                   switch(op & 0x0FF){

                    case 0x9E: op_EX9E(op); break;

                    case 0xA1: op_EXA1(op); break;
                  }break;

      case 0xF000: //System.out.println("F");
                   decode_09(op); break;

      default: System.out.println("error: "+(0xFF00 & op)+" : "+(0x00FF&op));
    }
  }

  private void decode_09(int op){

    switch(op & 0x00FF){

      case 0x07: op_FX07(op); break;

      case 0x0A: op_FX0A(op); break;

      case 0x15: op_FX15(op); break;

      case 0x18: op_FX18(op); break;

      case 0x1E: op_FX1E(op); break;

      case 0x29: op_FX29(op); break;

      case 0x33: op_FX33(op); break;

      case 0x55: op_FX55(op); break;

      case 0x65: op_FX65(op); break;

      default: System.out.println("error: "+(0xFF00 & op & (0xFF))+" : "+(0x00FF&op));
    }
  }

  private void op_0NNN(){}

  private void op_00E0(){ clearDisplay(); }

  private void op_00EE(){
    this.counter = stack.pop();
  }

  private void op_1NNN(int op){ //goto NNN
    this.counter = (short) (op & 0x0FFF);
  }

  private void op_2NNN(int op){
    this.stack.push((short)this.counter);
    this.counter = (short) (op & 0x0FFF);
  }

  private void op_3XNN(int op){ //check if register matches NN
    int var = op & 0x00FF;
    int n = (op & 0x0F00) >> 8; //this is the register to check
    if(this.registers[n]==var){
      this.counter+=2;
    }
  }

  private void op_4XNN(int op){ //check if register does not match NN
    int var = op & 0x00FF;
    int n = (op & 0x0F00) >> 8;
    if(this.registers[n]!=var){
      this.counter+=2;
    }
  }

  private void op_5XY0(int op){ //check if register X matches register Y
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    if(this.registers[x]==this.registers[y]){
      this.counter+=2;
    }
  }

  private void op_6XNN(int op){
    int x = (op & 0x0F00) >> 8;
    int n = (op & 0x00FF);
    this.registers[x] = (byte) n;
  }

  private void op_7XNN(int op){
    int x = (op & 0x0F00) >> 8;
    int n = (op & 0x00FF);
    this.registers[x] += (byte) n;
  }

  private void op_8XY0(int op){
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    this.registers[x] = this.registers[y];
  }

  private void op_8XY1(int op){
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    this.registers[x] |= this.registers[y];
  }

  private void op_8XY2(int op){
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    this.registers[x] &= this.registers[y];
  }

  private void op_8XY3(int op){
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    this.registers[x] ^= this.registers[y];
  }

  private void op_8XY4(int op){
    this.registers[0xF] = 0;
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    int data = this.registers[y] + this.registers[x];
    if(data > 255){
      this.registers[0xF] = (byte) 1;
    }
    this.registers[x] = (byte) data;
  }

  private void op_8XY5(int op){
    this.registers[0xF] = 1;
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    if(this.registers[y] > this.registers[x]){
      this.registers[0xF] = 0;
    }
    this.registers[x] -= this.registers[y];
  }

  private void op_8XY6(int op){
    int x = (op & 0x0F00) >> 8;
    this.registers[0xF] = (byte) (this.registers[x] & 0x01);
    this.registers[x] >>= 1;
  }

  private void op_8XY7(int op){
    this.registers[0xF] = 1;
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    if(this.registers[x] > this.registers[y]){
      this.registers[0xF] = 0;
    }
    this.registers[x] = (byte)((this.registers[y] - this.registers[x]));
  }

  private void op_8XYE(int op){
    int x = (op & 0x0F00) >> 8;
    int msb = getMSB(this.registers[x],0);
    this.registers[0xF] = (byte) msb;
    this.registers[x] = (byte) (this.registers[x] << 1);
  }

  private void op_9XY0(int op){
    int x = (op & 0x0F0) >> 8;
    int y = (op & 0x00F0) >> 4;
    if(this.registers[x] != this.registers[y]){
      this.counter+=2;
    }
  }

  private void op_ANNN(int op){
    int num = (op & 0x0FFF);
    this.addressReg = (short) num;
  }

  private void op_BNNN(int op){
    // System.out.println("BXNN");
    int n = (op & 0x0FFF);
    this.counter = (short) ( ( (short) this.registers[0] & 0xFF) + (short)n);
  }

  private void op_CXNN(int op){
    Random rand = new Random();
    int num = (op & 0x00FF);
    int x = (op & 0x0F00) >> 8;
    this.registers[x] = (byte) (rand.nextInt(256) & num);
  }

  public void op_DXYN(int op){
    int x = (op & 0x0F00) >> 8;
    int y = (op & 0x00F0) >> 4;
    int height = (op & 0x000F);

    this.registers[0xF] = 0;
    int xCord = this.registers[x] & 0xFF;
    int yCord = this.registers[y] & 0xFF;

    for(int yaxis=0; yaxis<height; yaxis++){
      byte data = this.memory[(int)(addressReg+yaxis+1)]; //the pixel at our memory location plus the current y

      for(int xaxis=0; xaxis<8; xaxis++){
        int mask = 1 << (7-xaxis);
        if((mask & data) != 0){ //if these & aren't zero, than we want to draw a pixel
          int xPix = (xaxis + xCord)%64;
          int yPix = (yaxis + yCord)%32;

          int color = 1; //this basically means "black for the moment"
          if(display[xPix][yPix]==1){ //if this pixel is already white
            color = 0; //we unset the pixel
            this.registers[0xF]=1; //we mark the F register
          }
          display[xPix][yPix] = (byte) color;
        }
      }
    }
  }

  private void op_EX9E(int op){ //this will either be 0-E
    // System.out.println("key press");
    int x = (op & 0x0F00) >> 8;
    int keyPos = this.registers[x];
    if(this.keySet[keyPos] ==  1){
      this.counter+=2;
    }
  }

  private void op_EXA1(int op){
    int x = (op & 0x0F00) >> 8;
    int keyPos = this.registers[x];
    if(this.keySet[keyPos] !=  1){
      this.counter+=2;
    }
  }

  private void op_FX07(int op){
    int x = (op & 0x0F00) >> 8;
    this.registers[x] = (byte) this.delayTimer;
  }

  private void op_FX0A(int op){
    // System.out.println("FX0A");
    int x = (op & 0x0F00) >> 8;
    int counterBack = 2;
    for(int i=0; i<keySet.length-1; i++){
      if(keySet[i]==1){
        this.registers[x] = (byte)i;
        counterBack = 0;
      }
    }
    this.counter -= counterBack;
  }

  private void op_FX15(int op){
    int x = (op & 0x0F00) >> 8;
    this.delayTimer = this.registers[x];
  }

  private void op_FX18(int op){
    int x = (op & 0x0F00) >> 8;
    this.soundTime = this.registers[x];
  }

  private void op_FX1E(int op){
    // System.out.println("FX1E");
    this.registers[0xF] = 0;
    int x = (op & 0x0F00) >> 8;
    if((0xFF & this.registers[x]) + this.addressReg > 0xFFF){
      this.registers[0xF] = 1;
    }
    this.addressReg += (short)(this.registers[x] & 0xFF);
  }

  private void op_FX29(int op){
    int x = (op & 0x0F00) >> 8;
    this.addressReg = this.memory[x*5];
  }

  private void op_FX33(int op){
    int x = (op & 0x0F00) >> 8;
    int reg = this.registers[x] & 0xFF;
    int d3 = reg / 100;
    int d2 = (reg / 10) % 10;
    int d1 = reg % 10;
    this.memory[this.addressReg] = (byte) d3;
    this.memory[this.addressReg+1] = (byte) d2;
    this.memory[this.addressReg+2] = (byte) d1;
  }

  private void op_FX55(int op){
    int x = (op & 0x0F00) >> 8;
    for(int i=0; i<=x; i++){
      this.memory[this.addressReg+i] = (byte) (this.registers[x+i] & 0xFF);
    }
  }

  private void op_FX65(int op){
    int x = (op & 0x0F00) >> 8;
    for(int i=0; i<=x ; i++){
      this.registers[i] = this.memory[this.addressReg+i];
    }
  }

  public static byte getMSB(int num, int n){
    if(num >> n == 0){
      return (byte)n;
    }
    return getMSB(num,n+1);
  }
}
