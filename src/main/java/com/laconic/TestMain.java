package com.laconic;

import com.laconic.emulator.Display;
import com.laconic.emulator.Emulator;
import com.laconic.gameboy.gpu.PPU;
import com.laconic.util.*;

import com.laconic.pce.hucard.Hucard;
import com.laconic.pce.io.Gamepad;
import com.laconic.pce.PCEMMU;
import com.laconic.pce.cpu.*;
import com.laconic.pce.gpu.InterruptControl;
import com.laconic.pce.gpu.Timer;
import com.laconic.pce.gpu.VCE;
import com.laconic.pce.gpu.VDC;

public class TestMain{

  static int test = 1;
  static int fakePC = 0;
  static Gamepad gamepad; //= new Gamepad();
  static Hucard hucard; //= initHucard();
  static VCE vce; //= new VCE();
  static InterruptControl ic; //= new InterruptControl();
  static PCEMMU mmu; //= new PCEMMU(hucard,new Timer(ic),ic,gamepad,vce);
  static HuC6280 hu;


  //TODO: I THINK I HAVE TO WRITE INDIRECT ADDRESSING DIFFERENTLY, IT'S NOT WORKING PROPERLY

  public static void main(String[] args){
  
    System.out.println("~~~ Test begin ~~~");

    byte tst = (byte)255;
    System.out.println(-1 & 0xFF);
    // PCinit();

    // System.out.println("initialize memory maps passed: "+initMemoryMaps());
    // System.out.println("zeropage load and store passed: "+zeroPageLoadAndStore());
    // System.out.println("test and reset memory bits passed: "+testAndResetMemoryBits());
    // System.out.println("test and reset memory bits against accumalator: "+testAndResetMemoryBitsAgainstAcc());
    // System.out.println("test and set memory bits against accumalator: "+testAndSetMemoryBitsAgainstAcc());

  }

  private static void PCinit(){
    gamepad = new Gamepad();
    hucard = new Hucard(PceReader.readROM("C:\\Users\\matthew\\Projects\\emulation_server\\ROMS\\pce\\init.pce"));
    vce = new VCE();
    ic = new InterruptControl();
    mmu = new PCEMMU(hucard,new Timer(ic),ic,gamepad,vce);
    hu = new HuC6280(mmu, ic, (short)0xE000);
  }

  private static boolean initMemoryMaps(){
    boolean pass = false;

    pass = hu.registers.A == (byte)0;
    if(!pass)
      return false;

    hu.decode();

    pass = hu.registers.A == (byte)255;
    if(!pass)
      return false;

    hu.decode();
    hu.decode();
    hu.decode();
    hu.decode();

    pass = hu.registers.X == (byte)0xFE;
    if(!pass)
      return false;
    
      hu.decode();

    pass = hu.registers.SP == (byte)0xFE;
    if(!pass)
      return false;

    hu.decode();

    pass = hu.registers.A == (byte)0;
    if(!pass)
      return false;

    return pass;
  }

  private static boolean zeroPageLoadAndStore(){//LDA, STA
      boolean pass = true;

      hu.decode();

      pass = hu.registers.A == (byte)0x05;
      if(!pass)
        return false;

      hu.decode();
      hu.decode();

      pass = hu.registers.A == 0;
      if(!pass)
        return false;

      hu.decode();

      pass = hu.registers.A == (byte) 0x05;
      if(!pass)
        return false;

      return pass;
  }

  private static boolean testAndResetMemoryBits(){//TST
    boolean pass = false;

    hu.decode();
    pass = hu.registers.A == (byte) 0x15;
    if(!pass)
      return false;

    hu.decode();
    hu.decode();
    hu.decode();
    hu.decode();

    pass = hu.registers.A == (byte) 0x01;
    if(!pass)  
      return false;

    hu.decode();

    pass = hu.registers.A == (byte) 0x50;
    if(!pass)  
      return false;

    hu.decode();
    hu.decode();
    hu.decode();

    pass = hu.registers.A == (byte) 0x10;
    if(!pass)  
      return false;
  
    return pass;
  }

  public static boolean testAndResetMemoryBitsAgainstAcc(){//TRB
    boolean pass = false;

    hu.decode();

    pass = hu.registers.A == (byte) 0x15;
    if(!pass)
      return pass;

    hu.decode();
    hu.decode();
    
    pass = hu.registers.A == (byte) 0x04;
    if(!pass)
      return pass;

    hu.decode();
    hu.decode();
    
    pass = hu.registers.A == (byte) 0x11;
    if(!pass)
      return pass;

    return pass;
  }

  public static boolean testAndSetMemoryBitsAgainstAcc(){ //TSB_Z
    boolean pass = false;

    hu.decode();
    pass = hu.registers.A == (byte) 0x04;
    if(!pass)
      return false;

    hu.decode();
    pass = hu.registers.A == (byte) 0x04;
    if(!pass)
      return false;

    hu.decode();
    pass = hu.registers.A == (byte) 0x03;
    if(!pass)
      return false;

    hu.decode();
    pass = hu.registers.A == (byte) 0x03;
    if(!pass)
       return false;  

    hu.decode();
    pass = hu.registers.A == (byte) 0x07;
    if(!pass)
       return false;  

    return pass;
  }

}
