package com.laconic.emulator;

public enum EmulatorType{
  NONE, CHIP8, GB;
}
